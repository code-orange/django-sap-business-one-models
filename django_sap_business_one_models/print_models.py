import pyclbr

module_name = "models_dump"
module_info = pyclbr.readmodule(module_name)
# print(module_info)

print("from . import models_dump")
print("\n")
print("\n")

for item in module_info.values():
    print("class " + item.name + "(models_dump." + item.name + "):")
    print("    class Meta(models_dump." + item.name + ".Meta):")
    print("        abstract = False")
    print("\n")
