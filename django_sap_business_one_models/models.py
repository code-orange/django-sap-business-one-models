from django.db import models

from . import models_dump


class Asapdatevattach(models_dump.Asapdatevattach):
    class Meta(models_dump.Asapdatevattach.Meta):
        abstract = False


class Asapdateverror(models_dump.Asapdateverror):
    class Meta(models_dump.Asapdateverror.Meta):
        abstract = False


class Asapdateverror2(models_dump.Asapdateverror2):
    class Meta(models_dump.Asapdateverror2.Meta):
        abstract = False


class Asapdateverror3(models_dump.Asapdateverror3):
    class Meta(models_dump.Asapdateverror3.Meta):
        abstract = False


class Asapdatevlines(models_dump.Asapdatevlines):
    class Meta(models_dump.Asapdatevlines.Meta):
        abstract = False


class Asapdatevlines2(models_dump.Asapdatevlines2):
    class Meta(models_dump.Asapdatevlines2.Meta):
        abstract = False


class Asapdatevlines3(models_dump.Asapdatevlines3):
    class Meta(models_dump.Asapdatevlines3.Meta):
        abstract = False


class Asapdatevrun(models_dump.Asapdatevrun):
    class Meta(models_dump.Asapdatevrun.Meta):
        abstract = False


class B1Fconfig(models_dump.B1Fconfig):
    class Meta(models_dump.B1Fconfig.Meta):
        abstract = False


class B1Fplugin(models_dump.B1Fplugin):
    class Meta(models_dump.B1Fplugin.Meta):
        abstract = False


class B1Ftrans(models_dump.B1Ftrans):
    class Meta(models_dump.B1Ftrans.Meta):
        abstract = False


class BdCset(models_dump.BdCset):
    class Meta(models_dump.BdCset.Meta):
        abstract = False


class BdCtax(models_dump.BdCtax):
    class Meta(models_dump.BdCtax.Meta):
        abstract = False


class BdSerr(models_dump.BdSerr):
    class Meta(models_dump.BdSerr.Meta):
        abstract = False


class BdSpath(models_dump.BdSpath):
    class Meta(models_dump.BdSpath.Meta):
        abstract = False


class BdSrep(models_dump.BdSrep):
    class Meta(models_dump.BdSrep.Meta):
        abstract = False


class Beolic(models_dump.Beolic):
    class Meta(models_dump.Beolic.Meta):
        abstract = False


class Bfextdbversion(models_dump.Bfextdbversion):
    class Meta(models_dump.Bfextdbversion.Meta):
        abstract = False


class Boemail(models_dump.Boemail):
    class Meta(models_dump.Boemail.Meta):
        abstract = False


class Boexcel(models_dump.Boexcel):
    class Meta(models_dump.Boexcel.Meta):
        abstract = False


class Booadm(models_dump.Booadm):
    class Meta(models_dump.Booadm.Meta):
        abstract = False


class Boodfs(models_dump.Boodfs):
    class Meta(models_dump.Boodfs.Meta):
        abstract = False


class Boostp(models_dump.Boostp):
    class Meta(models_dump.Boostp.Meta):
        abstract = False


class Boquot(models_dump.Boquot):
    class Meta(models_dump.Boquot.Meta):
        abstract = False


class Bosettings(models_dump.Bosettings):
    class Meta(models_dump.Bosettings.Meta):
        abstract = False


class Bostp1(models_dump.Bostp1):
    class Meta(models_dump.Bostp1.Meta):
        abstract = False


class Bosynclog(models_dump.Bosynclog):
    class Meta(models_dump.Bosynclog.Meta):
        abstract = False


class Boword(models_dump.Boword):
    class Meta(models_dump.Boword.Meta):
        abstract = False


class Bpcntr(models_dump.Bpcntr):
    class Meta(models_dump.Bpcntr.Meta):
        abstract = False


class Bprstrt(models_dump.Bprstrt):
    class Meta(models_dump.Bprstrt.Meta):
        abstract = False


class ChpstTg2Olgn(models_dump.ChpstTg2Olgn):
    class Meta(models_dump.ChpstTg2Olgn.Meta):
        abstract = False


class CksAdcfieldmap(models_dump.CksAdcfieldmap):
    class Meta(models_dump.CksAdcfieldmap.Meta):
        abstract = False


class CksAdckeywacc(models_dump.CksAdckeywacc):
    class Meta(models_dump.CksAdckeywacc.Meta):
        abstract = False


class CksAdckeywcst(models_dump.CksAdckeywcst):
    class Meta(models_dump.CksAdckeywcst.Meta):
        abstract = False


class CksAdckeywvat(models_dump.CksAdckeywvat):
    class Meta(models_dump.CksAdckeywvat.Meta):
        abstract = False


class CksAdcvalstatus(models_dump.CksAdcvalstatus):
    class Meta(models_dump.CksAdcvalstatus.Meta):
        abstract = False


class CksAdcvalvalues(models_dump.CksAdcvalvalues):
    class Meta(models_dump.CksAdcvalvalues.Meta):
        abstract = False


class CksAdrlog(models_dump.CksAdrlog):
    class Meta(models_dump.CksAdrlog.Meta):
        abstract = False


class CksAdrpackages(models_dump.CksAdrpackages):
    class Meta(models_dump.CksAdrpackages.Meta):
        abstract = False


class CksAdrpckgvlmps(models_dump.CksAdrpckgvlmps):
    class Meta(models_dump.CksAdrpckgvlmps.Meta):
        abstract = False


class CksAdrprfstacc(models_dump.CksAdrprfstacc):
    class Meta(models_dump.CksAdrprfstacc.Meta):
        abstract = False


class CksAdrprfstfrg(models_dump.CksAdrprfstfrg):
    class Meta(models_dump.CksAdrprfstfrg.Meta):
        abstract = False


class CksAdrprofiles(models_dump.CksAdrprofiles):
    class Meta(models_dump.CksAdrprofiles.Meta):
        abstract = False


class CksAdrprofset(models_dump.CksAdrprofset):
    class Meta(models_dump.CksAdrprofset.Meta):
        abstract = False


class CksAdrrectype(models_dump.CksAdrrectype):
    class Meta(models_dump.CksAdrrectype.Meta):
        abstract = False


class CksAdrsett(models_dump.CksAdrsett):
    class Meta(models_dump.CksAdrsett.Meta):
        abstract = False


class CksAdrtranslations(models_dump.CksAdrtranslations):
    class Meta(models_dump.CksAdrtranslations.Meta):
        abstract = False


class CksAprectype(models_dump.CksAprectype):
    class Meta(models_dump.CksAprectype.Meta):
        abstract = False


class CksArchive(models_dump.CksArchive):
    class Meta(models_dump.CksArchive.Meta):
        abstract = False


class CksArchivechngelog(models_dump.CksArchivechngelog):
    class Meta(models_dump.CksArchivechngelog.Meta):
        abstract = False


class CksArchiveexports(models_dump.CksArchiveexports):
    class Meta(models_dump.CksArchiveexports.Meta):
        abstract = False


class CksArchivelinkings(models_dump.CksArchivelinkings):
    class Meta(models_dump.CksArchivelinkings.Meta):
        abstract = False


class CksArchiveqm(models_dump.CksArchiveqm):
    class Meta(models_dump.CksArchiveqm.Meta):
        abstract = False


class CksArchivetype(models_dump.CksArchivetype):
    class Meta(models_dump.CksArchivetype.Meta):
        abstract = False


class CksAttachments(models_dump.CksAttachments):
    class Meta(models_dump.CksAttachments.Meta):
        abstract = False


class CksB1Toarchive(models_dump.CksB1Toarchive):
    class Meta(models_dump.CksB1Toarchive.Meta):
        abstract = False


class CksBarcodecali(models_dump.CksBarcodecali):
    class Meta(models_dump.CksBarcodecali.Meta):
        abstract = False


class CksBarcodeseqs(models_dump.CksBarcodeseqs):
    class Meta(models_dump.CksBarcodeseqs.Meta):
        abstract = False


class CksBrcdsqsrs(models_dump.CksBrcdsqsrs):
    class Meta(models_dump.CksBrcdsqsrs.Meta):
        abstract = False


class CksCategory(models_dump.CksCategory):
    class Meta(models_dump.CksCategory.Meta):
        abstract = False


class CksCustomsettings(models_dump.CksCustomsettings):
    class Meta(models_dump.CksCustomsettings.Meta):
        abstract = False


class CksDbconnections(models_dump.CksDbconnections):
    class Meta(models_dump.CksDbconnections.Meta):
        abstract = False


class CksDbversion(models_dump.CksDbversion):
    class Meta(models_dump.CksDbversion.Meta):
        abstract = False


class CksDmsscansett(models_dump.CksDmsscansett):
    class Meta(models_dump.CksDmsscansett.Meta):
        abstract = False


class CksDmssettings(models_dump.CksDmssettings):
    class Meta(models_dump.CksDmssettings.Meta):
        abstract = False


class CksDocreclan(models_dump.CksDocreclan):
    class Meta(models_dump.CksDocreclan.Meta):
        abstract = False


class CksDocrecpro(models_dump.CksDocrecpro):
    class Meta(models_dump.CksDocrecpro.Meta):
        abstract = False


class CksDocrecset(models_dump.CksDocrecset):
    class Meta(models_dump.CksDocrecset.Meta):
        abstract = False


class CksEinvoicelog(models_dump.CksEinvoicelog):
    class Meta(models_dump.CksEinvoicelog.Meta):
        abstract = False


class CksFiles(models_dump.CksFiles):
    class Meta(models_dump.CksFiles.Meta):
        abstract = False


class CksFormxml(models_dump.CksFormxml):
    class Meta(models_dump.CksFormxml.Meta):
        abstract = False


class CksGrpcatprmssns(models_dump.CksGrpcatprmssns):
    class Meta(models_dump.CksGrpcatprmssns.Meta):
        abstract = False


class CksGrpprcsprmssns(models_dump.CksGrpprcsprmssns):
    class Meta(models_dump.CksGrpprcsprmssns.Meta):
        abstract = False


class CksInstallpackage(models_dump.CksInstallpackage):
    class Meta(models_dump.CksInstallpackage.Meta):
        abstract = False


class CksLicense(models_dump.CksLicense):
    class Meta(models_dump.CksLicense.Meta):
        abstract = False


class CksLicenseuser(models_dump.CksLicenseuser):
    class Meta(models_dump.CksLicenseuser.Meta):
        abstract = False


class CksMaildocsettings(models_dump.CksMaildocsettings):
    class Meta(models_dump.CksMaildocsettings.Meta):
        abstract = False


class CksMaildoctransfer(models_dump.CksMaildoctransfer):
    class Meta(models_dump.CksMaildoctransfer.Meta):
        abstract = False


class CksMailedarchives(models_dump.CksMailedarchives):
    class Meta(models_dump.CksMailedarchives.Meta):
        abstract = False


class CksMailsettings(models_dump.CksMailsettings):
    class Meta(models_dump.CksMailsettings.Meta):
        abstract = False


class CksObjtypes(models_dump.CksObjtypes):
    class Meta(models_dump.CksObjtypes.Meta):
        abstract = False


class CksOutlooksettings(models_dump.CksOutlooksettings):
    class Meta(models_dump.CksOutlooksettings.Meta):
        abstract = False


class CksPackagedata(models_dump.CksPackagedata):
    class Meta(models_dump.CksPackagedata.Meta):
        abstract = False


class CksPdftypes(models_dump.CksPdftypes):
    class Meta(models_dump.CksPdftypes.Meta):
        abstract = False


class CksPrefixes(models_dump.CksPrefixes):
    class Meta(models_dump.CksPrefixes.Meta):
        abstract = False


class CksPresystems(models_dump.CksPresystems):
    class Meta(models_dump.CksPresystems.Meta):
        abstract = False


class CksPrintqueue(models_dump.CksPrintqueue):
    class Meta(models_dump.CksPrintqueue.Meta):
        abstract = False


class CksPrtdocsettings(models_dump.CksPrtdocsettings):
    class Meta(models_dump.CksPrtdocsettings.Meta):
        abstract = False


class CksPrtdoctransfer(models_dump.CksPrtdoctransfer):
    class Meta(models_dump.CksPrtdoctransfer.Meta):
        abstract = False


class CksRevisionarchive(models_dump.CksRevisionarchive):
    class Meta(models_dump.CksRevisionarchive.Meta):
        abstract = False


class CksScannercali(models_dump.CksScannercali):
    class Meta(models_dump.CksScannercali.Meta):
        abstract = False


class CksThumbnails(models_dump.CksThumbnails):
    class Meta(models_dump.CksThumbnails.Meta):
        abstract = False


class CksUseraccarch(models_dump.CksUseraccarch):
    class Meta(models_dump.CksUseraccarch.Meta):
        abstract = False


class CksUseraction(models_dump.CksUseraction):
    class Meta(models_dump.CksUseraction.Meta):
        abstract = False


class CksUserstamps(models_dump.CksUserstamps):
    class Meta(models_dump.CksUserstamps.Meta):
        abstract = False


class CksUsrcatprmssns(models_dump.CksUsrcatprmssns):
    class Meta(models_dump.CksUsrcatprmssns.Meta):
        abstract = False


class CksUsrprcsprmssns(models_dump.CksUsrprcsprmssns):
    class Meta(models_dump.CksUsrprcsprmssns.Meta):
        abstract = False


class CksVersions(models_dump.CksVersions):
    class Meta(models_dump.CksVersions.Meta):
        abstract = False


class CksWebpermissions(models_dump.CksWebpermissions):
    class Meta(models_dump.CksWebpermissions.Meta):
        abstract = False


class CksWebusers(models_dump.CksWebusers):
    class Meta(models_dump.CksWebusers.Meta):
        abstract = False


class CksZugferdlog(models_dump.CksZugferdlog):
    class Meta(models_dump.CksZugferdlog.Meta):
        abstract = False


class CksZugferdsett(models_dump.CksZugferdsett):
    class Meta(models_dump.CksZugferdsett.Meta):
        abstract = False


class CorAcctConf(models_dump.CorAcctConf):
    class Meta(models_dump.CorAcctConf.Meta):
        abstract = False


class CorBgtSales(models_dump.CorBgtSales):
    class Meta(models_dump.CorBgtSales.Meta):
        abstract = False


class CorBgtSalesscen(models_dump.CorBgtSalesscen):
    class Meta(models_dump.CorBgtSalesscen.Meta):
        abstract = False


class CorCorFs(models_dump.CorCorFs):
    class Meta(models_dump.CorCorFs.Meta):
        abstract = False


class CorCorLicence(models_dump.CorCorLicence):
    class Meta(models_dump.CorCorLicence.Meta):
        abstract = False


class CorCorModules(models_dump.CorCorModules):
    class Meta(models_dump.CorCorModules.Meta):
        abstract = False


class CorCorModulesuser(models_dump.CorCorModulesuser):
    class Meta(models_dump.CorCorModulesuser.Meta):
        abstract = False


class CorCorSettings(models_dump.CorCorSettings):
    class Meta(models_dump.CorCorSettings.Meta):
        abstract = False


class CorCustomConf(models_dump.CorCustomConf):
    class Meta(models_dump.CorCustomConf.Meta):
        abstract = False


class CorCustomEfcond(models_dump.CorCustomEfcond):
    class Meta(models_dump.CorCustomEfcond.Meta):
        abstract = False


class CorCustomEfrule(models_dump.CorCustomEfrule):
    class Meta(models_dump.CorCustomEfrule.Meta):
        abstract = False


class CorCustomField(models_dump.CorCustomField):
    class Meta(models_dump.CorCustomField.Meta):
        abstract = False


class CorCustomField2(models_dump.CorCustomField2):
    class Meta(models_dump.CorCustomField2.Meta):
        abstract = False


class CorCustomFuncb(models_dump.CorCustomFuncb):
    class Meta(models_dump.CorCustomFuncb.Meta):
        abstract = False


class CorCustomFuncb2(models_dump.CorCustomFuncb2):
    class Meta(models_dump.CorCustomFuncb2.Meta):
        abstract = False


class CorCustomGrid(models_dump.CorCustomGrid):
    class Meta(models_dump.CorCustomGrid.Meta):
        abstract = False


class CorCustomGridcol(models_dump.CorCustomGridcol):
    class Meta(models_dump.CorCustomGridcol.Meta):
        abstract = False


class CorCustomGriddesp(models_dump.CorCustomGriddesp):
    class Meta(models_dump.CorCustomGriddesp.Meta):
        abstract = False


class CorCustomGridefp(models_dump.CorCustomGridefp):
    class Meta(models_dump.CorCustomGridefp.Meta):
        abstract = False


class CorCustomGscript(models_dump.CorCustomGscript):
    class Meta(models_dump.CorCustomGscript.Meta):
        abstract = False


class CorCustomMdm(models_dump.CorCustomMdm):
    class Meta(models_dump.CorCustomMdm.Meta):
        abstract = False


class CorCustomNewitems(models_dump.CorCustomNewitems):
    class Meta(models_dump.CorCustomNewitems.Meta):
        abstract = False


class CorCustomNewmenu(models_dump.CorCustomNewmenu):
    class Meta(models_dump.CorCustomNewmenu.Meta):
        abstract = False


class CorCustomPlace(models_dump.CorCustomPlace):
    class Meta(models_dump.CorCustomPlace.Meta):
        abstract = False


class CorCustomRfact(models_dump.CorCustomRfact):
    class Meta(models_dump.CorCustomRfact.Meta):
        abstract = False


class CorCustomRfrow(models_dump.CorCustomRfrow):
    class Meta(models_dump.CorCustomRfrow.Meta):
        abstract = False


class CorCustomRfrule(models_dump.CorCustomRfrule):
    class Meta(models_dump.CorCustomRfrule.Meta):
        abstract = False


class CorCustomSvcext(models_dump.CorCustomSvcext):
    class Meta(models_dump.CorCustomSvcext.Meta):
        abstract = False


class CorCustomSvclog(models_dump.CorCustomSvclog):
    class Meta(models_dump.CorCustomSvclog.Meta):
        abstract = False


class CorCustomSvcrule(models_dump.CorCustomSvcrule):
    class Meta(models_dump.CorCustomSvcrule.Meta):
        abstract = False


class CorCustomSvcruleh(models_dump.CorCustomSvcruleh):
    class Meta(models_dump.CorCustomSvcruleh.Meta):
        abstract = False


class CorCustomSvcscode(models_dump.CorCustomSvcscode):
    class Meta(models_dump.CorCustomSvcscode.Meta):
        abstract = False


class CorCustomUdtconf(models_dump.CorCustomUdtconf):
    class Meta(models_dump.CorCustomUdtconf.Meta):
        abstract = False


class CorCustomUdtfld(models_dump.CorCustomUdtfld):
    class Meta(models_dump.CorCustomUdtfld.Meta):
        abstract = False


class CorCustomUdtparam(models_dump.CorCustomUdtparam):
    class Meta(models_dump.CorCustomUdtparam.Meta):
        abstract = False


class CorCustomUsrset(models_dump.CorCustomUsrset):
    class Meta(models_dump.CorCustomUsrset.Meta):
        abstract = False


class CorDoc6(models_dump.CorDoc6):
    class Meta(models_dump.CorDoc6.Meta):
        abstract = False


class CorFinAcctcalcdef(models_dump.CorFinAcctcalcdef):
    class Meta(models_dump.CorFinAcctcalcdef.Meta):
        abstract = False


class CorFinAcctcflow(models_dump.CorFinAcctcflow):
    class Meta(models_dump.CorFinAcctcflow.Meta):
        abstract = False


class CorFinAcctclass(models_dump.CorFinAcctclass):
    class Meta(models_dump.CorFinAcctclass.Meta):
        abstract = False


class CorFinAcctclsgrp(models_dump.CorFinAcctclsgrp):
    class Meta(models_dump.CorFinAcctclsgrp.Meta):
        abstract = False


class CorFinAcctclsprp(models_dump.CorFinAcctclsprp):
    class Meta(models_dump.CorFinAcctclsprp.Meta):
        abstract = False


class CorFinAcctcormsk(models_dump.CorFinAcctcormsk):
    class Meta(models_dump.CorFinAcctcormsk.Meta):
        abstract = False


class CorFinAcctgrpmsk(models_dump.CorFinAcctgrpmsk):
    class Meta(models_dump.CorFinAcctgrpmsk.Meta):
        abstract = False


class CorFinAcctgrpprp(models_dump.CorFinAcctgrpprp):
    class Meta(models_dump.CorFinAcctgrpprp.Meta):
        abstract = False


class CorKpi1(models_dump.CorKpi1):
    class Meta(models_dump.CorKpi1.Meta):
        abstract = False


class CorOkpi(models_dump.CorOkpi):
    class Meta(models_dump.CorOkpi.Meta):
        abstract = False


class CorSearchConf(models_dump.CorSearchConf):
    class Meta(models_dump.CorSearchConf.Meta):
        abstract = False


class CorSearchUsercs(models_dump.CorSearchUsercs):
    class Meta(models_dump.CorSearchUsercs.Meta):
        abstract = False


class CorSearchUserfs(models_dump.CorSearchUserfs):
    class Meta(models_dump.CorSearchUserfs.Meta):
        abstract = False


class CorSearchUserse(models_dump.CorSearchUserse):
    class Meta(models_dump.CorSearchUserse.Meta):
        abstract = False


class DatevSachverhalte(models_dump.DatevSachverhalte):
    class Meta(models_dump.DatevSachverhalte.Meta):
        abstract = False


class DolIntTelBlist(models_dump.DolIntTelBlist):
    class Meta(models_dump.DolIntTelBlist.Meta):
        abstract = False


class DolIntTelWlist(models_dump.DolIntTelWlist):
    class Meta(models_dump.DolIntTelWlist.Meta):
        abstract = False


class DolInvSettings(models_dump.DolInvSettings):
    class Meta(models_dump.DolInvSettings.Meta):
        abstract = False


class DolMainCountries(models_dump.DolMainCountries):
    class Meta(models_dump.DolMainCountries.Meta):
        abstract = False


class DolMainTemplates(models_dump.DolMainTemplates):
    class Meta(models_dump.DolMainTemplates.Meta):
        abstract = False


class DolMainZipCodes(models_dump.DolMainZipCodes):
    class Meta(models_dump.DolMainZipCodes.Meta):
        abstract = False


class DolMntActiNtfr(models_dump.DolMntActiNtfr):
    class Meta(models_dump.DolMntActiNtfr.Meta):
        abstract = False


class DolMntEmergCampa(models_dump.DolMntEmergCampa):
    u_group = models.ForeignKey(
        "DolMntNtfrGroups",
        models.DO_NOTHING,
        db_column="U_group",
        max_length=50,
        blank=True,
        null=True,
    )
    u_referenceto = models.ForeignKey(
        "DolMntEmergCampa",
        models.DO_NOTHING,
        db_column="U_referenceto",
        max_length=50,
        blank=True,
        null=True,
    )

    class Meta(models_dump.DolMntEmergCampa.Meta):
        abstract = False


class DolMntEmergLog(models_dump.DolMntEmergLog):
    u_campaignid = models.ForeignKey(
        "DolMntEmergCampa",
        models.DO_NOTHING,
        db_column="U_campaignid",
        max_length=50,
        blank=True,
        null=True,
    )
    u_custnr = models.ForeignKey(
        "Ocrd", models.DO_NOTHING, db_column="U_custnr", max_length=10
    )

    class Meta(models_dump.DolMntEmergLog.Meta):
        abstract = False


class DolMntItemSkip(models_dump.DolMntItemSkip):
    u_itemid = models.ForeignKey(
        "Oitm", models.DO_NOTHING, db_column="U_itemid", max_length=50
    )

    class Meta(models_dump.DolMntItemSkip.Meta):
        abstract = False


class DolMntNtfrGroups(models_dump.DolMntNtfrGroups):
    class Meta(models_dump.DolMntNtfrGroups.Meta):
        abstract = False


class DolMntProducts(models_dump.DolMntProducts):
    u_itemnr = models.ForeignKey(
        "Oitm", models.DO_NOTHING, db_column="U_itemnr", max_length=50
    )

    class Meta(models_dump.DolMntProducts.Meta):
        abstract = False


class DolMntTimeframe(models_dump.DolMntTimeframe):
    class Meta(models_dump.DolMntTimeframe.Meta):
        abstract = False


class DolTelAccessProd(models_dump.DolTelAccessProd):
    class Meta(models_dump.DolTelAccessProd.Meta):
        abstract = False


class DolTelAccessSprd(models_dump.DolTelAccessSprd):
    class Meta(models_dump.DolTelAccessSprd.Meta):
        abstract = False


class Sapdatevattach(models_dump.Sapdatevattach):
    class Meta(models_dump.Sapdatevattach.Meta):
        abstract = False


class Sapdatevconf(models_dump.Sapdatevconf):
    class Meta(models_dump.Sapdatevconf.Meta):
        abstract = False


class Sapdateverror(models_dump.Sapdateverror):
    class Meta(models_dump.Sapdateverror.Meta):
        abstract = False


class Sapdateverror2(models_dump.Sapdateverror2):
    class Meta(models_dump.Sapdateverror2.Meta):
        abstract = False


class Sapdateverror3(models_dump.Sapdateverror3):
    class Meta(models_dump.Sapdateverror3.Meta):
        abstract = False


class Sapdatevlines(models_dump.Sapdatevlines):
    class Meta(models_dump.Sapdatevlines.Meta):
        abstract = False


class Sapdatevlines2(models_dump.Sapdatevlines2):
    class Meta(models_dump.Sapdatevlines2.Meta):
        abstract = False


class Sapdatevlines3(models_dump.Sapdatevlines3):
    class Meta(models_dump.Sapdatevlines3.Meta):
        abstract = False


class Sapdatevrun(models_dump.Sapdatevrun):
    class Meta(models_dump.Sapdatevrun.Meta):
        abstract = False


class SwaLdBatchprint(models_dump.SwaLdBatchprint):
    class Meta(models_dump.SwaLdBatchprint.Meta):
        abstract = False


class SwaLdCategories(models_dump.SwaLdCategories):
    class Meta(models_dump.SwaLdCategories.Meta):
        abstract = False


class SwaLdFormtype(models_dump.SwaLdFormtype):
    class Meta(models_dump.SwaLdFormtype.Meta):
        abstract = False


class SwaLdInstpack(models_dump.SwaLdInstpack):
    class Meta(models_dump.SwaLdInstpack.Meta):
        abstract = False


class SwaLdLayout(models_dump.SwaLdLayout):
    class Meta(models_dump.SwaLdLayout.Meta):
        abstract = False


class SwaLdLayoutblob(models_dump.SwaLdLayoutblob):
    class Meta(models_dump.SwaLdLayoutblob.Meta):
        abstract = False


class SwaLdLayoutmenu(models_dump.SwaLdLayoutmenu):
    class Meta(models_dump.SwaLdLayoutmenu.Meta):
        abstract = False


class SwaLdLayoutmenuS(models_dump.SwaLdLayoutmenuS):
    class Meta(models_dump.SwaLdLayoutmenuS.Meta):
        abstract = False


class SwaLdLayoutparam(models_dump.SwaLdLayoutparam):
    class Meta(models_dump.SwaLdLayoutparam.Meta):
        abstract = False


class SwaLdLayoutperms(models_dump.SwaLdLayoutperms):
    class Meta(models_dump.SwaLdLayoutperms.Meta):
        abstract = False


class SwaLdLayoutprint(models_dump.SwaLdLayoutprint):
    class Meta(models_dump.SwaLdLayoutprint.Meta):
        abstract = False


class SwaLdLayoutsrc(models_dump.SwaLdLayoutsrc):
    class Meta(models_dump.SwaLdLayoutsrc.Meta):
        abstract = False


class SwaLdLayouttype(models_dump.SwaLdLayouttype):
    class Meta(models_dump.SwaLdLayouttype.Meta):
        abstract = False


class SwaLdMain(models_dump.SwaLdMain):
    class Meta(models_dump.SwaLdMain.Meta):
        abstract = False


class SwaLdParam(models_dump.SwaLdParam):
    class Meta(models_dump.SwaLdParam.Meta):
        abstract = False


class SwaLdParamftype(models_dump.SwaLdParamftype):
    class Meta(models_dump.SwaLdParamftype.Meta):
        abstract = False


class SwaLdParamhistory(models_dump.SwaLdParamhistory):
    class Meta(models_dump.SwaLdParamhistory.Meta):
        abstract = False


class SwaLdPkgblob(models_dump.SwaLdPkgblob):
    class Meta(models_dump.SwaLdPkgblob.Meta):
        abstract = False


class SwaLdPkghead(models_dump.SwaLdPkghead):
    class Meta(models_dump.SwaLdPkghead.Meta):
        abstract = False


class SwaLdPkglayout(models_dump.SwaLdPkglayout):
    class Meta(models_dump.SwaLdPkglayout.Meta):
        abstract = False


class SwaLdPkgprndef(models_dump.SwaLdPkgprndef):
    class Meta(models_dump.SwaLdPkgprndef.Meta):
        abstract = False


class SwaLdPkgsubscr2(models_dump.SwaLdPkgsubscr2):
    class Meta(models_dump.SwaLdPkgsubscr2.Meta):
        abstract = False


class SwaLdPrndef(models_dump.SwaLdPrndef):
    class Meta(models_dump.SwaLdPrndef.Meta):
        abstract = False


class SwaLdPrndefcard(models_dump.SwaLdPrndefcard):
    class Meta(models_dump.SwaLdPrndefcard.Meta):
        abstract = False


class SwaLdPrndeford(models_dump.SwaLdPrndeford):
    class Meta(models_dump.SwaLdPrndeford.Meta):
        abstract = False


class SwaLdStartlayouts(models_dump.SwaLdStartlayouts):
    class Meta(models_dump.SwaLdStartlayouts.Meta):
        abstract = False


class SwaLdSubscr2(models_dump.SwaLdSubscr2):
    class Meta(models_dump.SwaLdSubscr2.Meta):
        abstract = False


class SwaLdSubscrparam2(models_dump.SwaLdSubscrparam2):
    class Meta(models_dump.SwaLdSubscrparam2.Meta):
        abstract = False


class SwaLdSubscrtimer2(models_dump.SwaLdSubscrtimer2):
    class Meta(models_dump.SwaLdSubscrtimer2.Meta):
        abstract = False


class SwaLdText(models_dump.SwaLdText):
    class Meta(models_dump.SwaLdText.Meta):
        abstract = False


class SwaLicences(models_dump.SwaLicences):
    class Meta(models_dump.SwaLicences.Meta):
        abstract = False


class SwaModules(models_dump.SwaModules):
    class Meta(models_dump.SwaModules.Meta):
        abstract = False


class SwaSwaConf(models_dump.SwaSwaConf):
    class Meta(models_dump.SwaSwaConf.Meta):
        abstract = False


class Aaar(models_dump.Aaar):
    class Meta(models_dump.Aaar.Meta):
        abstract = False


class Aac1(models_dump.Aac1):
    class Meta(models_dump.Aac1.Meta):
        abstract = False


class Aacp(models_dump.Aacp):
    class Meta(models_dump.Aacp.Meta):
        abstract = False


class Aacs(models_dump.Aacs):
    class Meta(models_dump.Aacs.Meta):
        abstract = False


class Aact(models_dump.Aact):
    class Meta(models_dump.Aact.Meta):
        abstract = False


class Aad1(models_dump.Aad1):
    class Meta(models_dump.Aad1.Meta):
        abstract = False


class Aadm(models_dump.Aadm):
    class Meta(models_dump.Aadm.Meta):
        abstract = False


class Aadp(models_dump.Aadp):
    class Meta(models_dump.Aadp.Meta):
        abstract = False


class Aadt(models_dump.Aadt):
    class Meta(models_dump.Aadt.Meta):
        abstract = False


class Abat(models_dump.Abat):
    class Meta(models_dump.Abat.Meta):
        abstract = False


class Abfc(models_dump.Abfc):
    class Meta(models_dump.Abfc.Meta):
        abstract = False


class Abin(models_dump.Abin):
    class Meta(models_dump.Abin.Meta):
        abstract = False


class Abo1(models_dump.Abo1):
    class Meta(models_dump.Abo1.Meta):
        abstract = False


class Aboc(models_dump.Aboc):
    class Meta(models_dump.Aboc.Meta):
        abstract = False


class Aboe(models_dump.Aboe):
    class Meta(models_dump.Aboe.Meta):
        abstract = False


class Abp1(models_dump.Abp1):
    class Meta(models_dump.Abp1.Meta):
        abstract = False


class Abp2(models_dump.Abp2):
    class Meta(models_dump.Abp2.Meta):
        abstract = False


class Abpl(models_dump.Abpl):
    class Meta(models_dump.Abpl.Meta):
        abstract = False


class Absl(models_dump.Absl):
    class Meta(models_dump.Absl.Meta):
        abstract = False


class Abt1(models_dump.Abt1):
    class Meta(models_dump.Abt1.Meta):
        abstract = False


class Abtc(models_dump.Abtc):
    class Meta(models_dump.Abtc.Meta):
        abstract = False


class Abtn(models_dump.Abtn):
    class Meta(models_dump.Abtn.Meta):
        abstract = False


class Abtw(models_dump.Abtw):
    class Meta(models_dump.Abtw.Meta):
        abstract = False


class Acd1(models_dump.Acd1):
    class Meta(models_dump.Acd1.Meta):
        abstract = False


class Acd2(models_dump.Acd2):
    class Meta(models_dump.Acd2.Meta):
        abstract = False


class Acd3(models_dump.Acd3):
    class Meta(models_dump.Acd3.Meta):
        abstract = False


class Acem(models_dump.Acem):
    class Meta(models_dump.Acem.Meta):
        abstract = False


class Acest(models_dump.Acest):
    class Meta(models_dump.Acest.Meta):
        abstract = False


class Acfp(models_dump.Acfp):
    class Meta(models_dump.Acfp.Meta):
        abstract = False


class Ach1(models_dump.Ach1):
    class Meta(models_dump.Ach1.Meta):
        abstract = False


class Ach2(models_dump.Ach2):
    class Meta(models_dump.Ach2.Meta):
        abstract = False


class Ach3(models_dump.Ach3):
    class Meta(models_dump.Ach3.Meta):
        abstract = False


class Acho(models_dump.Acho):
    class Meta(models_dump.Acho.Meta):
        abstract = False


class Acl1(models_dump.Acl1):
    class Meta(models_dump.Acl1.Meta):
        abstract = False


class Acl2(models_dump.Acl2):
    class Meta(models_dump.Acl2.Meta):
        abstract = False


class Aclg(models_dump.Aclg):
    class Meta(models_dump.Aclg.Meta):
        abstract = False


class Acm1(models_dump.Acm1):
    class Meta(models_dump.Acm1.Meta):
        abstract = False


class Acp1(models_dump.Acp1):
    class Meta(models_dump.Acp1.Meta):
        abstract = False


class Acp2(models_dump.Acp2):
    class Meta(models_dump.Acp2.Meta):
        abstract = False


class Acp3(models_dump.Acp3):
    class Meta(models_dump.Acp3.Meta):
        abstract = False


class Acpa1(models_dump.Acpa1):
    class Meta(models_dump.Acpa1.Meta):
        abstract = False


class Acpn(models_dump.Acpn):
    class Meta(models_dump.Acpn.Meta):
        abstract = False


class Acpr(models_dump.Acpr):
    class Meta(models_dump.Acpr.Meta):
        abstract = False


class Acq1(models_dump.Acq1):
    class Meta(models_dump.Acq1.Meta):
        abstract = False


class Acq2(models_dump.Acq2):
    class Meta(models_dump.Acq2.Meta):
        abstract = False


class Acq3(models_dump.Acq3):
    class Meta(models_dump.Acq3.Meta):
        abstract = False


class Acr1(models_dump.Acr1):
    class Meta(models_dump.Acr1.Meta):
        abstract = False


class Acr11(models_dump.Acr11):
    class Meta(models_dump.Acr11.Meta):
        abstract = False


class Acr12(models_dump.Acr12):
    class Meta(models_dump.Acr12.Meta):
        abstract = False


class Acr13(models_dump.Acr13):
    class Meta(models_dump.Acr13.Meta):
        abstract = False


class Acr2(models_dump.Acr2):
    class Meta(models_dump.Acr2.Meta):
        abstract = False


class Acr3(models_dump.Acr3):
    class Meta(models_dump.Acr3.Meta):
        abstract = False


class Acr4(models_dump.Acr4):
    class Meta(models_dump.Acr4.Meta):
        abstract = False


class Acr5(models_dump.Acr5):
    class Meta(models_dump.Acr5.Meta):
        abstract = False


class Acr7(models_dump.Acr7):
    class Meta(models_dump.Acr7.Meta):
        abstract = False


class Acrb(models_dump.Acrb):
    class Meta(models_dump.Acrb.Meta):
        abstract = False


class Acrc(models_dump.Acrc):
    class Meta(models_dump.Acrc.Meta):
        abstract = False


class Acrd(models_dump.Acrd):
    class Meta(models_dump.Acrd.Meta):
        abstract = False


class Acs1(models_dump.Acs1):
    class Meta(models_dump.Acs1.Meta):
        abstract = False


class Act1(models_dump.Act1):
    class Meta(models_dump.Act1.Meta):
        abstract = False


class Act2(models_dump.Act2):
    class Meta(models_dump.Act2.Meta):
        abstract = False


class Actr(models_dump.Actr):
    class Meta(models_dump.Actr.Meta):
        abstract = False


class Adg1(models_dump.Adg1):
    class Meta(models_dump.Adg1.Meta):
        abstract = False


class Adm1(models_dump.Adm1):
    class Meta(models_dump.Adm1.Meta):
        abstract = False


class Adm2(models_dump.Adm2):
    class Meta(models_dump.Adm2.Meta):
        abstract = False


class Admc(models_dump.Admc):
    class Meta(models_dump.Admc.Meta):
        abstract = False


class Adnf(models_dump.Adnf):
    class Meta(models_dump.Adnf.Meta):
        abstract = False


class Ado1(models_dump.Ado1):
    class Meta(models_dump.Ado1.Meta):
        abstract = False


class Ado10(models_dump.Ado10):
    class Meta(models_dump.Ado10.Meta):
        abstract = False


class Ado11(models_dump.Ado11):
    class Meta(models_dump.Ado11.Meta):
        abstract = False


class Ado12(models_dump.Ado12):
    class Meta(models_dump.Ado12.Meta):
        abstract = False


class Ado13(models_dump.Ado13):
    class Meta(models_dump.Ado13.Meta):
        abstract = False


class Ado14(models_dump.Ado14):
    class Meta(models_dump.Ado14.Meta):
        abstract = False


class Ado15(models_dump.Ado15):
    class Meta(models_dump.Ado15.Meta):
        abstract = False


class Ado16(models_dump.Ado16):
    class Meta(models_dump.Ado16.Meta):
        abstract = False


class Ado17(models_dump.Ado17):
    class Meta(models_dump.Ado17.Meta):
        abstract = False


class Ado18(models_dump.Ado18):
    class Meta(models_dump.Ado18.Meta):
        abstract = False


class Ado19(models_dump.Ado19):
    class Meta(models_dump.Ado19.Meta):
        abstract = False


class Ado2(models_dump.Ado2):
    class Meta(models_dump.Ado2.Meta):
        abstract = False


class Ado20(models_dump.Ado20):
    class Meta(models_dump.Ado20.Meta):
        abstract = False


class Ado21(models_dump.Ado21):
    class Meta(models_dump.Ado21.Meta):
        abstract = False


class Ado22(models_dump.Ado22):
    class Meta(models_dump.Ado22.Meta):
        abstract = False


class Ado23(models_dump.Ado23):
    class Meta(models_dump.Ado23.Meta):
        abstract = False


class Ado24(models_dump.Ado24):
    class Meta(models_dump.Ado24.Meta):
        abstract = False


class Ado25(models_dump.Ado25):
    class Meta(models_dump.Ado25.Meta):
        abstract = False


class Ado26(models_dump.Ado26):
    class Meta(models_dump.Ado26.Meta):
        abstract = False


class Ado27(models_dump.Ado27):
    class Meta(models_dump.Ado27.Meta):
        abstract = False


class Ado28(models_dump.Ado28):
    class Meta(models_dump.Ado28.Meta):
        abstract = False


class Ado3(models_dump.Ado3):
    class Meta(models_dump.Ado3.Meta):
        abstract = False


class Ado4(models_dump.Ado4):
    class Meta(models_dump.Ado4.Meta):
        abstract = False


class Ado5(models_dump.Ado5):
    class Meta(models_dump.Ado5.Meta):
        abstract = False


class Ado6(models_dump.Ado6):
    class Meta(models_dump.Ado6.Meta):
        abstract = False


class Ado7(models_dump.Ado7):
    class Meta(models_dump.Ado7.Meta):
        abstract = False


class Ado8(models_dump.Ado8):
    class Meta(models_dump.Ado8.Meta):
        abstract = False


class Ado9(models_dump.Ado9):
    class Meta(models_dump.Ado9.Meta):
        abstract = False


class Adoc(models_dump.Adoc):
    class Meta(models_dump.Adoc.Meta):
        abstract = False


class Adp1(models_dump.Adp1):
    class Meta(models_dump.Adp1.Meta):
        abstract = False


class Adp2(models_dump.Adp2):
    class Meta(models_dump.Adp2.Meta):
        abstract = False


class Adpa(models_dump.Adpa):
    class Meta(models_dump.Adpa.Meta):
        abstract = False


class Adrc(models_dump.Adrc):
    class Meta(models_dump.Adrc.Meta):
        abstract = False


class Ads1(models_dump.Ads1):
    class Meta(models_dump.Ads1.Meta):
        abstract = False


class Adt1(models_dump.Adt1):
    class Meta(models_dump.Adt1.Meta):
        abstract = False


class Adtp(models_dump.Adtp):
    class Meta(models_dump.Adtp.Meta):
        abstract = False


class Aeb1(models_dump.Aeb1):
    class Meta(models_dump.Aeb1.Meta):
        abstract = False


class Aebk(models_dump.Aebk):
    class Meta(models_dump.Aebk.Meta):
        abstract = False


class Aec1(models_dump.Aec1):
    class Meta(models_dump.Aec1.Meta):
        abstract = False


class Aec2(models_dump.Aec2):
    class Meta(models_dump.Aec2.Meta):
        abstract = False


class Aec3(models_dump.Aec3):
    class Meta(models_dump.Aec3.Meta):
        abstract = False


class Aec4(models_dump.Aec4):
    class Meta(models_dump.Aec4.Meta):
        abstract = False


class Aec5(models_dump.Aec5):
    class Meta(models_dump.Aec5.Meta):
        abstract = False


class Aec6(models_dump.Aec6):
    class Meta(models_dump.Aec6.Meta):
        abstract = False


class Aec7(models_dump.Aec7):
    class Meta(models_dump.Aec7.Meta):
        abstract = False


class Aec8(models_dump.Aec8):
    class Meta(models_dump.Aec8.Meta):
        abstract = False


class Aecm(models_dump.Aecm):
    class Meta(models_dump.Aecm.Meta):
        abstract = False


class Aedg(models_dump.Aedg):
    class Meta(models_dump.Aedg.Meta):
        abstract = False


class Aek1(models_dump.Aek1):
    class Meta(models_dump.Aek1.Meta):
        abstract = False


class Aexd(models_dump.Aexd):
    class Meta(models_dump.Aexd.Meta):
        abstract = False


class Aext(models_dump.Aext):
    class Meta(models_dump.Aext.Meta):
        abstract = False


class Afa1(models_dump.Afa1):
    class Meta(models_dump.Afa1.Meta):
        abstract = False


class Afa2(models_dump.Afa2):
    class Meta(models_dump.Afa2.Meta):
        abstract = False


class Afad(models_dump.Afad):
    class Meta(models_dump.Afad.Meta):
        abstract = False


class Afm1(models_dump.Afm1):
    class Meta(models_dump.Afm1.Meta):
        abstract = False


class Afml(models_dump.Afml):
    class Meta(models_dump.Afml.Meta):
        abstract = False


class Afpr(models_dump.Afpr):
    class Meta(models_dump.Afpr.Meta):
        abstract = False


class Agar(models_dump.Agar):
    class Meta(models_dump.Agar.Meta):
        abstract = False


class Agrs(models_dump.Agrs):
    class Meta(models_dump.Agrs.Meta):
        abstract = False


class Ahe1(models_dump.Ahe1):
    class Meta(models_dump.Ahe1.Meta):
        abstract = False


class Ahe2(models_dump.Ahe2):
    class Meta(models_dump.Ahe2.Meta):
        abstract = False


class Ahe3(models_dump.Ahe3):
    class Meta(models_dump.Ahe3.Meta):
        abstract = False


class Ahe4(models_dump.Ahe4):
    class Meta(models_dump.Ahe4.Meta):
        abstract = False


class Ahe6(models_dump.Ahe6):
    class Meta(models_dump.Ahe6.Meta):
        abstract = False


class Ahe7(models_dump.Ahe7):
    class Meta(models_dump.Ahe7.Meta):
        abstract = False


class Ahem(models_dump.Ahem):
    class Meta(models_dump.Ahem.Meta):
        abstract = False


class Ahf1(models_dump.Ahf1):
    class Meta(models_dump.Ahf1.Meta):
        abstract = False


class Ahf2(models_dump.Ahf2):
    class Meta(models_dump.Ahf2.Meta):
        abstract = False


class Ahfc(models_dump.Ahfc):
    class Meta(models_dump.Ahfc.Meta):
        abstract = False


class Aht1(models_dump.Aht1):
    class Meta(models_dump.Aht1.Meta):
        abstract = False


class Ahtm(models_dump.Ahtm):
    class Meta(models_dump.Ahtm.Meta):
        abstract = False


class Aigw(models_dump.Aigw):
    class Meta(models_dump.Aigw.Meta):
        abstract = False


class Ain1(models_dump.Ain1):
    class Meta(models_dump.Ain1.Meta):
        abstract = False


class Ain10(models_dump.Ain10):
    class Meta(models_dump.Ain10.Meta):
        abstract = False


class Ain11(models_dump.Ain11):
    class Meta(models_dump.Ain11.Meta):
        abstract = False


class Ain12(models_dump.Ain12):
    class Meta(models_dump.Ain12.Meta):
        abstract = False


class Ain2(models_dump.Ain2):
    class Meta(models_dump.Ain2.Meta):
        abstract = False


class Ain3(models_dump.Ain3):
    class Meta(models_dump.Ain3.Meta):
        abstract = False


class Ain4(models_dump.Ain4):
    class Meta(models_dump.Ain4.Meta):
        abstract = False


class Ain5(models_dump.Ain5):
    class Meta(models_dump.Ain5.Meta):
        abstract = False


class Ain6(models_dump.Ain6):
    class Meta(models_dump.Ain6.Meta):
        abstract = False


class Ain7(models_dump.Ain7):
    class Meta(models_dump.Ain7.Meta):
        abstract = False


class Ain8(models_dump.Ain8):
    class Meta(models_dump.Ain8.Meta):
        abstract = False


class Ain9(models_dump.Ain9):
    class Meta(models_dump.Ain9.Meta):
        abstract = False


class Ainc(models_dump.Ainc):
    class Meta(models_dump.Ainc.Meta):
        abstract = False


class Ainf(models_dump.Ainf):
    class Meta(models_dump.Ainf.Meta):
        abstract = False


class Ains(models_dump.Ains):
    class Meta(models_dump.Ains.Meta):
        abstract = False


class Aiqi(models_dump.Aiqi):
    class Meta(models_dump.Aiqi.Meta):
        abstract = False


class Aiqr(models_dump.Aiqr):
    class Meta(models_dump.Aiqr.Meta):
        abstract = False


class Air1(models_dump.Air1):
    class Meta(models_dump.Air1.Meta):
        abstract = False


class Airi(models_dump.Airi):
    class Meta(models_dump.Airi.Meta):
        abstract = False


class Airr(models_dump.Airr):
    class Meta(models_dump.Airr.Meta):
        abstract = False


class Ais1(models_dump.Ais1):
    class Meta(models_dump.Ais1.Meta):
        abstract = False


class Ais2(models_dump.Ais2):
    class Meta(models_dump.Ais2.Meta):
        abstract = False


class Aisc(models_dump.Aisc):
    class Meta(models_dump.Aisc.Meta):
        abstract = False


class Aisd(models_dump.Aisd):
    class Meta(models_dump.Aisd.Meta):
        abstract = False


class Aisi(models_dump.Aisi):
    class Meta(models_dump.Aisi.Meta):
        abstract = False


class Ait1(models_dump.Ait1):
    class Meta(models_dump.Ait1.Meta):
        abstract = False


class Ait11(models_dump.Ait11):
    class Meta(models_dump.Ait11.Meta):
        abstract = False


class Ait13(models_dump.Ait13):
    class Meta(models_dump.Ait13.Meta):
        abstract = False


class Ait2(models_dump.Ait2):
    class Meta(models_dump.Ait2.Meta):
        abstract = False


class Ait3(models_dump.Ait3):
    class Meta(models_dump.Ait3.Meta):
        abstract = False


class Ait5(models_dump.Ait5):
    class Meta(models_dump.Ait5.Meta):
        abstract = False


class Ait6(models_dump.Ait6):
    class Meta(models_dump.Ait6.Meta):
        abstract = False


class Ait7(models_dump.Ait7):
    class Meta(models_dump.Ait7.Meta):
        abstract = False


class Ait8(models_dump.Ait8):
    class Meta(models_dump.Ait8.Meta):
        abstract = False


class Ait9(models_dump.Ait9):
    class Meta(models_dump.Ait9.Meta):
        abstract = False


class Aitb(models_dump.Aitb):
    class Meta(models_dump.Aitb.Meta):
        abstract = False


class Aitm(models_dump.Aitm):
    class Meta(models_dump.Aitm.Meta):
        abstract = False


class Aitt(models_dump.Aitt):
    class Meta(models_dump.Aitt.Meta):
        abstract = False


class Aitw(models_dump.Aitw):
    class Meta(models_dump.Aitw.Meta):
        abstract = False


class Ajd1(models_dump.Ajd1):
    class Meta(models_dump.Ajd1.Meta):
        abstract = False


class Ajd2(models_dump.Ajd2):
    class Meta(models_dump.Ajd2.Meta):
        abstract = False


class Ajdt(models_dump.Ajdt):
    class Meta(models_dump.Ajdt.Meta):
        abstract = False


class Akl1(models_dump.Akl1):
    class Meta(models_dump.Akl1.Meta):
        abstract = False


class Akl2(models_dump.Akl2):
    class Meta(models_dump.Akl2.Meta):
        abstract = False


class Alr1(models_dump.Alr1):
    class Meta(models_dump.Alr1.Meta):
        abstract = False


class Alr2(models_dump.Alr2):
    class Meta(models_dump.Alr2.Meta):
        abstract = False


class Alr3(models_dump.Alr3):
    class Meta(models_dump.Alr3.Meta):
        abstract = False


class Alt1(models_dump.Alt1):
    class Meta(models_dump.Alt1.Meta):
        abstract = False


class Alt2(models_dump.Alt2):
    class Meta(models_dump.Alt2.Meta):
        abstract = False


class Amd1(models_dump.Amd1):
    class Meta(models_dump.Amd1.Meta):
        abstract = False


class Amdr(models_dump.Amdr):
    class Meta(models_dump.Amdr.Meta):
        abstract = False


class Amdr1(models_dump.Amdr1):
    class Meta(models_dump.Amdr1.Meta):
        abstract = False


class Amgp(models_dump.Amgp):
    class Meta(models_dump.Amgp.Meta):
        abstract = False


class Amr1(models_dump.Amr1):
    class Meta(models_dump.Amr1.Meta):
        abstract = False


class Amr2(models_dump.Amr2):
    class Meta(models_dump.Amr2.Meta):
        abstract = False


class Amr3(models_dump.Amr3):
    class Meta(models_dump.Amr3.Meta):
        abstract = False


class Amr4(models_dump.Amr4):
    class Meta(models_dump.Amr4.Meta):
        abstract = False


class Amrv(models_dump.Amrv):
    class Meta(models_dump.Amrv.Meta):
        abstract = False


class Ancm(models_dump.Ancm):
    class Meta(models_dump.Ancm.Meta):
        abstract = False


class Ann1(models_dump.Ann1):
    class Meta(models_dump.Ann1.Meta):
        abstract = False


class Ann3(models_dump.Ann3):
    class Meta(models_dump.Ann3.Meta):
        abstract = False


class Annm(models_dump.Annm):
    class Meta(models_dump.Annm.Meta):
        abstract = False


class Ans1(models_dump.Ans1):
    class Meta(models_dump.Ans1.Meta):
        abstract = False


class Aoa1(models_dump.Aoa1):
    class Meta(models_dump.Aoa1.Meta):
        abstract = False


class Aoa2(models_dump.Aoa2):
    class Meta(models_dump.Aoa2.Meta):
        abstract = False


class Aoa3(models_dump.Aoa3):
    class Meta(models_dump.Aoa3.Meta):
        abstract = False


class Aoa4(models_dump.Aoa4):
    class Meta(models_dump.Aoa4.Meta):
        abstract = False


class Aoat(models_dump.Aoat):
    class Meta(models_dump.Aoat.Meta):
        abstract = False


class Aob1(models_dump.Aob1):
    class Meta(models_dump.Aob1.Meta):
        abstract = False


class Aoc1(models_dump.Aoc1):
    class Meta(models_dump.Aoc1.Meta):
        abstract = False


class Aocr(models_dump.Aocr):
    class Meta(models_dump.Aocr.Meta):
        abstract = False


class Aopr(models_dump.Aopr):
    class Meta(models_dump.Aopr.Meta):
        abstract = False


class Apfs(models_dump.Apfs):
    class Meta(models_dump.Apfs.Meta):
        abstract = False


class Aph1(models_dump.Aph1):
    class Meta(models_dump.Aph1.Meta):
        abstract = False


class Aph2(models_dump.Aph2):
    class Meta(models_dump.Aph2.Meta):
        abstract = False


class Aph3(models_dump.Aph3):
    class Meta(models_dump.Aph3.Meta):
        abstract = False


class Aph4(models_dump.Aph4):
    class Meta(models_dump.Aph4.Meta):
        abstract = False


class Aph5(models_dump.Aph5):
    class Meta(models_dump.Aph5.Meta):
        abstract = False


class Aph6(models_dump.Aph6):
    class Meta(models_dump.Aph6.Meta):
        abstract = False


class Aph7(models_dump.Aph7):
    class Meta(models_dump.Aph7.Meta):
        abstract = False


class Aph8(models_dump.Aph8):
    class Meta(models_dump.Aph8.Meta):
        abstract = False


class Apha(models_dump.Apha):
    class Meta(models_dump.Apha.Meta):
        abstract = False


class Apj1(models_dump.Apj1):
    class Meta(models_dump.Apj1.Meta):
        abstract = False


class Apj2(models_dump.Apj2):
    class Meta(models_dump.Apj2.Meta):
        abstract = False


class Apjt(models_dump.Apjt):
    class Meta(models_dump.Apjt.Meta):
        abstract = False


class Apkl(models_dump.Apkl):
    class Meta(models_dump.Apkl.Meta):
        abstract = False


class Apln(models_dump.Apln):
    class Meta(models_dump.Apln.Meta):
        abstract = False


class Apm1(models_dump.Apm1):
    class Meta(models_dump.Apm1.Meta):
        abstract = False


class Apm2(models_dump.Apm2):
    class Meta(models_dump.Apm2.Meta):
        abstract = False


class Apm3(models_dump.Apm3):
    class Meta(models_dump.Apm3.Meta):
        abstract = False


class Apm4(models_dump.Apm4):
    class Meta(models_dump.Apm4.Meta):
        abstract = False


class Apm5(models_dump.Apm5):
    class Meta(models_dump.Apm5.Meta):
        abstract = False


class Apm6(models_dump.Apm6):
    class Meta(models_dump.Apm6.Meta):
        abstract = False


class Apm7(models_dump.Apm7):
    class Meta(models_dump.Apm7.Meta):
        abstract = False


class Apm8(models_dump.Apm8):
    class Meta(models_dump.Apm8.Meta):
        abstract = False


class Apmg(models_dump.Apmg):
    class Meta(models_dump.Apmg.Meta):
        abstract = False


class Aprc(models_dump.Aprc):
    class Meta(models_dump.Aprc.Meta):
        abstract = False


class Aprj(models_dump.Aprj):
    class Meta(models_dump.Aprj.Meta):
        abstract = False


class Aqag(models_dump.Aqag):
    class Meta(models_dump.Aqag.Meta):
        abstract = False


class Aqi1(models_dump.Aqi1):
    class Meta(models_dump.Aqi1.Meta):
        abstract = False


class Aqi2(models_dump.Aqi2):
    class Meta(models_dump.Aqi2.Meta):
        abstract = False


class Aqi3(models_dump.Aqi3):
    class Meta(models_dump.Aqi3.Meta):
        abstract = False


class Aqr1(models_dump.Aqr1):
    class Meta(models_dump.Aqr1.Meta):
        abstract = False


class Aqr2(models_dump.Aqr2):
    class Meta(models_dump.Aqr2.Meta):
        abstract = False


class Aqr3(models_dump.Aqr3):
    class Meta(models_dump.Aqr3.Meta):
        abstract = False


class Aqr4(models_dump.Aqr4):
    class Meta(models_dump.Aqr4.Meta):
        abstract = False


class Aqr5(models_dump.Aqr5):
    class Meta(models_dump.Aqr5.Meta):
        abstract = False


class Arc1(models_dump.Arc1):
    class Meta(models_dump.Arc1.Meta):
        abstract = False


class Arc2(models_dump.Arc2):
    class Meta(models_dump.Arc2.Meta):
        abstract = False


class Arc3(models_dump.Arc3):
    class Meta(models_dump.Arc3.Meta):
        abstract = False


class Arc4(models_dump.Arc4):
    class Meta(models_dump.Arc4.Meta):
        abstract = False


class Arc5(models_dump.Arc5):
    class Meta(models_dump.Arc5.Meta):
        abstract = False


class Arc6(models_dump.Arc6):
    class Meta(models_dump.Arc6.Meta):
        abstract = False


class Arc7(models_dump.Arc7):
    class Meta(models_dump.Arc7.Meta):
        abstract = False


class Arc8(models_dump.Arc8):
    class Meta(models_dump.Arc8.Meta):
        abstract = False


class Arc9(models_dump.Arc9):
    class Meta(models_dump.Arc9.Meta):
        abstract = False


class Arct(models_dump.Arct):
    class Meta(models_dump.Arct.Meta):
        abstract = False


class Ari1(models_dump.Ari1):
    class Meta(models_dump.Ari1.Meta):
        abstract = False


class Arr1(models_dump.Arr1):
    class Meta(models_dump.Arr1.Meta):
        abstract = False


class Arsb(models_dump.Arsb):
    class Meta(models_dump.Arsb.Meta):
        abstract = False


class Arsc(models_dump.Arsc):
    class Meta(models_dump.Arsc.Meta):
        abstract = False


class Arsc1(models_dump.Arsc1):
    class Meta(models_dump.Arsc1.Meta):
        abstract = False


class Arsc2(models_dump.Arsc2):
    class Meta(models_dump.Arsc2.Meta):
        abstract = False


class Arsc3(models_dump.Arsc3):
    class Meta(models_dump.Arsc3.Meta):
        abstract = False


class Arsc4(models_dump.Arsc4):
    class Meta(models_dump.Arsc4.Meta):
        abstract = False


class Arsc5(models_dump.Arsc5):
    class Meta(models_dump.Arsc5.Meta):
        abstract = False


class Arsc6(models_dump.Arsc6):
    class Meta(models_dump.Arsc6.Meta):
        abstract = False


class Arst(models_dump.Arst):
    class Meta(models_dump.Arst.Meta):
        abstract = False


class Arts(models_dump.Arts):
    class Meta(models_dump.Arts.Meta):
        abstract = False


class Artt(models_dump.Artt):
    class Meta(models_dump.Artt.Meta):
        abstract = False


class Asc1(models_dump.Asc1):
    class Meta(models_dump.Asc1.Meta):
        abstract = False


class Asc2(models_dump.Asc2):
    class Meta(models_dump.Asc2.Meta):
        abstract = False


class Asc3(models_dump.Asc3):
    class Meta(models_dump.Asc3.Meta):
        abstract = False


class Asc4(models_dump.Asc4):
    class Meta(models_dump.Asc4.Meta):
        abstract = False


class Asc5(models_dump.Asc5):
    class Meta(models_dump.Asc5.Meta):
        abstract = False


class Asc6(models_dump.Asc6):
    class Meta(models_dump.Asc6.Meta):
        abstract = False


class Asc7(models_dump.Asc7):
    class Meta(models_dump.Asc7.Meta):
        abstract = False


class Ascl(models_dump.Ascl):
    class Meta(models_dump.Ascl.Meta):
        abstract = False


class Asgp(models_dump.Asgp):
    class Meta(models_dump.Asgp.Meta):
        abstract = False


class Ash1(models_dump.Ash1):
    class Meta(models_dump.Ash1.Meta):
        abstract = False


class Asi1(models_dump.Asi1):
    class Meta(models_dump.Asi1.Meta):
        abstract = False


class Asp1(models_dump.Asp1):
    class Meta(models_dump.Asp1.Meta):
        abstract = False


class Asp2(models_dump.Asp2):
    class Meta(models_dump.Asp2.Meta):
        abstract = False


class Aspp(models_dump.Aspp):
    class Meta(models_dump.Aspp.Meta):
        abstract = False


class Asql(models_dump.Asql):
    class Meta(models_dump.Asql.Meta):
        abstract = False


class Asrn(models_dump.Asrn):
    class Meta(models_dump.Asrn.Meta):
        abstract = False


class Ast1(models_dump.Ast1):
    class Meta(models_dump.Ast1.Meta):
        abstract = False


class Astc(models_dump.Astc):
    class Meta(models_dump.Astc.Meta):
        abstract = False


class Astt(models_dump.Astt):
    class Meta(models_dump.Astt.Meta):
        abstract = False


class Asuc(models_dump.Asuc):
    class Meta(models_dump.Asuc.Meta):
        abstract = False


class Asvm(models_dump.Asvm):
    class Meta(models_dump.Asvm.Meta):
        abstract = False


class Atc1(models_dump.Atc1):
    class Meta(models_dump.Atc1.Meta):
        abstract = False


class Athl(models_dump.Athl):
    class Meta(models_dump.Athl.Meta):
        abstract = False


class Atr1(models_dump.Atr1):
    class Meta(models_dump.Atr1.Meta):
        abstract = False


class Atro(models_dump.Atro):
    class Meta(models_dump.Atro.Meta):
        abstract = False


class Ats1(models_dump.Ats1):
    class Meta(models_dump.Ats1.Meta):
        abstract = False


class Atsc(models_dump.Atsc):
    class Meta(models_dump.Atsc.Meta):
        abstract = False


class Atsh(models_dump.Atsh):
    class Meta(models_dump.Atsh.Meta):
        abstract = False


class Atsp(models_dump.Atsp):
    class Meta(models_dump.Atsp.Meta):
        abstract = False


class Att1(models_dump.Att1):
    class Meta(models_dump.Att1.Meta):
        abstract = False


class Att2(models_dump.Att2):
    class Meta(models_dump.Att2.Meta):
        abstract = False


class Atx1(models_dump.Atx1):
    class Meta(models_dump.Atx1.Meta):
        abstract = False


class Atx2(models_dump.Atx2):
    class Meta(models_dump.Atx2.Meta):
        abstract = False


class Atx3(models_dump.Atx3):
    class Meta(models_dump.Atx3.Meta):
        abstract = False


class Atx4(models_dump.Atx4):
    class Meta(models_dump.Atx4.Meta):
        abstract = False


class Atxi(models_dump.Atxi):
    class Meta(models_dump.Atxi.Meta):
        abstract = False


class Aug1(models_dump.Aug1):
    class Meta(models_dump.Aug1.Meta):
        abstract = False


class Augp(models_dump.Augp):
    class Meta(models_dump.Augp.Meta):
        abstract = False


class Augr(models_dump.Augr):
    class Meta(models_dump.Augr.Meta):
        abstract = False


class Auncl(models_dump.Auncl):
    class Meta(models_dump.Auncl.Meta):
        abstract = False


class Auom(models_dump.Auom):
    class Meta(models_dump.Auom.Meta):
        abstract = False


class Auq1(models_dump.Auq1):
    class Meta(models_dump.Auq1.Meta):
        abstract = False


class Auqr(models_dump.Auqr):
    class Meta(models_dump.Auqr.Meta):
        abstract = False


class Ausr(models_dump.Ausr):
    class Meta(models_dump.Ausr.Meta):
        abstract = False


class Aveb(models_dump.Aveb):
    class Meta(models_dump.Aveb.Meta):
        abstract = False


class Avm1(models_dump.Avm1):
    class Meta(models_dump.Avm1.Meta):
        abstract = False


class Avm2(models_dump.Avm2):
    class Meta(models_dump.Avm2.Meta):
        abstract = False


class Avt1(models_dump.Avt1):
    class Meta(models_dump.Avt1.Meta):
        abstract = False


class Avtg(models_dump.Avtg):
    class Meta(models_dump.Avtg.Meta):
        abstract = False


class Awd1(models_dump.Awd1):
    class Meta(models_dump.Awd1.Meta):
        abstract = False


class Awd2(models_dump.Awd2):
    class Meta(models_dump.Awd2.Meta):
        abstract = False


class Awd3(models_dump.Awd3):
    class Meta(models_dump.Awd3.Meta):
        abstract = False


class Awd4(models_dump.Awd4):
    class Meta(models_dump.Awd4.Meta):
        abstract = False


class Awd5(models_dump.Awd5):
    class Meta(models_dump.Awd5.Meta):
        abstract = False


class Awex(models_dump.Awex):
    class Meta(models_dump.Awex.Meta):
        abstract = False


class Awfq(models_dump.Awfq):
    class Meta(models_dump.Awfq.Meta):
        abstract = False


class Awh1(models_dump.Awh1):
    class Meta(models_dump.Awh1.Meta):
        abstract = False


class Awh2(models_dump.Awh2):
    class Meta(models_dump.Awh2.Meta):
        abstract = False


class Awh3(models_dump.Awh3):
    class Meta(models_dump.Awh3.Meta):
        abstract = False


class Awhs(models_dump.Awhs):
    class Meta(models_dump.Awhs.Meta):
        abstract = False


class Awht(models_dump.Awht):
    class Meta(models_dump.Awht.Meta):
        abstract = False


class Awl1(models_dump.Awl1):
    class Meta(models_dump.Awl1.Meta):
        abstract = False


class Awl2(models_dump.Awl2):
    class Meta(models_dump.Awl2.Meta):
        abstract = False


class Awl3(models_dump.Awl3):
    class Meta(models_dump.Awl3.Meta):
        abstract = False


class Awl4(models_dump.Awl4):
    class Meta(models_dump.Awl4.Meta):
        abstract = False


class Awl5(models_dump.Awl5):
    class Meta(models_dump.Awl5.Meta):
        abstract = False


class Awls(models_dump.Awls):
    class Meta(models_dump.Awls.Meta):
        abstract = False


class Awmg(models_dump.Awmg):
    class Meta(models_dump.Awmg.Meta):
        abstract = False


class Awo1(models_dump.Awo1):
    class Meta(models_dump.Awo1.Meta):
        abstract = False


class Awo2(models_dump.Awo2):
    class Meta(models_dump.Awo2.Meta):
        abstract = False


class Awo3(models_dump.Awo3):
    class Meta(models_dump.Awo3.Meta):
        abstract = False


class Awo4(models_dump.Awo4):
    class Meta(models_dump.Awo4.Meta):
        abstract = False


class Awo5(models_dump.Awo5):
    class Meta(models_dump.Awo5.Meta):
        abstract = False


class Awor(models_dump.Awor):
    class Meta(models_dump.Awor.Meta):
        abstract = False


class Awtd(models_dump.Awtd):
    class Meta(models_dump.Awtd.Meta):
        abstract = False


class Awts(models_dump.Awts):
    class Meta(models_dump.Awts.Meta):
        abstract = False


class Awtt(models_dump.Awtt):
    class Meta(models_dump.Awtt.Meta):
        abstract = False


class Bal1(models_dump.Bal1):
    class Meta(models_dump.Bal1.Meta):
        abstract = False


class Bal2(models_dump.Bal2):
    class Meta(models_dump.Bal2.Meta):
        abstract = False


class Bgt1(models_dump.Bgt1):
    class Meta(models_dump.Bgt1.Meta):
        abstract = False


class Bgt2(models_dump.Bgt2):
    class Meta(models_dump.Bgt2.Meta):
        abstract = False


class Bgt3(models_dump.Bgt3):
    class Meta(models_dump.Bgt3.Meta):
        abstract = False


class Bnk1(models_dump.Bnk1):
    class Meta(models_dump.Bnk1.Meta):
        abstract = False


class Bnk2(models_dump.Bnk2):
    class Meta(models_dump.Bnk2.Meta):
        abstract = False


class Boc1(models_dump.Boc1):
    class Meta(models_dump.Boc1.Meta):
        abstract = False


class Boe1(models_dump.Boe1):
    class Meta(models_dump.Boe1.Meta):
        abstract = False


class Bot1(models_dump.Bot1):
    class Meta(models_dump.Bot1.Meta):
        abstract = False


class Box1(models_dump.Box1):
    class Meta(models_dump.Box1.Meta):
        abstract = False


class Box2(models_dump.Box2):
    class Meta(models_dump.Box2.Meta):
        abstract = False


class Box3(models_dump.Box3):
    class Meta(models_dump.Box3.Meta):
        abstract = False


class Box4(models_dump.Box4):
    class Meta(models_dump.Box4.Meta):
        abstract = False


class Bpl1(models_dump.Bpl1):
    class Meta(models_dump.Bpl1.Meta):
        abstract = False


class Bpl2(models_dump.Bpl2):
    class Meta(models_dump.Bpl2.Meta):
        abstract = False


class Bsj1(models_dump.Bsj1):
    class Meta(models_dump.Bsj1.Meta):
        abstract = False


class Bta1(models_dump.Bta1):
    class Meta(models_dump.Bta1.Meta):
        abstract = False


class Bta2(models_dump.Bta2):
    class Meta(models_dump.Bta2.Meta):
        abstract = False


class Btc1(models_dump.Btc1):
    class Meta(models_dump.Btc1.Meta):
        abstract = False


class Btf1(models_dump.Btf1):
    class Meta(models_dump.Btf1.Meta):
        abstract = False


class Btf2(models_dump.Btf2):
    class Meta(models_dump.Btf2.Meta):
        abstract = False


class Case(models_dump.Case):
    class Meta(models_dump.Case.Meta):
        abstract = False


class Case1(models_dump.Case1):
    class Meta(models_dump.Case1.Meta):
        abstract = False


class Ccal(models_dump.Ccal):
    class Meta(models_dump.Ccal.Meta):
        abstract = False


class Ccfg(models_dump.Ccfg):
    class Meta(models_dump.Ccfg.Meta):
        abstract = False


class Ccpd(models_dump.Ccpd):
    class Meta(models_dump.Ccpd.Meta):
        abstract = False


class Ccs1(models_dump.Ccs1):
    class Meta(models_dump.Ccs1.Meta):
        abstract = False


class Cdc1(models_dump.Cdc1):
    class Meta(models_dump.Cdc1.Meta):
        abstract = False


class Cdic(models_dump.Cdic):
    class Meta(models_dump.Cdic.Meta):
        abstract = False


class Cdpm(models_dump.Cdpm):
    class Meta(models_dump.Cdpm.Meta):
        abstract = False


class Cdro(models_dump.Cdro):
    class Meta(models_dump.Cdro.Meta):
        abstract = False


class Cdru(models_dump.Cdru):
    class Meta(models_dump.Cdru.Meta):
        abstract = False


class Cfh1(models_dump.Cfh1):
    class Meta(models_dump.Cfh1.Meta):
        abstract = False


class Cfn1(models_dump.Cfn1):
    class Meta(models_dump.Cfn1.Meta):
        abstract = False


class Cftc(models_dump.Cftc):
    class Meta(models_dump.Cftc.Meta):
        abstract = False


class Cfus(models_dump.Cfus):
    class Meta(models_dump.Cfus.Meta):
        abstract = False


class Cgev(models_dump.Cgev):
    class Meta(models_dump.Cgev.Meta):
        abstract = False


class Chd1(models_dump.Chd1):
    class Meta(models_dump.Chd1.Meta):
        abstract = False


class Chd2(models_dump.Chd2):
    class Meta(models_dump.Chd2.Meta):
        abstract = False


class Chen(models_dump.Chen):
    class Meta(models_dump.Chen.Meta):
        abstract = False


class Chfl(models_dump.Chfl):
    class Meta(models_dump.Chfl.Meta):
        abstract = False


class Cho1(models_dump.Cho1):
    class Meta(models_dump.Cho1.Meta):
        abstract = False


class Cho2(models_dump.Cho2):
    class Meta(models_dump.Cho2.Meta):
        abstract = False


class Cho3(models_dump.Cho3):
    class Meta(models_dump.Cho3.Meta):
        abstract = False


class Cif1(models_dump.Cif1):
    class Meta(models_dump.Cif1.Meta):
        abstract = False


class Cigr(models_dump.Cigr):
    class Meta(models_dump.Cigr.Meta):
        abstract = False


class Cin1(models_dump.Cin1):
    class Meta(models_dump.Cin1.Meta):
        abstract = False


class Cin10(models_dump.Cin10):
    class Meta(models_dump.Cin10.Meta):
        abstract = False


class Cin11(models_dump.Cin11):
    class Meta(models_dump.Cin11.Meta):
        abstract = False


class Cin12(models_dump.Cin12):
    class Meta(models_dump.Cin12.Meta):
        abstract = False


class Cin13(models_dump.Cin13):
    class Meta(models_dump.Cin13.Meta):
        abstract = False


class Cin14(models_dump.Cin14):
    class Meta(models_dump.Cin14.Meta):
        abstract = False


class Cin15(models_dump.Cin15):
    class Meta(models_dump.Cin15.Meta):
        abstract = False


class Cin16(models_dump.Cin16):
    class Meta(models_dump.Cin16.Meta):
        abstract = False


class Cin17(models_dump.Cin17):
    class Meta(models_dump.Cin17.Meta):
        abstract = False


class Cin18(models_dump.Cin18):
    class Meta(models_dump.Cin18.Meta):
        abstract = False


class Cin19(models_dump.Cin19):
    class Meta(models_dump.Cin19.Meta):
        abstract = False


class Cin2(models_dump.Cin2):
    class Meta(models_dump.Cin2.Meta):
        abstract = False


class Cin20(models_dump.Cin20):
    class Meta(models_dump.Cin20.Meta):
        abstract = False


class Cin21(models_dump.Cin21):
    class Meta(models_dump.Cin21.Meta):
        abstract = False


class Cin22(models_dump.Cin22):
    class Meta(models_dump.Cin22.Meta):
        abstract = False


class Cin23(models_dump.Cin23):
    class Meta(models_dump.Cin23.Meta):
        abstract = False


class Cin24(models_dump.Cin24):
    class Meta(models_dump.Cin24.Meta):
        abstract = False


class Cin25(models_dump.Cin25):
    class Meta(models_dump.Cin25.Meta):
        abstract = False


class Cin26(models_dump.Cin26):
    class Meta(models_dump.Cin26.Meta):
        abstract = False


class Cin27(models_dump.Cin27):
    class Meta(models_dump.Cin27.Meta):
        abstract = False


class Cin28(models_dump.Cin28):
    class Meta(models_dump.Cin28.Meta):
        abstract = False


class Cin3(models_dump.Cin3):
    class Meta(models_dump.Cin3.Meta):
        abstract = False


class Cin4(models_dump.Cin4):
    class Meta(models_dump.Cin4.Meta):
        abstract = False


class Cin5(models_dump.Cin5):
    class Meta(models_dump.Cin5.Meta):
        abstract = False


class Cin6(models_dump.Cin6):
    class Meta(models_dump.Cin6.Meta):
        abstract = False


class Cin7(models_dump.Cin7):
    class Meta(models_dump.Cin7.Meta):
        abstract = False


class Cin8(models_dump.Cin8):
    class Meta(models_dump.Cin8.Meta):
        abstract = False


class Cin9(models_dump.Cin9):
    class Meta(models_dump.Cin9.Meta):
        abstract = False


class Cinf(models_dump.Cinf):
    class Meta(models_dump.Cinf.Meta):
        abstract = False


class Civi(models_dump.Civi):
    class Meta(models_dump.Civi.Meta):
        abstract = False


class Clc1(models_dump.Clc1):
    class Meta(models_dump.Clc1.Meta):
        abstract = False


class Clg1(models_dump.Clg1):
    class Meta(models_dump.Clg1.Meta):
        abstract = False


class Clg2(models_dump.Clg2):
    class Meta(models_dump.Clg2.Meta):
        abstract = False


class Cpa1(models_dump.Cpa1):
    class Meta(models_dump.Cpa1.Meta):
        abstract = False


class Cpi1(models_dump.Cpi1):
    class Meta(models_dump.Cpi1.Meta):
        abstract = False


class Cpi10(models_dump.Cpi10):
    class Meta(models_dump.Cpi10.Meta):
        abstract = False


class Cpi11(models_dump.Cpi11):
    class Meta(models_dump.Cpi11.Meta):
        abstract = False


class Cpi12(models_dump.Cpi12):
    class Meta(models_dump.Cpi12.Meta):
        abstract = False


class Cpi13(models_dump.Cpi13):
    class Meta(models_dump.Cpi13.Meta):
        abstract = False


class Cpi14(models_dump.Cpi14):
    class Meta(models_dump.Cpi14.Meta):
        abstract = False


class Cpi15(models_dump.Cpi15):
    class Meta(models_dump.Cpi15.Meta):
        abstract = False


class Cpi16(models_dump.Cpi16):
    class Meta(models_dump.Cpi16.Meta):
        abstract = False


class Cpi17(models_dump.Cpi17):
    class Meta(models_dump.Cpi17.Meta):
        abstract = False


class Cpi18(models_dump.Cpi18):
    class Meta(models_dump.Cpi18.Meta):
        abstract = False


class Cpi19(models_dump.Cpi19):
    class Meta(models_dump.Cpi19.Meta):
        abstract = False


class Cpi2(models_dump.Cpi2):
    class Meta(models_dump.Cpi2.Meta):
        abstract = False


class Cpi20(models_dump.Cpi20):
    class Meta(models_dump.Cpi20.Meta):
        abstract = False


class Cpi21(models_dump.Cpi21):
    class Meta(models_dump.Cpi21.Meta):
        abstract = False


class Cpi22(models_dump.Cpi22):
    class Meta(models_dump.Cpi22.Meta):
        abstract = False


class Cpi23(models_dump.Cpi23):
    class Meta(models_dump.Cpi23.Meta):
        abstract = False


class Cpi24(models_dump.Cpi24):
    class Meta(models_dump.Cpi24.Meta):
        abstract = False


class Cpi25(models_dump.Cpi25):
    class Meta(models_dump.Cpi25.Meta):
        abstract = False


class Cpi26(models_dump.Cpi26):
    class Meta(models_dump.Cpi26.Meta):
        abstract = False


class Cpi27(models_dump.Cpi27):
    class Meta(models_dump.Cpi27.Meta):
        abstract = False


class Cpi28(models_dump.Cpi28):
    class Meta(models_dump.Cpi28.Meta):
        abstract = False


class Cpi3(models_dump.Cpi3):
    class Meta(models_dump.Cpi3.Meta):
        abstract = False


class Cpi4(models_dump.Cpi4):
    class Meta(models_dump.Cpi4.Meta):
        abstract = False


class Cpi5(models_dump.Cpi5):
    class Meta(models_dump.Cpi5.Meta):
        abstract = False


class Cpi6(models_dump.Cpi6):
    class Meta(models_dump.Cpi6.Meta):
        abstract = False


class Cpi7(models_dump.Cpi7):
    class Meta(models_dump.Cpi7.Meta):
        abstract = False


class Cpi8(models_dump.Cpi8):
    class Meta(models_dump.Cpi8.Meta):
        abstract = False


class Cpi9(models_dump.Cpi9):
    class Meta(models_dump.Cpi9.Meta):
        abstract = False


class Cpl1(models_dump.Cpl1):
    class Meta(models_dump.Cpl1.Meta):
        abstract = False


class Cpn1(models_dump.Cpn1):
    class Meta(models_dump.Cpn1.Meta):
        abstract = False


class Cpn2(models_dump.Cpn2):
    class Meta(models_dump.Cpn2.Meta):
        abstract = False


class Cpn3(models_dump.Cpn3):
    class Meta(models_dump.Cpn3.Meta):
        abstract = False


class Cprc(models_dump.Cprc):
    class Meta(models_dump.Cprc.Meta):
        abstract = False


class Cprf(models_dump.Cprf):
    class Meta(models_dump.Cprf.Meta):
        abstract = False


class Cpt1(models_dump.Cpt1):
    class Meta(models_dump.Cpt1.Meta):
        abstract = False


class Cpv1(models_dump.Cpv1):
    class Meta(models_dump.Cpv1.Meta):
        abstract = False


class Cpv10(models_dump.Cpv10):
    class Meta(models_dump.Cpv10.Meta):
        abstract = False


class Cpv11(models_dump.Cpv11):
    class Meta(models_dump.Cpv11.Meta):
        abstract = False


class Cpv12(models_dump.Cpv12):
    class Meta(models_dump.Cpv12.Meta):
        abstract = False


class Cpv13(models_dump.Cpv13):
    class Meta(models_dump.Cpv13.Meta):
        abstract = False


class Cpv14(models_dump.Cpv14):
    class Meta(models_dump.Cpv14.Meta):
        abstract = False


class Cpv15(models_dump.Cpv15):
    class Meta(models_dump.Cpv15.Meta):
        abstract = False


class Cpv16(models_dump.Cpv16):
    class Meta(models_dump.Cpv16.Meta):
        abstract = False


class Cpv17(models_dump.Cpv17):
    class Meta(models_dump.Cpv17.Meta):
        abstract = False


class Cpv18(models_dump.Cpv18):
    class Meta(models_dump.Cpv18.Meta):
        abstract = False


class Cpv19(models_dump.Cpv19):
    class Meta(models_dump.Cpv19.Meta):
        abstract = False


class Cpv2(models_dump.Cpv2):
    class Meta(models_dump.Cpv2.Meta):
        abstract = False


class Cpv20(models_dump.Cpv20):
    class Meta(models_dump.Cpv20.Meta):
        abstract = False


class Cpv21(models_dump.Cpv21):
    class Meta(models_dump.Cpv21.Meta):
        abstract = False


class Cpv22(models_dump.Cpv22):
    class Meta(models_dump.Cpv22.Meta):
        abstract = False


class Cpv23(models_dump.Cpv23):
    class Meta(models_dump.Cpv23.Meta):
        abstract = False


class Cpv24(models_dump.Cpv24):
    class Meta(models_dump.Cpv24.Meta):
        abstract = False


class Cpv25(models_dump.Cpv25):
    class Meta(models_dump.Cpv25.Meta):
        abstract = False


class Cpv26(models_dump.Cpv26):
    class Meta(models_dump.Cpv26.Meta):
        abstract = False


class Cpv27(models_dump.Cpv27):
    class Meta(models_dump.Cpv27.Meta):
        abstract = False


class Cpv28(models_dump.Cpv28):
    class Meta(models_dump.Cpv28.Meta):
        abstract = False


class Cpv3(models_dump.Cpv3):
    class Meta(models_dump.Cpv3.Meta):
        abstract = False


class Cpv4(models_dump.Cpv4):
    class Meta(models_dump.Cpv4.Meta):
        abstract = False


class Cpv5(models_dump.Cpv5):
    class Meta(models_dump.Cpv5.Meta):
        abstract = False


class Cpv6(models_dump.Cpv6):
    class Meta(models_dump.Cpv6.Meta):
        abstract = False


class Cpv7(models_dump.Cpv7):
    class Meta(models_dump.Cpv7.Meta):
        abstract = False


class Cpv8(models_dump.Cpv8):
    class Meta(models_dump.Cpv8.Meta):
        abstract = False


class Cpv9(models_dump.Cpv9):
    class Meta(models_dump.Cpv9.Meta):
        abstract = False


class Crd1(models_dump.Crd1):
    cardcode = models.ForeignKey(
        "Ocrd",
        models.DO_NOTHING,
        db_column="CardCode",
        max_length=15,
        blank=True,
        null=True,
    )

    class Meta(models_dump.Crd1.Meta):
        abstract = False


class Crd11(models_dump.Crd11):
    class Meta(models_dump.Crd11.Meta):
        abstract = False


class Crd12(models_dump.Crd12):
    class Meta(models_dump.Crd12.Meta):
        abstract = False


class Crd13(models_dump.Crd13):
    class Meta(models_dump.Crd13.Meta):
        abstract = False


class Crd2(models_dump.Crd2):
    class Meta(models_dump.Crd2.Meta):
        abstract = False


class Crd3(models_dump.Crd3):
    class Meta(models_dump.Crd3.Meta):
        abstract = False


class Crd4(models_dump.Crd4):
    class Meta(models_dump.Crd4.Meta):
        abstract = False


class Crd5(models_dump.Crd5):
    class Meta(models_dump.Crd5.Meta):
        abstract = False


class Crd6(models_dump.Crd6):
    class Meta(models_dump.Crd6.Meta):
        abstract = False


class Crd7(models_dump.Crd7):
    class Meta(models_dump.Crd7.Meta):
        abstract = False


class Crd8(models_dump.Crd8):
    class Meta(models_dump.Crd8.Meta):
        abstract = False


class Crd9(models_dump.Crd9):
    class Meta(models_dump.Crd9.Meta):
        abstract = False


class Crdc(models_dump.Crdc):
    class Meta(models_dump.Crdc.Meta):
        abstract = False


class Cry1(models_dump.Cry1):
    class Meta(models_dump.Cry1.Meta):
        abstract = False


class Cshs(models_dump.Cshs):
    class Meta(models_dump.Cshs.Meta):
        abstract = False


class Csi1(models_dump.Csi1):
    class Meta(models_dump.Csi1.Meta):
        abstract = False


class Csi10(models_dump.Csi10):
    class Meta(models_dump.Csi10.Meta):
        abstract = False


class Csi11(models_dump.Csi11):
    class Meta(models_dump.Csi11.Meta):
        abstract = False


class Csi12(models_dump.Csi12):
    class Meta(models_dump.Csi12.Meta):
        abstract = False


class Csi13(models_dump.Csi13):
    class Meta(models_dump.Csi13.Meta):
        abstract = False


class Csi14(models_dump.Csi14):
    class Meta(models_dump.Csi14.Meta):
        abstract = False


class Csi15(models_dump.Csi15):
    class Meta(models_dump.Csi15.Meta):
        abstract = False


class Csi16(models_dump.Csi16):
    class Meta(models_dump.Csi16.Meta):
        abstract = False


class Csi17(models_dump.Csi17):
    class Meta(models_dump.Csi17.Meta):
        abstract = False


class Csi18(models_dump.Csi18):
    class Meta(models_dump.Csi18.Meta):
        abstract = False


class Csi19(models_dump.Csi19):
    class Meta(models_dump.Csi19.Meta):
        abstract = False


class Csi2(models_dump.Csi2):
    class Meta(models_dump.Csi2.Meta):
        abstract = False


class Csi20(models_dump.Csi20):
    class Meta(models_dump.Csi20.Meta):
        abstract = False


class Csi21(models_dump.Csi21):
    class Meta(models_dump.Csi21.Meta):
        abstract = False


class Csi22(models_dump.Csi22):
    class Meta(models_dump.Csi22.Meta):
        abstract = False


class Csi23(models_dump.Csi23):
    class Meta(models_dump.Csi23.Meta):
        abstract = False


class Csi24(models_dump.Csi24):
    class Meta(models_dump.Csi24.Meta):
        abstract = False


class Csi25(models_dump.Csi25):
    class Meta(models_dump.Csi25.Meta):
        abstract = False


class Csi26(models_dump.Csi26):
    class Meta(models_dump.Csi26.Meta):
        abstract = False


class Csi27(models_dump.Csi27):
    class Meta(models_dump.Csi27.Meta):
        abstract = False


class Csi28(models_dump.Csi28):
    class Meta(models_dump.Csi28.Meta):
        abstract = False


class Csi3(models_dump.Csi3):
    class Meta(models_dump.Csi3.Meta):
        abstract = False


class Csi4(models_dump.Csi4):
    class Meta(models_dump.Csi4.Meta):
        abstract = False


class Csi5(models_dump.Csi5):
    class Meta(models_dump.Csi5.Meta):
        abstract = False


class Csi6(models_dump.Csi6):
    class Meta(models_dump.Csi6.Meta):
        abstract = False


class Csi7(models_dump.Csi7):
    class Meta(models_dump.Csi7.Meta):
        abstract = False


class Csi8(models_dump.Csi8):
    class Meta(models_dump.Csi8.Meta):
        abstract = False


class Csi9(models_dump.Csi9):
    class Meta(models_dump.Csi9.Meta):
        abstract = False


class Csn1(models_dump.Csn1):
    class Meta(models_dump.Csn1.Meta):
        abstract = False


class Cspi(models_dump.Cspi):
    class Meta(models_dump.Cspi.Meta):
        abstract = False


class Cstn(models_dump.Cstn):
    class Meta(models_dump.Cstn.Meta):
        abstract = False


class Csv1(models_dump.Csv1):
    class Meta(models_dump.Csv1.Meta):
        abstract = False


class Csv10(models_dump.Csv10):
    class Meta(models_dump.Csv10.Meta):
        abstract = False


class Csv11(models_dump.Csv11):
    class Meta(models_dump.Csv11.Meta):
        abstract = False


class Csv12(models_dump.Csv12):
    class Meta(models_dump.Csv12.Meta):
        abstract = False


class Csv13(models_dump.Csv13):
    class Meta(models_dump.Csv13.Meta):
        abstract = False


class Csv14(models_dump.Csv14):
    class Meta(models_dump.Csv14.Meta):
        abstract = False


class Csv15(models_dump.Csv15):
    class Meta(models_dump.Csv15.Meta):
        abstract = False


class Csv16(models_dump.Csv16):
    class Meta(models_dump.Csv16.Meta):
        abstract = False


class Csv17(models_dump.Csv17):
    class Meta(models_dump.Csv17.Meta):
        abstract = False


class Csv18(models_dump.Csv18):
    class Meta(models_dump.Csv18.Meta):
        abstract = False


class Csv19(models_dump.Csv19):
    class Meta(models_dump.Csv19.Meta):
        abstract = False


class Csv2(models_dump.Csv2):
    class Meta(models_dump.Csv2.Meta):
        abstract = False


class Csv20(models_dump.Csv20):
    class Meta(models_dump.Csv20.Meta):
        abstract = False


class Csv21(models_dump.Csv21):
    class Meta(models_dump.Csv21.Meta):
        abstract = False


class Csv22(models_dump.Csv22):
    class Meta(models_dump.Csv22.Meta):
        abstract = False


class Csv23(models_dump.Csv23):
    class Meta(models_dump.Csv23.Meta):
        abstract = False


class Csv24(models_dump.Csv24):
    class Meta(models_dump.Csv24.Meta):
        abstract = False


class Csv25(models_dump.Csv25):
    class Meta(models_dump.Csv25.Meta):
        abstract = False


class Csv26(models_dump.Csv26):
    class Meta(models_dump.Csv26.Meta):
        abstract = False


class Csv27(models_dump.Csv27):
    class Meta(models_dump.Csv27.Meta):
        abstract = False


class Csv28(models_dump.Csv28):
    class Meta(models_dump.Csv28.Meta):
        abstract = False


class Csv3(models_dump.Csv3):
    class Meta(models_dump.Csv3.Meta):
        abstract = False


class Csv4(models_dump.Csv4):
    class Meta(models_dump.Csv4.Meta):
        abstract = False


class Csv5(models_dump.Csv5):
    class Meta(models_dump.Csv5.Meta):
        abstract = False


class Csv6(models_dump.Csv6):
    class Meta(models_dump.Csv6.Meta):
        abstract = False


class Csv7(models_dump.Csv7):
    class Meta(models_dump.Csv7.Meta):
        abstract = False


class Csv8(models_dump.Csv8):
    class Meta(models_dump.Csv8.Meta):
        abstract = False


class Csv9(models_dump.Csv9):
    class Meta(models_dump.Csv9.Meta):
        abstract = False


class Ctbr(models_dump.Ctbr):
    class Meta(models_dump.Ctbr.Meta):
        abstract = False


class Ctg1(models_dump.Ctg1):
    class Meta(models_dump.Ctg1.Meta):
        abstract = False


class Ctns(models_dump.Ctns):
    class Meta(models_dump.Ctns.Meta):
        abstract = False


class Ctr1(models_dump.Ctr1):
    class Meta(models_dump.Ctr1.Meta):
        abstract = False


class Ctr2(models_dump.Ctr2):
    class Meta(models_dump.Ctr2.Meta):
        abstract = False


class Cudc(models_dump.Cudc):
    class Meta(models_dump.Cudc.Meta):
        abstract = False


class Cufd(models_dump.Cufd):
    class Meta(models_dump.Cufd.Meta):
        abstract = False


class Cul1(models_dump.Cul1):
    class Meta(models_dump.Cul1.Meta):
        abstract = False


class Culg(models_dump.Culg):
    class Meta(models_dump.Culg.Meta):
        abstract = False


class Cumf(models_dump.Cumf):
    class Meta(models_dump.Cumf.Meta):
        abstract = False


class Cumi(models_dump.Cumi):
    class Meta(models_dump.Cumi.Meta):
        abstract = False


class Cupc(models_dump.Cupc):
    class Meta(models_dump.Cupc.Meta):
        abstract = False


class Cuvv(models_dump.Cuvv):
    class Meta(models_dump.Cuvv.Meta):
        abstract = False


class Dab1(models_dump.Dab1):
    class Meta(models_dump.Dab1.Meta):
        abstract = False


class Dadb(models_dump.Dadb):
    class Meta(models_dump.Dadb.Meta):
        abstract = False


class Dal1(models_dump.Dal1):
    class Meta(models_dump.Dal1.Meta):
        abstract = False


class Dar1(models_dump.Dar1):
    class Meta(models_dump.Dar1.Meta):
        abstract = False


class Dar2(models_dump.Dar2):
    class Meta(models_dump.Dar2.Meta):
        abstract = False


class Dar3(models_dump.Dar3):
    class Meta(models_dump.Dar3.Meta):
        abstract = False


class Dar4(models_dump.Dar4):
    class Meta(models_dump.Dar4.Meta):
        abstract = False


class Datb(models_dump.Datb):
    class Meta(models_dump.Datb.Meta):
        abstract = False


class Dbadm(models_dump.Dbadm):
    class Meta(models_dump.Dbadm.Meta):
        abstract = False


class Ddt1(models_dump.Ddt1):
    class Meta(models_dump.Ddt1.Meta):
        abstract = False


class Dgp1(models_dump.Dgp1):
    class Meta(models_dump.Dgp1.Meta):
        abstract = False


class Dgp2(models_dump.Dgp2):
    class Meta(models_dump.Dgp2.Meta):
        abstract = False


class Dgp3(models_dump.Dgp3):
    class Meta(models_dump.Dgp3.Meta):
        abstract = False


class Dgp4(models_dump.Dgp4):
    class Meta(models_dump.Dgp4.Meta):
        abstract = False


class Dgp5(models_dump.Dgp5):
    class Meta(models_dump.Dgp5.Meta):
        abstract = False


class Dln1(models_dump.Dln1):
    class Meta(models_dump.Dln1.Meta):
        abstract = False


class Dln10(models_dump.Dln10):
    class Meta(models_dump.Dln10.Meta):
        abstract = False


class Dln11(models_dump.Dln11):
    class Meta(models_dump.Dln11.Meta):
        abstract = False


class Dln12(models_dump.Dln12):
    class Meta(models_dump.Dln12.Meta):
        abstract = False


class Dln13(models_dump.Dln13):
    class Meta(models_dump.Dln13.Meta):
        abstract = False


class Dln14(models_dump.Dln14):
    class Meta(models_dump.Dln14.Meta):
        abstract = False


class Dln15(models_dump.Dln15):
    class Meta(models_dump.Dln15.Meta):
        abstract = False


class Dln16(models_dump.Dln16):
    class Meta(models_dump.Dln16.Meta):
        abstract = False


class Dln17(models_dump.Dln17):
    class Meta(models_dump.Dln17.Meta):
        abstract = False


class Dln18(models_dump.Dln18):
    class Meta(models_dump.Dln18.Meta):
        abstract = False


class Dln19(models_dump.Dln19):
    class Meta(models_dump.Dln19.Meta):
        abstract = False


class Dln2(models_dump.Dln2):
    class Meta(models_dump.Dln2.Meta):
        abstract = False


class Dln20(models_dump.Dln20):
    class Meta(models_dump.Dln20.Meta):
        abstract = False


class Dln21(models_dump.Dln21):
    class Meta(models_dump.Dln21.Meta):
        abstract = False


class Dln22(models_dump.Dln22):
    class Meta(models_dump.Dln22.Meta):
        abstract = False


class Dln23(models_dump.Dln23):
    class Meta(models_dump.Dln23.Meta):
        abstract = False


class Dln24(models_dump.Dln24):
    class Meta(models_dump.Dln24.Meta):
        abstract = False


class Dln25(models_dump.Dln25):
    class Meta(models_dump.Dln25.Meta):
        abstract = False


class Dln26(models_dump.Dln26):
    class Meta(models_dump.Dln26.Meta):
        abstract = False


class Dln27(models_dump.Dln27):
    class Meta(models_dump.Dln27.Meta):
        abstract = False


class Dln28(models_dump.Dln28):
    class Meta(models_dump.Dln28.Meta):
        abstract = False


class Dln3(models_dump.Dln3):
    class Meta(models_dump.Dln3.Meta):
        abstract = False


class Dln4(models_dump.Dln4):
    class Meta(models_dump.Dln4.Meta):
        abstract = False


class Dln5(models_dump.Dln5):
    class Meta(models_dump.Dln5.Meta):
        abstract = False


class Dln6(models_dump.Dln6):
    class Meta(models_dump.Dln6.Meta):
        abstract = False


class Dln7(models_dump.Dln7):
    class Meta(models_dump.Dln7.Meta):
        abstract = False


class Dln8(models_dump.Dln8):
    class Meta(models_dump.Dln8.Meta):
        abstract = False


class Dln9(models_dump.Dln9):
    class Meta(models_dump.Dln9.Meta):
        abstract = False


class Dmw1(models_dump.Dmw1):
    class Meta(models_dump.Dmw1.Meta):
        abstract = False


class Doc20(models_dump.Doc20):
    class Meta(models_dump.Doc20.Meta):
        abstract = False


class Doc21(models_dump.Doc21):
    class Meta(models_dump.Doc21.Meta):
        abstract = False


class Doc23(models_dump.Doc23):
    class Meta(models_dump.Doc23.Meta):
        abstract = False


class Doc24(models_dump.Doc24):
    class Meta(models_dump.Doc24.Meta):
        abstract = False


class Doc28(models_dump.Doc28):
    class Meta(models_dump.Doc28.Meta):
        abstract = False


class Dpi1(models_dump.Dpi1):
    class Meta(models_dump.Dpi1.Meta):
        abstract = False


class Dpi10(models_dump.Dpi10):
    class Meta(models_dump.Dpi10.Meta):
        abstract = False


class Dpi11(models_dump.Dpi11):
    class Meta(models_dump.Dpi11.Meta):
        abstract = False


class Dpi12(models_dump.Dpi12):
    class Meta(models_dump.Dpi12.Meta):
        abstract = False


class Dpi13(models_dump.Dpi13):
    class Meta(models_dump.Dpi13.Meta):
        abstract = False


class Dpi14(models_dump.Dpi14):
    class Meta(models_dump.Dpi14.Meta):
        abstract = False


class Dpi15(models_dump.Dpi15):
    class Meta(models_dump.Dpi15.Meta):
        abstract = False


class Dpi16(models_dump.Dpi16):
    class Meta(models_dump.Dpi16.Meta):
        abstract = False


class Dpi17(models_dump.Dpi17):
    class Meta(models_dump.Dpi17.Meta):
        abstract = False


class Dpi18(models_dump.Dpi18):
    class Meta(models_dump.Dpi18.Meta):
        abstract = False


class Dpi19(models_dump.Dpi19):
    class Meta(models_dump.Dpi19.Meta):
        abstract = False


class Dpi2(models_dump.Dpi2):
    class Meta(models_dump.Dpi2.Meta):
        abstract = False


class Dpi20(models_dump.Dpi20):
    class Meta(models_dump.Dpi20.Meta):
        abstract = False


class Dpi21(models_dump.Dpi21):
    class Meta(models_dump.Dpi21.Meta):
        abstract = False


class Dpi22(models_dump.Dpi22):
    class Meta(models_dump.Dpi22.Meta):
        abstract = False


class Dpi23(models_dump.Dpi23):
    class Meta(models_dump.Dpi23.Meta):
        abstract = False


class Dpi24(models_dump.Dpi24):
    class Meta(models_dump.Dpi24.Meta):
        abstract = False


class Dpi25(models_dump.Dpi25):
    class Meta(models_dump.Dpi25.Meta):
        abstract = False


class Dpi26(models_dump.Dpi26):
    class Meta(models_dump.Dpi26.Meta):
        abstract = False


class Dpi27(models_dump.Dpi27):
    class Meta(models_dump.Dpi27.Meta):
        abstract = False


class Dpi28(models_dump.Dpi28):
    class Meta(models_dump.Dpi28.Meta):
        abstract = False


class Dpi3(models_dump.Dpi3):
    class Meta(models_dump.Dpi3.Meta):
        abstract = False


class Dpi4(models_dump.Dpi4):
    class Meta(models_dump.Dpi4.Meta):
        abstract = False


class Dpi5(models_dump.Dpi5):
    class Meta(models_dump.Dpi5.Meta):
        abstract = False


class Dpi6(models_dump.Dpi6):
    class Meta(models_dump.Dpi6.Meta):
        abstract = False


class Dpi7(models_dump.Dpi7):
    class Meta(models_dump.Dpi7.Meta):
        abstract = False


class Dpi8(models_dump.Dpi8):
    class Meta(models_dump.Dpi8.Meta):
        abstract = False


class Dpi9(models_dump.Dpi9):
    class Meta(models_dump.Dpi9.Meta):
        abstract = False


class Dpo1(models_dump.Dpo1):
    class Meta(models_dump.Dpo1.Meta):
        abstract = False


class Dpo10(models_dump.Dpo10):
    class Meta(models_dump.Dpo10.Meta):
        abstract = False


class Dpo11(models_dump.Dpo11):
    class Meta(models_dump.Dpo11.Meta):
        abstract = False


class Dpo12(models_dump.Dpo12):
    class Meta(models_dump.Dpo12.Meta):
        abstract = False


class Dpo13(models_dump.Dpo13):
    class Meta(models_dump.Dpo13.Meta):
        abstract = False


class Dpo14(models_dump.Dpo14):
    class Meta(models_dump.Dpo14.Meta):
        abstract = False


class Dpo15(models_dump.Dpo15):
    class Meta(models_dump.Dpo15.Meta):
        abstract = False


class Dpo16(models_dump.Dpo16):
    class Meta(models_dump.Dpo16.Meta):
        abstract = False


class Dpo17(models_dump.Dpo17):
    class Meta(models_dump.Dpo17.Meta):
        abstract = False


class Dpo18(models_dump.Dpo18):
    class Meta(models_dump.Dpo18.Meta):
        abstract = False


class Dpo19(models_dump.Dpo19):
    class Meta(models_dump.Dpo19.Meta):
        abstract = False


class Dpo2(models_dump.Dpo2):
    class Meta(models_dump.Dpo2.Meta):
        abstract = False


class Dpo20(models_dump.Dpo20):
    class Meta(models_dump.Dpo20.Meta):
        abstract = False


class Dpo21(models_dump.Dpo21):
    class Meta(models_dump.Dpo21.Meta):
        abstract = False


class Dpo22(models_dump.Dpo22):
    class Meta(models_dump.Dpo22.Meta):
        abstract = False


class Dpo23(models_dump.Dpo23):
    class Meta(models_dump.Dpo23.Meta):
        abstract = False


class Dpo24(models_dump.Dpo24):
    class Meta(models_dump.Dpo24.Meta):
        abstract = False


class Dpo25(models_dump.Dpo25):
    class Meta(models_dump.Dpo25.Meta):
        abstract = False


class Dpo26(models_dump.Dpo26):
    class Meta(models_dump.Dpo26.Meta):
        abstract = False


class Dpo27(models_dump.Dpo27):
    class Meta(models_dump.Dpo27.Meta):
        abstract = False


class Dpo28(models_dump.Dpo28):
    class Meta(models_dump.Dpo28.Meta):
        abstract = False


class Dpo3(models_dump.Dpo3):
    class Meta(models_dump.Dpo3.Meta):
        abstract = False


class Dpo4(models_dump.Dpo4):
    class Meta(models_dump.Dpo4.Meta):
        abstract = False


class Dpo5(models_dump.Dpo5):
    class Meta(models_dump.Dpo5.Meta):
        abstract = False


class Dpo6(models_dump.Dpo6):
    class Meta(models_dump.Dpo6.Meta):
        abstract = False


class Dpo7(models_dump.Dpo7):
    class Meta(models_dump.Dpo7.Meta):
        abstract = False


class Dpo8(models_dump.Dpo8):
    class Meta(models_dump.Dpo8.Meta):
        abstract = False


class Dpo9(models_dump.Dpo9):
    class Meta(models_dump.Dpo9.Meta):
        abstract = False


class Dps1(models_dump.Dps1):
    class Meta(models_dump.Dps1.Meta):
        abstract = False


class Drf1(models_dump.Drf1):
    class Meta(models_dump.Drf1.Meta):
        abstract = False


class Drf10(models_dump.Drf10):
    class Meta(models_dump.Drf10.Meta):
        abstract = False


class Drf11(models_dump.Drf11):
    class Meta(models_dump.Drf11.Meta):
        abstract = False


class Drf12(models_dump.Drf12):
    class Meta(models_dump.Drf12.Meta):
        abstract = False


class Drf13(models_dump.Drf13):
    class Meta(models_dump.Drf13.Meta):
        abstract = False


class Drf14(models_dump.Drf14):
    class Meta(models_dump.Drf14.Meta):
        abstract = False


class Drf15(models_dump.Drf15):
    class Meta(models_dump.Drf15.Meta):
        abstract = False


class Drf16(models_dump.Drf16):
    class Meta(models_dump.Drf16.Meta):
        abstract = False


class Drf17(models_dump.Drf17):
    class Meta(models_dump.Drf17.Meta):
        abstract = False


class Drf18(models_dump.Drf18):
    class Meta(models_dump.Drf18.Meta):
        abstract = False


class Drf19(models_dump.Drf19):
    class Meta(models_dump.Drf19.Meta):
        abstract = False


class Drf2(models_dump.Drf2):
    class Meta(models_dump.Drf2.Meta):
        abstract = False


class Drf20(models_dump.Drf20):
    class Meta(models_dump.Drf20.Meta):
        abstract = False


class Drf21(models_dump.Drf21):
    class Meta(models_dump.Drf21.Meta):
        abstract = False


class Drf22(models_dump.Drf22):
    class Meta(models_dump.Drf22.Meta):
        abstract = False


class Drf23(models_dump.Drf23):
    class Meta(models_dump.Drf23.Meta):
        abstract = False


class Drf24(models_dump.Drf24):
    class Meta(models_dump.Drf24.Meta):
        abstract = False


class Drf25(models_dump.Drf25):
    class Meta(models_dump.Drf25.Meta):
        abstract = False


class Drf26(models_dump.Drf26):
    class Meta(models_dump.Drf26.Meta):
        abstract = False


class Drf27(models_dump.Drf27):
    class Meta(models_dump.Drf27.Meta):
        abstract = False


class Drf28(models_dump.Drf28):
    class Meta(models_dump.Drf28.Meta):
        abstract = False


class Drf3(models_dump.Drf3):
    class Meta(models_dump.Drf3.Meta):
        abstract = False


class Drf4(models_dump.Drf4):
    class Meta(models_dump.Drf4.Meta):
        abstract = False


class Drf5(models_dump.Drf5):
    class Meta(models_dump.Drf5.Meta):
        abstract = False


class Drf6(models_dump.Drf6):
    class Meta(models_dump.Drf6.Meta):
        abstract = False


class Drf7(models_dump.Drf7):
    class Meta(models_dump.Drf7.Meta):
        abstract = False


class Drf8(models_dump.Drf8):
    class Meta(models_dump.Drf8.Meta):
        abstract = False


class Drf9(models_dump.Drf9):
    class Meta(models_dump.Drf9.Meta):
        abstract = False


class Drn1(models_dump.Drn1):
    class Meta(models_dump.Drn1.Meta):
        abstract = False


class Drn2(models_dump.Drn2):
    class Meta(models_dump.Drn2.Meta):
        abstract = False


class Dsc1(models_dump.Dsc1):
    class Meta(models_dump.Dsc1.Meta):
        abstract = False


class Dtp1(models_dump.Dtp1):
    class Meta(models_dump.Dtp1.Meta):
        abstract = False


class Dut1(models_dump.Dut1):
    class Meta(models_dump.Dut1.Meta):
        abstract = False


class Dwz1(models_dump.Dwz1):
    class Meta(models_dump.Dwz1.Meta):
        abstract = False


class Dwz2(models_dump.Dwz2):
    class Meta(models_dump.Dwz2.Meta):
        abstract = False


class Dwz3(models_dump.Dwz3):
    class Meta(models_dump.Dwz3.Meta):
        abstract = False


class Dwz4(models_dump.Dwz4):
    class Meta(models_dump.Dwz4.Meta):
        abstract = False


class Dwz5(models_dump.Dwz5):
    class Meta(models_dump.Dwz5.Meta):
        abstract = False


class Ebk1(models_dump.Ebk1):
    class Meta(models_dump.Ebk1.Meta):
        abstract = False


class Ebl1(models_dump.Ebl1):
    class Meta(models_dump.Ebl1.Meta):
        abstract = False


class Ecdw1(models_dump.Ecdw1):
    class Meta(models_dump.Ecdw1.Meta):
        abstract = False


class Ecm1(models_dump.Ecm1):
    class Meta(models_dump.Ecm1.Meta):
        abstract = False


class Ecm2(models_dump.Ecm2):
    class Meta(models_dump.Ecm2.Meta):
        abstract = False


class Ecm3(models_dump.Ecm3):
    class Meta(models_dump.Ecm3.Meta):
        abstract = False


class Ecm4(models_dump.Ecm4):
    class Meta(models_dump.Ecm4.Meta):
        abstract = False


class Ecm5(models_dump.Ecm5):
    class Meta(models_dump.Ecm5.Meta):
        abstract = False


class Ecm6(models_dump.Ecm6):
    class Meta(models_dump.Ecm6.Meta):
        abstract = False


class Ecm7(models_dump.Ecm7):
    class Meta(models_dump.Ecm7.Meta):
        abstract = False


class Ecm8(models_dump.Ecm8):
    class Meta(models_dump.Ecm8.Meta):
        abstract = False


class Edg1(models_dump.Edg1):
    class Meta(models_dump.Edg1.Meta):
        abstract = False


class Efdw1(models_dump.Efdw1):
    class Meta(models_dump.Efdw1.Meta):
        abstract = False


class Ejb1(models_dump.Ejb1):
    class Meta(models_dump.Ejb1.Meta):
        abstract = False


class Ejb2(models_dump.Ejb2):
    class Meta(models_dump.Ejb2.Meta):
        abstract = False


class Ejd1(models_dump.Ejd1):
    class Meta(models_dump.Ejd1.Meta):
        abstract = False


class Eml1(models_dump.Eml1):
    class Meta(models_dump.Eml1.Meta):
        abstract = False


class Ent1(models_dump.Ent1):
    class Meta(models_dump.Ent1.Meta):
        abstract = False


class Eoy1(models_dump.Eoy1):
    class Meta(models_dump.Eoy1.Meta):
        abstract = False


class Erx1(models_dump.Erx1):
    class Meta(models_dump.Erx1.Meta):
        abstract = False


class Faa1(models_dump.Faa1):
    class Meta(models_dump.Faa1.Meta):
        abstract = False


class Fac1(models_dump.Fac1):
    class Meta(models_dump.Fac1.Meta):
        abstract = False


class Fac2(models_dump.Fac2):
    class Meta(models_dump.Fac2.Meta):
        abstract = False


class Fam1(models_dump.Fam1):
    class Meta(models_dump.Fam1.Meta):
        abstract = False


class Far1(models_dump.Far1):
    class Meta(models_dump.Far1.Meta):
        abstract = False


class Fct1(models_dump.Fct1):
    class Meta(models_dump.Fct1.Meta):
        abstract = False


class Fix1(models_dump.Fix1):
    class Meta(models_dump.Fix1.Meta):
        abstract = False


class Flr1(models_dump.Flr1):
    class Meta(models_dump.Flr1.Meta):
        abstract = False


class Flt1(models_dump.Flt1):
    class Meta(models_dump.Flt1.Meta):
        abstract = False


class Fml1(models_dump.Fml1):
    class Meta(models_dump.Fml1.Meta):
        abstract = False


class Fnd1(models_dump.Fnd1):
    class Meta(models_dump.Fnd1.Meta):
        abstract = False


class Fns1(models_dump.Fns1):
    class Meta(models_dump.Fns1.Meta):
        abstract = False


class Frc1(models_dump.Frc1):
    class Meta(models_dump.Frc1.Meta):
        abstract = False


class Ftr1(models_dump.Ftr1):
    class Meta(models_dump.Ftr1.Meta):
        abstract = False


class Ftr2(models_dump.Ftr2):
    class Meta(models_dump.Ftr2.Meta):
        abstract = False


class Ftr3(models_dump.Ftr3):
    class Meta(models_dump.Ftr3.Meta):
        abstract = False


class Ftt1(models_dump.Ftt1):
    class Meta(models_dump.Ftt1.Meta):
        abstract = False


class Gbi1(models_dump.Gbi1):
    class Meta(models_dump.Gbi1.Meta):
        abstract = False


class Gbi10(models_dump.Gbi10):
    class Meta(models_dump.Gbi10.Meta):
        abstract = False


class Gbi11(models_dump.Gbi11):
    class Meta(models_dump.Gbi11.Meta):
        abstract = False


class Gbi12(models_dump.Gbi12):
    class Meta(models_dump.Gbi12.Meta):
        abstract = False


class Gbi13(models_dump.Gbi13):
    class Meta(models_dump.Gbi13.Meta):
        abstract = False


class Gbi14(models_dump.Gbi14):
    class Meta(models_dump.Gbi14.Meta):
        abstract = False


class Gbi15(models_dump.Gbi15):
    class Meta(models_dump.Gbi15.Meta):
        abstract = False


class Gbi16(models_dump.Gbi16):
    class Meta(models_dump.Gbi16.Meta):
        abstract = False


class Gbi2(models_dump.Gbi2):
    class Meta(models_dump.Gbi2.Meta):
        abstract = False


class Gbi3(models_dump.Gbi3):
    class Meta(models_dump.Gbi3.Meta):
        abstract = False


class Gbi4(models_dump.Gbi4):
    class Meta(models_dump.Gbi4.Meta):
        abstract = False


class Gbi5(models_dump.Gbi5):
    class Meta(models_dump.Gbi5.Meta):
        abstract = False


class Gbi6(models_dump.Gbi6):
    class Meta(models_dump.Gbi6.Meta):
        abstract = False


class Gbi7(models_dump.Gbi7):
    class Meta(models_dump.Gbi7.Meta):
        abstract = False


class Gbi8(models_dump.Gbi8):
    class Meta(models_dump.Gbi8.Meta):
        abstract = False


class Gbi9(models_dump.Gbi9):
    class Meta(models_dump.Gbi9.Meta):
        abstract = False


class Gdp1(models_dump.Gdp1):
    class Meta(models_dump.Gdp1.Meta):
        abstract = False


class Gdp2(models_dump.Gdp2):
    class Meta(models_dump.Gdp2.Meta):
        abstract = False


class Gdp3(models_dump.Gdp3):
    class Meta(models_dump.Gdp3.Meta):
        abstract = False


class Gfl1(models_dump.Gfl1):
    class Meta(models_dump.Gfl1.Meta):
        abstract = False


class Gfl2(models_dump.Gfl2):
    class Meta(models_dump.Gfl2.Meta):
        abstract = False


class Gpa1(models_dump.Gpa1):
    class Meta(models_dump.Gpa1.Meta):
        abstract = False


class Gpa2(models_dump.Gpa2):
    class Meta(models_dump.Gpa2.Meta):
        abstract = False


class Gpa3(models_dump.Gpa3):
    class Meta(models_dump.Gpa3.Meta):
        abstract = False


class Gpa4(models_dump.Gpa4):
    class Meta(models_dump.Gpa4.Meta):
        abstract = False


class Gpa5(models_dump.Gpa5):
    class Meta(models_dump.Gpa5.Meta):
        abstract = False


class Gpa6(models_dump.Gpa6):
    class Meta(models_dump.Gpa6.Meta):
        abstract = False


class Gpa7(models_dump.Gpa7):
    class Meta(models_dump.Gpa7.Meta):
        abstract = False


class Gpa8(models_dump.Gpa8):
    class Meta(models_dump.Gpa8.Meta):
        abstract = False


class Gpa9(models_dump.Gpa9):
    class Meta(models_dump.Gpa9.Meta):
        abstract = False


class Gpc1(models_dump.Gpc1):
    class Meta(models_dump.Gpc1.Meta):
        abstract = False


class Gti1(models_dump.Gti1):
    class Meta(models_dump.Gti1.Meta):
        abstract = False


class Gtm1(models_dump.Gtm1):
    class Meta(models_dump.Gtm1.Meta):
        abstract = False


class Hem1(models_dump.Hem1):
    class Meta(models_dump.Hem1.Meta):
        abstract = False


class Hem10(models_dump.Hem10):
    class Meta(models_dump.Hem10.Meta):
        abstract = False


class Hem2(models_dump.Hem2):
    class Meta(models_dump.Hem2.Meta):
        abstract = False


class Hem3(models_dump.Hem3):
    class Meta(models_dump.Hem3.Meta):
        abstract = False


class Hem4(models_dump.Hem4):
    class Meta(models_dump.Hem4.Meta):
        abstract = False


class Hem5(models_dump.Hem5):
    class Meta(models_dump.Hem5.Meta):
        abstract = False


class Hem6(models_dump.Hem6):
    class Meta(models_dump.Hem6.Meta):
        abstract = False


class Hem7(models_dump.Hem7):
    class Meta(models_dump.Hem7.Meta):
        abstract = False


class Het1(models_dump.Het1):
    class Meta(models_dump.Het1.Meta):
        abstract = False


class Hfc1(models_dump.Hfc1):
    class Meta(models_dump.Hfc1.Meta):
        abstract = False


class Hfc2(models_dump.Hfc2):
    class Meta(models_dump.Hfc2.Meta):
        abstract = False


class Hld1(models_dump.Hld1):
    class Meta(models_dump.Hld1.Meta):
        abstract = False


class Hmm1(models_dump.Hmm1):
    class Meta(models_dump.Hmm1.Meta):
        abstract = False


class Hmm2(models_dump.Hmm2):
    class Meta(models_dump.Hmm2.Meta):
        abstract = False


class Hmm3(models_dump.Hmm3):
    class Meta(models_dump.Hmm3.Meta):
        abstract = False


class Htm1(models_dump.Htm1):
    class Meta(models_dump.Htm1.Meta):
        abstract = False


class Icd1(models_dump.Icd1):
    class Meta(models_dump.Icd1.Meta):
        abstract = False


class Icd10(models_dump.Icd10):
    class Meta(models_dump.Icd10.Meta):
        abstract = False


class Icd11(models_dump.Icd11):
    class Meta(models_dump.Icd11.Meta):
        abstract = False


class Icd12(models_dump.Icd12):
    class Meta(models_dump.Icd12.Meta):
        abstract = False


class Icd2(models_dump.Icd2):
    class Meta(models_dump.Icd2.Meta):
        abstract = False


class Icd3(models_dump.Icd3):
    class Meta(models_dump.Icd3.Meta):
        abstract = False


class Icd4(models_dump.Icd4):
    class Meta(models_dump.Icd4.Meta):
        abstract = False


class Icd5(models_dump.Icd5):
    class Meta(models_dump.Icd5.Meta):
        abstract = False


class Icd6(models_dump.Icd6):
    class Meta(models_dump.Icd6.Meta):
        abstract = False


class Icd7(models_dump.Icd7):
    class Meta(models_dump.Icd7.Meta):
        abstract = False


class Icd8(models_dump.Icd8):
    class Meta(models_dump.Icd8.Meta):
        abstract = False


class Icd9(models_dump.Icd9):
    class Meta(models_dump.Icd9.Meta):
        abstract = False


class Iei1(models_dump.Iei1):
    class Meta(models_dump.Iei1.Meta):
        abstract = False


class Iei10(models_dump.Iei10):
    class Meta(models_dump.Iei10.Meta):
        abstract = False


class Iei11(models_dump.Iei11):
    class Meta(models_dump.Iei11.Meta):
        abstract = False


class Iei12(models_dump.Iei12):
    class Meta(models_dump.Iei12.Meta):
        abstract = False


class Iei13(models_dump.Iei13):
    class Meta(models_dump.Iei13.Meta):
        abstract = False


class Iei14(models_dump.Iei14):
    class Meta(models_dump.Iei14.Meta):
        abstract = False


class Iei15(models_dump.Iei15):
    class Meta(models_dump.Iei15.Meta):
        abstract = False


class Iei16(models_dump.Iei16):
    class Meta(models_dump.Iei16.Meta):
        abstract = False


class Iei17(models_dump.Iei17):
    class Meta(models_dump.Iei17.Meta):
        abstract = False


class Iei18(models_dump.Iei18):
    class Meta(models_dump.Iei18.Meta):
        abstract = False


class Iei19(models_dump.Iei19):
    class Meta(models_dump.Iei19.Meta):
        abstract = False


class Iei2(models_dump.Iei2):
    class Meta(models_dump.Iei2.Meta):
        abstract = False


class Iei20(models_dump.Iei20):
    class Meta(models_dump.Iei20.Meta):
        abstract = False


class Iei21(models_dump.Iei21):
    class Meta(models_dump.Iei21.Meta):
        abstract = False


class Iei22(models_dump.Iei22):
    class Meta(models_dump.Iei22.Meta):
        abstract = False


class Iei23(models_dump.Iei23):
    class Meta(models_dump.Iei23.Meta):
        abstract = False


class Iei24(models_dump.Iei24):
    class Meta(models_dump.Iei24.Meta):
        abstract = False


class Iei25(models_dump.Iei25):
    class Meta(models_dump.Iei25.Meta):
        abstract = False


class Iei26(models_dump.Iei26):
    class Meta(models_dump.Iei26.Meta):
        abstract = False


class Iei27(models_dump.Iei27):
    class Meta(models_dump.Iei27.Meta):
        abstract = False


class Iei28(models_dump.Iei28):
    class Meta(models_dump.Iei28.Meta):
        abstract = False


class Iei3(models_dump.Iei3):
    class Meta(models_dump.Iei3.Meta):
        abstract = False


class Iei4(models_dump.Iei4):
    class Meta(models_dump.Iei4.Meta):
        abstract = False


class Iei5(models_dump.Iei5):
    class Meta(models_dump.Iei5.Meta):
        abstract = False


class Iei6(models_dump.Iei6):
    class Meta(models_dump.Iei6.Meta):
        abstract = False


class Iei7(models_dump.Iei7):
    class Meta(models_dump.Iei7.Meta):
        abstract = False


class Iei8(models_dump.Iei8):
    class Meta(models_dump.Iei8.Meta):
        abstract = False


class Iei9(models_dump.Iei9):
    class Meta(models_dump.Iei9.Meta):
        abstract = False


class Ier1(models_dump.Ier1):
    class Meta(models_dump.Ier1.Meta):
        abstract = False


class Ige1(models_dump.Ige1):
    class Meta(models_dump.Ige1.Meta):
        abstract = False


class Ige10(models_dump.Ige10):
    class Meta(models_dump.Ige10.Meta):
        abstract = False


class Ige11(models_dump.Ige11):
    class Meta(models_dump.Ige11.Meta):
        abstract = False


class Ige12(models_dump.Ige12):
    class Meta(models_dump.Ige12.Meta):
        abstract = False


class Ige13(models_dump.Ige13):
    class Meta(models_dump.Ige13.Meta):
        abstract = False


class Ige14(models_dump.Ige14):
    class Meta(models_dump.Ige14.Meta):
        abstract = False


class Ige15(models_dump.Ige15):
    class Meta(models_dump.Ige15.Meta):
        abstract = False


class Ige16(models_dump.Ige16):
    class Meta(models_dump.Ige16.Meta):
        abstract = False


class Ige17(models_dump.Ige17):
    class Meta(models_dump.Ige17.Meta):
        abstract = False


class Ige18(models_dump.Ige18):
    class Meta(models_dump.Ige18.Meta):
        abstract = False


class Ige19(models_dump.Ige19):
    class Meta(models_dump.Ige19.Meta):
        abstract = False


class Ige2(models_dump.Ige2):
    class Meta(models_dump.Ige2.Meta):
        abstract = False


class Ige20(models_dump.Ige20):
    class Meta(models_dump.Ige20.Meta):
        abstract = False


class Ige21(models_dump.Ige21):
    class Meta(models_dump.Ige21.Meta):
        abstract = False


class Ige22(models_dump.Ige22):
    class Meta(models_dump.Ige22.Meta):
        abstract = False


class Ige23(models_dump.Ige23):
    class Meta(models_dump.Ige23.Meta):
        abstract = False


class Ige24(models_dump.Ige24):
    class Meta(models_dump.Ige24.Meta):
        abstract = False


class Ige25(models_dump.Ige25):
    class Meta(models_dump.Ige25.Meta):
        abstract = False


class Ige26(models_dump.Ige26):
    class Meta(models_dump.Ige26.Meta):
        abstract = False


class Ige27(models_dump.Ige27):
    class Meta(models_dump.Ige27.Meta):
        abstract = False


class Ige28(models_dump.Ige28):
    class Meta(models_dump.Ige28.Meta):
        abstract = False


class Ige3(models_dump.Ige3):
    class Meta(models_dump.Ige3.Meta):
        abstract = False


class Ige4(models_dump.Ige4):
    class Meta(models_dump.Ige4.Meta):
        abstract = False


class Ige5(models_dump.Ige5):
    class Meta(models_dump.Ige5.Meta):
        abstract = False


class Ige6(models_dump.Ige6):
    class Meta(models_dump.Ige6.Meta):
        abstract = False


class Ige7(models_dump.Ige7):
    class Meta(models_dump.Ige7.Meta):
        abstract = False


class Ige8(models_dump.Ige8):
    class Meta(models_dump.Ige8.Meta):
        abstract = False


class Ige9(models_dump.Ige9):
    class Meta(models_dump.Ige9.Meta):
        abstract = False


class Ign1(models_dump.Ign1):
    class Meta(models_dump.Ign1.Meta):
        abstract = False


class Ign10(models_dump.Ign10):
    class Meta(models_dump.Ign10.Meta):
        abstract = False


class Ign11(models_dump.Ign11):
    class Meta(models_dump.Ign11.Meta):
        abstract = False


class Ign12(models_dump.Ign12):
    class Meta(models_dump.Ign12.Meta):
        abstract = False


class Ign13(models_dump.Ign13):
    class Meta(models_dump.Ign13.Meta):
        abstract = False


class Ign14(models_dump.Ign14):
    class Meta(models_dump.Ign14.Meta):
        abstract = False


class Ign15(models_dump.Ign15):
    class Meta(models_dump.Ign15.Meta):
        abstract = False


class Ign16(models_dump.Ign16):
    class Meta(models_dump.Ign16.Meta):
        abstract = False


class Ign17(models_dump.Ign17):
    class Meta(models_dump.Ign17.Meta):
        abstract = False


class Ign18(models_dump.Ign18):
    class Meta(models_dump.Ign18.Meta):
        abstract = False


class Ign19(models_dump.Ign19):
    class Meta(models_dump.Ign19.Meta):
        abstract = False


class Ign2(models_dump.Ign2):
    class Meta(models_dump.Ign2.Meta):
        abstract = False


class Ign20(models_dump.Ign20):
    class Meta(models_dump.Ign20.Meta):
        abstract = False


class Ign21(models_dump.Ign21):
    class Meta(models_dump.Ign21.Meta):
        abstract = False


class Ign22(models_dump.Ign22):
    class Meta(models_dump.Ign22.Meta):
        abstract = False


class Ign23(models_dump.Ign23):
    class Meta(models_dump.Ign23.Meta):
        abstract = False


class Ign24(models_dump.Ign24):
    class Meta(models_dump.Ign24.Meta):
        abstract = False


class Ign25(models_dump.Ign25):
    class Meta(models_dump.Ign25.Meta):
        abstract = False


class Ign26(models_dump.Ign26):
    class Meta(models_dump.Ign26.Meta):
        abstract = False


class Ign27(models_dump.Ign27):
    class Meta(models_dump.Ign27.Meta):
        abstract = False


class Ign28(models_dump.Ign28):
    class Meta(models_dump.Ign28.Meta):
        abstract = False


class Ign3(models_dump.Ign3):
    class Meta(models_dump.Ign3.Meta):
        abstract = False


class Ign4(models_dump.Ign4):
    class Meta(models_dump.Ign4.Meta):
        abstract = False


class Ign5(models_dump.Ign5):
    class Meta(models_dump.Ign5.Meta):
        abstract = False


class Ign6(models_dump.Ign6):
    class Meta(models_dump.Ign6.Meta):
        abstract = False


class Ign7(models_dump.Ign7):
    class Meta(models_dump.Ign7.Meta):
        abstract = False


class Ign8(models_dump.Ign8):
    class Meta(models_dump.Ign8.Meta):
        abstract = False


class Ign9(models_dump.Ign9):
    class Meta(models_dump.Ign9.Meta):
        abstract = False


class Ilm1(models_dump.Ilm1):
    class Meta(models_dump.Ilm1.Meta):
        abstract = False


class Ilm2(models_dump.Ilm2):
    class Meta(models_dump.Ilm2.Meta):
        abstract = False


class Ilm3(models_dump.Ilm3):
    class Meta(models_dump.Ilm3.Meta):
        abstract = False


class Imgdt(models_dump.Imgdt):
    class Meta(models_dump.Imgdt.Meta):
        abstract = False


class Imglg(models_dump.Imglg):
    class Meta(models_dump.Imglg.Meta):
        abstract = False


class Imgp1(models_dump.Imgp1):
    class Meta(models_dump.Imgp1.Meta):
        abstract = False


class Imgrp(models_dump.Imgrp):
    class Meta(models_dump.Imgrp.Meta):
        abstract = False


class Imltm(models_dump.Imltm):
    class Meta(models_dump.Imltm.Meta):
        abstract = False


class Implg(models_dump.Implg):
    class Meta(models_dump.Implg.Meta):
        abstract = False


class Imqsg(models_dump.Imqsg):
    class Meta(models_dump.Imqsg.Meta):
        abstract = False


class Imt1(models_dump.Imt1):
    class Meta(models_dump.Imt1.Meta):
        abstract = False


class Imt11(models_dump.Imt11):
    class Meta(models_dump.Imt11.Meta):
        abstract = False


class Inc1(models_dump.Inc1):
    class Meta(models_dump.Inc1.Meta):
        abstract = False


class Inc10(models_dump.Inc10):
    class Meta(models_dump.Inc10.Meta):
        abstract = False


class Inc11(models_dump.Inc11):
    class Meta(models_dump.Inc11.Meta):
        abstract = False


class Inc12(models_dump.Inc12):
    class Meta(models_dump.Inc12.Meta):
        abstract = False


class Inc2(models_dump.Inc2):
    class Meta(models_dump.Inc2.Meta):
        abstract = False


class Inc3(models_dump.Inc3):
    class Meta(models_dump.Inc3.Meta):
        abstract = False


class Inc4(models_dump.Inc4):
    class Meta(models_dump.Inc4.Meta):
        abstract = False


class Inc5(models_dump.Inc5):
    class Meta(models_dump.Inc5.Meta):
        abstract = False


class Inc6(models_dump.Inc6):
    class Meta(models_dump.Inc6.Meta):
        abstract = False


class Inc7(models_dump.Inc7):
    class Meta(models_dump.Inc7.Meta):
        abstract = False


class Inc8(models_dump.Inc8):
    class Meta(models_dump.Inc8.Meta):
        abstract = False


class Inc9(models_dump.Inc9):
    class Meta(models_dump.Inc9.Meta):
        abstract = False


class Ins1(models_dump.Ins1):
    class Meta(models_dump.Ins1.Meta):
        abstract = False


class Inv1(models_dump.Inv1):
    class Meta(models_dump.Inv1.Meta):
        abstract = False


class Inv10(models_dump.Inv10):
    class Meta(models_dump.Inv10.Meta):
        abstract = False


class Inv11(models_dump.Inv11):
    class Meta(models_dump.Inv11.Meta):
        abstract = False


class Inv12(models_dump.Inv12):
    class Meta(models_dump.Inv12.Meta):
        abstract = False


class Inv13(models_dump.Inv13):
    class Meta(models_dump.Inv13.Meta):
        abstract = False


class Inv14(models_dump.Inv14):
    class Meta(models_dump.Inv14.Meta):
        abstract = False


class Inv15(models_dump.Inv15):
    class Meta(models_dump.Inv15.Meta):
        abstract = False


class Inv16(models_dump.Inv16):
    class Meta(models_dump.Inv16.Meta):
        abstract = False


class Inv17(models_dump.Inv17):
    class Meta(models_dump.Inv17.Meta):
        abstract = False


class Inv18(models_dump.Inv18):
    class Meta(models_dump.Inv18.Meta):
        abstract = False


class Inv19(models_dump.Inv19):
    class Meta(models_dump.Inv19.Meta):
        abstract = False


class Inv2(models_dump.Inv2):
    class Meta(models_dump.Inv2.Meta):
        abstract = False


class Inv20(models_dump.Inv20):
    class Meta(models_dump.Inv20.Meta):
        abstract = False


class Inv21(models_dump.Inv21):
    class Meta(models_dump.Inv21.Meta):
        abstract = False


class Inv22(models_dump.Inv22):
    class Meta(models_dump.Inv22.Meta):
        abstract = False


class Inv23(models_dump.Inv23):
    class Meta(models_dump.Inv23.Meta):
        abstract = False


class Inv24(models_dump.Inv24):
    class Meta(models_dump.Inv24.Meta):
        abstract = False


class Inv25(models_dump.Inv25):
    class Meta(models_dump.Inv25.Meta):
        abstract = False


class Inv26(models_dump.Inv26):
    class Meta(models_dump.Inv26.Meta):
        abstract = False


class Inv27(models_dump.Inv27):
    class Meta(models_dump.Inv27.Meta):
        abstract = False


class Inv28(models_dump.Inv28):
    class Meta(models_dump.Inv28.Meta):
        abstract = False


class Inv3(models_dump.Inv3):
    class Meta(models_dump.Inv3.Meta):
        abstract = False


class Inv4(models_dump.Inv4):
    class Meta(models_dump.Inv4.Meta):
        abstract = False


class Inv5(models_dump.Inv5):
    class Meta(models_dump.Inv5.Meta):
        abstract = False


class Inv6(models_dump.Inv6):
    class Meta(models_dump.Inv6.Meta):
        abstract = False


class Inv7(models_dump.Inv7):
    class Meta(models_dump.Inv7.Meta):
        abstract = False


class Inv8(models_dump.Inv8):
    class Meta(models_dump.Inv8.Meta):
        abstract = False


class Inv9(models_dump.Inv9):
    class Meta(models_dump.Inv9.Meta):
        abstract = False


class Iod1(models_dump.Iod1):
    class Meta(models_dump.Iod1.Meta):
        abstract = False


class Iod2(models_dump.Iod2):
    class Meta(models_dump.Iod2.Meta):
        abstract = False


class Iod3(models_dump.Iod3):
    class Meta(models_dump.Iod3.Meta):
        abstract = False


class Ipd1(models_dump.Ipd1):
    class Meta(models_dump.Ipd1.Meta):
        abstract = False


class Ipd2(models_dump.Ipd2):
    class Meta(models_dump.Ipd2.Meta):
        abstract = False


class Ipd3(models_dump.Ipd3):
    class Meta(models_dump.Ipd3.Meta):
        abstract = False


class Ipd4(models_dump.Ipd4):
    class Meta(models_dump.Ipd4.Meta):
        abstract = False


class Ipd5(models_dump.Ipd5):
    class Meta(models_dump.Ipd5.Meta):
        abstract = False


class Ipf1(models_dump.Ipf1):
    class Meta(models_dump.Ipf1.Meta):
        abstract = False


class Ipf2(models_dump.Ipf2):
    class Meta(models_dump.Ipf2.Meta):
        abstract = False


class Ipf3(models_dump.Ipf3):
    class Meta(models_dump.Ipf3.Meta):
        abstract = False


class Iqi1(models_dump.Iqi1):
    class Meta(models_dump.Iqi1.Meta):
        abstract = False


class Iqi2(models_dump.Iqi2):
    class Meta(models_dump.Iqi2.Meta):
        abstract = False


class Iqi3(models_dump.Iqi3):
    class Meta(models_dump.Iqi3.Meta):
        abstract = False


class Iqr1(models_dump.Iqr1):
    class Meta(models_dump.Iqr1.Meta):
        abstract = False


class Iqr2(models_dump.Iqr2):
    class Meta(models_dump.Iqr2.Meta):
        abstract = False


class Iqr3(models_dump.Iqr3):
    class Meta(models_dump.Iqr3.Meta):
        abstract = False


class Iqr4(models_dump.Iqr4):
    class Meta(models_dump.Iqr4.Meta):
        abstract = False


class Iqr5(models_dump.Iqr5):
    class Meta(models_dump.Iqr5.Meta):
        abstract = False


class Ird1(models_dump.Ird1):
    class Meta(models_dump.Ird1.Meta):
        abstract = False


class Iri1(models_dump.Iri1):
    class Meta(models_dump.Iri1.Meta):
        abstract = False


class Irr1(models_dump.Irr1):
    class Meta(models_dump.Irr1.Meta):
        abstract = False


class Isc1(models_dump.Isc1):
    class Meta(models_dump.Isc1.Meta):
        abstract = False


class Isd1(models_dump.Isd1):
    class Meta(models_dump.Isd1.Meta):
        abstract = False


class Isd2(models_dump.Isd2):
    class Meta(models_dump.Isd2.Meta):
        abstract = False


class Isi1(models_dump.Isi1):
    class Meta(models_dump.Isi1.Meta):
        abstract = False


class Isw1(models_dump.Isw1):
    class Meta(models_dump.Isw1.Meta):
        abstract = False


class Isw2(models_dump.Isw2):
    class Meta(models_dump.Isw2.Meta):
        abstract = False


class Isw3(models_dump.Isw3):
    class Meta(models_dump.Isw3.Meta):
        abstract = False


class Itl1(models_dump.Itl1):
    class Meta(models_dump.Itl1.Meta):
        abstract = False


class Itm1(models_dump.Itm1):
    class Meta(models_dump.Itm1.Meta):
        abstract = False


class Itm10(models_dump.Itm10):
    class Meta(models_dump.Itm10.Meta):
        abstract = False


class Itm11(models_dump.Itm11):
    class Meta(models_dump.Itm11.Meta):
        abstract = False


class Itm12(models_dump.Itm12):
    class Meta(models_dump.Itm12.Meta):
        abstract = False


class Itm13(models_dump.Itm13):
    class Meta(models_dump.Itm13.Meta):
        abstract = False


class Itm2(models_dump.Itm2):
    class Meta(models_dump.Itm2.Meta):
        abstract = False


class Itm3(models_dump.Itm3):
    class Meta(models_dump.Itm3.Meta):
        abstract = False


class Itm4(models_dump.Itm4):
    class Meta(models_dump.Itm4.Meta):
        abstract = False


class Itm5(models_dump.Itm5):
    class Meta(models_dump.Itm5.Meta):
        abstract = False


class Itm6(models_dump.Itm6):
    class Meta(models_dump.Itm6.Meta):
        abstract = False


class Itm7(models_dump.Itm7):
    class Meta(models_dump.Itm7.Meta):
        abstract = False


class Itm8(models_dump.Itm8):
    class Meta(models_dump.Itm8.Meta):
        abstract = False


class Itm9(models_dump.Itm9):
    class Meta(models_dump.Itm9.Meta):
        abstract = False


class Itr1(models_dump.Itr1):
    class Meta(models_dump.Itr1.Meta):
        abstract = False


class Itt1(models_dump.Itt1):
    class Meta(models_dump.Itt1.Meta):
        abstract = False


class Itt2(models_dump.Itt2):
    class Meta(models_dump.Itt2.Meta):
        abstract = False


class Itw1(models_dump.Itw1):
    class Meta(models_dump.Itw1.Meta):
        abstract = False


class Ivl1(models_dump.Ivl1):
    class Meta(models_dump.Ivl1.Meta):
        abstract = False


class Ivl2(models_dump.Ivl2):
    class Meta(models_dump.Ivl2.Meta):
        abstract = False


class Ivlg(models_dump.Ivlg):
    class Meta(models_dump.Ivlg.Meta):
        abstract = False


class Ivm1(models_dump.Ivm1):
    class Meta(models_dump.Ivm1.Meta):
        abstract = False


class Ivru(models_dump.Ivru):
    class Meta(models_dump.Ivru.Meta):
        abstract = False


class Iwb1(models_dump.Iwb1):
    class Meta(models_dump.Iwb1.Meta):
        abstract = False


class Iwb2(models_dump.Iwb2):
    class Meta(models_dump.Iwb2.Meta):
        abstract = False


class Iwz1(models_dump.Iwz1):
    class Meta(models_dump.Iwz1.Meta):
        abstract = False


class Iwz2(models_dump.Iwz2):
    class Meta(models_dump.Iwz2.Meta):
        abstract = False


class Iwz3(models_dump.Iwz3):
    class Meta(models_dump.Iwz3.Meta):
        abstract = False


class Jdt1(models_dump.Jdt1):
    class Meta(models_dump.Jdt1.Meta):
        abstract = False


class Jdt2(models_dump.Jdt2):
    class Meta(models_dump.Jdt2.Meta):
        abstract = False


class Jst1(models_dump.Jst1):
    class Meta(models_dump.Jst1.Meta):
        abstract = False


class Kpi1(models_dump.Kpi1):
    class Meta(models_dump.Kpi1.Meta):
        abstract = False


class Kpi2(models_dump.Kpi2):
    class Meta(models_dump.Kpi2.Meta):
        abstract = False


class Kpi3(models_dump.Kpi3):
    class Meta(models_dump.Kpi3.Meta):
        abstract = False


class Kpi4(models_dump.Kpi4):
    class Meta(models_dump.Kpi4.Meta):
        abstract = False


class Kpi5(models_dump.Kpi5):
    class Meta(models_dump.Kpi5.Meta):
        abstract = False


class Kps1(models_dump.Kps1):
    class Meta(models_dump.Kps1.Meta):
        abstract = False


class Lgl1(models_dump.Lgl1):
    class Meta(models_dump.Lgl1.Meta):
        abstract = False


class Livi(models_dump.Livi):
    class Meta(models_dump.Livi.Meta):
        abstract = False


class Livi1(models_dump.Livi1):
    class Meta(models_dump.Livi1.Meta):
        abstract = False


class Llr1(models_dump.Llr1):
    class Meta(models_dump.Llr1.Meta):
        abstract = False


class Ltf1(models_dump.Ltf1):
    class Meta(models_dump.Ltf1.Meta):
        abstract = False


class Mab1(models_dump.Mab1):
    class Meta(models_dump.Mab1.Meta):
        abstract = False


class Mabv(models_dump.Mabv):
    class Meta(models_dump.Mabv.Meta):
        abstract = False


class Map1(models_dump.Map1):
    class Meta(models_dump.Map1.Meta):
        abstract = False


class Map2(models_dump.Map2):
    class Meta(models_dump.Map2.Meta):
        abstract = False


class Mdc1(models_dump.Mdc1):
    class Meta(models_dump.Mdc1.Meta):
        abstract = False


class Mdc2(models_dump.Mdc2):
    class Meta(models_dump.Mdc2.Meta):
        abstract = False


class Mdp1(models_dump.Mdp1):
    class Meta(models_dump.Mdp1.Meta):
        abstract = False


class Mdp2(models_dump.Mdp2):
    class Meta(models_dump.Mdp2.Meta):
        abstract = False


class Mdp3(models_dump.Mdp3):
    class Meta(models_dump.Mdp3.Meta):
        abstract = False


class Mdr1(models_dump.Mdr1):
    class Meta(models_dump.Mdr1.Meta):
        abstract = False


class Min1(models_dump.Min1):
    class Meta(models_dump.Min1.Meta):
        abstract = False


class Min2(models_dump.Min2):
    class Meta(models_dump.Min2.Meta):
        abstract = False


class Miv1(models_dump.Miv1):
    class Meta(models_dump.Miv1.Meta):
        abstract = False


class Miv2(models_dump.Miv2):
    class Meta(models_dump.Miv2.Meta):
        abstract = False


class Mls1(models_dump.Mls1):
    class Meta(models_dump.Mls1.Meta):
        abstract = False


class Mlt1(models_dump.Mlt1):
    class Meta(models_dump.Mlt1.Meta):
        abstract = False


class Mpo1(models_dump.Mpo1):
    class Meta(models_dump.Mpo1.Meta):
        abstract = False


class Mrv1(models_dump.Mrv1):
    class Meta(models_dump.Mrv1.Meta):
        abstract = False


class Mrv2(models_dump.Mrv2):
    class Meta(models_dump.Mrv2.Meta):
        abstract = False


class Mrv3(models_dump.Mrv3):
    class Meta(models_dump.Mrv3.Meta):
        abstract = False


class Mrv4(models_dump.Mrv4):
    class Meta(models_dump.Mrv4.Meta):
        abstract = False


class Msn1(models_dump.Msn1):
    class Meta(models_dump.Msn1.Meta):
        abstract = False


class Msn2(models_dump.Msn2):
    class Meta(models_dump.Msn2.Meta):
        abstract = False


class Msn3(models_dump.Msn3):
    class Meta(models_dump.Msn3.Meta):
        abstract = False


class Msn4(models_dump.Msn4):
    class Meta(models_dump.Msn4.Meta):
        abstract = False


class Msn5(models_dump.Msn5):
    class Meta(models_dump.Msn5.Meta):
        abstract = False


class Mth1V(models_dump.Mth1V):
    class Meta(models_dump.Mth1V.Meta):
        abstract = False


class Mth2V(models_dump.Mth2V):
    class Meta(models_dump.Mth2V.Meta):
        abstract = False


class Mth3V(models_dump.Mth3V):
    class Meta(models_dump.Mth3V.Meta):
        abstract = False


class Ncp1(models_dump.Ncp1):
    class Meta(models_dump.Ncp1.Meta):
        abstract = False


class Ncp2(models_dump.Ncp2):
    class Meta(models_dump.Ncp2.Meta):
        abstract = False


class Nfn1(models_dump.Nfn1):
    class Meta(models_dump.Nfn1.Meta):
        abstract = False


class Nfn2(models_dump.Nfn2):
    class Meta(models_dump.Nfn2.Meta):
        abstract = False


class Nfn3(models_dump.Nfn3):
    class Meta(models_dump.Nfn3.Meta):
        abstract = False


class Nfn4(models_dump.Nfn4):
    class Meta(models_dump.Nfn4.Meta):
        abstract = False


class Nfn5(models_dump.Nfn5):
    class Meta(models_dump.Nfn5.Meta):
        abstract = False


class Nnm1(models_dump.Nnm1):
    class Meta(models_dump.Nnm1.Meta):
        abstract = False


class Nnm2(models_dump.Nnm2):
    class Meta(models_dump.Nnm2.Meta):
        abstract = False


class Nnm3(models_dump.Nnm3):
    class Meta(models_dump.Nnm3.Meta):
        abstract = False


class Nnm4(models_dump.Nnm4):
    class Meta(models_dump.Nnm4.Meta):
        abstract = False


class Nnm5(models_dump.Nnm5):
    class Meta(models_dump.Nnm5.Meta):
        abstract = False


class Nnm6(models_dump.Nnm6):
    class Meta(models_dump.Nnm6.Meta):
        abstract = False


class Oaar(models_dump.Oaar):
    class Meta(models_dump.Oaar.Meta):
        abstract = False


class Oacd(models_dump.Oacd):
    class Meta(models_dump.Oacd.Meta):
        abstract = False


class Oacg(models_dump.Oacg):
    class Meta(models_dump.Oacg.Meta):
        abstract = False


class Oack(models_dump.Oack):
    class Meta(models_dump.Oack.Meta):
        abstract = False


class Oacm(models_dump.Oacm):
    class Meta(models_dump.Oacm.Meta):
        abstract = False


class Oacp(models_dump.Oacp):
    class Meta(models_dump.Oacp.Meta):
        abstract = False


class Oacq(models_dump.Oacq):
    class Meta(models_dump.Oacq.Meta):
        abstract = False


class Oacr(models_dump.Oacr):
    class Meta(models_dump.Oacr.Meta):
        abstract = False


class Oacs(models_dump.Oacs):
    class Meta(models_dump.Oacs.Meta):
        abstract = False


class Oact(models_dump.Oact):
    class Meta(models_dump.Oact.Meta):
        abstract = False


class Oadf(models_dump.Oadf):
    class Meta(models_dump.Oadf.Meta):
        abstract = False


class Oadg(models_dump.Oadg):
    class Meta(models_dump.Oadg.Meta):
        abstract = False


class Oadm(models_dump.Oadm):
    class Meta(models_dump.Oadm.Meta):
        abstract = False


class Oadp(models_dump.Oadp):
    class Meta(models_dump.Oadp.Meta):
        abstract = False


class Oadt(models_dump.Oadt):
    class Meta(models_dump.Oadt.Meta):
        abstract = False


class Oagm(models_dump.Oagm):
    class Meta(models_dump.Oagm.Meta):
        abstract = False


class Oagp(models_dump.Oagp):
    class Meta(models_dump.Oagp.Meta):
        abstract = False


class Oags(models_dump.Oags):
    class Meta(models_dump.Oags.Meta):
        abstract = False


class Oaib(models_dump.Oaib):
    class Meta(models_dump.Oaib.Meta):
        abstract = False


class Oaim(models_dump.Oaim):
    class Meta(models_dump.Oaim.Meta):
        abstract = False


class Oalc(models_dump.Oalc):
    class Meta(models_dump.Oalc.Meta):
        abstract = False


class Oali(models_dump.Oali):
    class Meta(models_dump.Oali.Meta):
        abstract = False


class Oalr(models_dump.Oalr):
    class Meta(models_dump.Oalr.Meta):
        abstract = False


class Oalt(models_dump.Oalt):
    class Meta(models_dump.Oalt.Meta):
        abstract = False


class Oamd(models_dump.Oamd):
    class Meta(models_dump.Oamd.Meta):
        abstract = False


class Oaob(models_dump.Oaob):
    class Meta(models_dump.Oaob.Meta):
        abstract = False


class Oarg(models_dump.Oarg):
    class Meta(models_dump.Oarg.Meta):
        abstract = False


class Oari(models_dump.Oari):
    class Meta(models_dump.Oari.Meta):
        abstract = False


class Oasc(models_dump.Oasc):
    class Meta(models_dump.Oasc.Meta):
        abstract = False


class Oasg(models_dump.Oasg):
    class Meta(models_dump.Oasg.Meta):
        abstract = False


class Oat1(models_dump.Oat1):
    class Meta(models_dump.Oat1.Meta):
        abstract = False


class Oat2(models_dump.Oat2):
    class Meta(models_dump.Oat2.Meta):
        abstract = False


class Oat3(models_dump.Oat3):
    class Meta(models_dump.Oat3.Meta):
        abstract = False


class Oat4(models_dump.Oat4):
    class Meta(models_dump.Oat4.Meta):
        abstract = False


class Oatc(models_dump.Oatc):
    class Meta(models_dump.Oatc.Meta):
        abstract = False


class Obal(models_dump.Obal):
    class Meta(models_dump.Obal.Meta):
        abstract = False


class Obat(models_dump.Obat):
    class Meta(models_dump.Obat.Meta):
        abstract = False


class Obbi(models_dump.Obbi):
    class Meta(models_dump.Obbi.Meta):
        abstract = False


class Obbq(models_dump.Obbq):
    class Meta(models_dump.Obbq.Meta):
        abstract = False


class Obca(models_dump.Obca):
    class Meta(models_dump.Obca.Meta):
        abstract = False


class Obcd(models_dump.Obcd):
    itemcode = models.ForeignKey(
        "Oitm", models.DO_NOTHING, db_column="ItemCode", max_length=50
    )

    class Meta(models_dump.Obcd.Meta):
        abstract = False


class Obcg(models_dump.Obcg):
    class Meta(models_dump.Obcg.Meta):
        abstract = False


class Obdc(models_dump.Obdc):
    class Meta(models_dump.Obdc.Meta):
        abstract = False


class Obfc(models_dump.Obfc):
    class Meta(models_dump.Obfc.Meta):
        abstract = False


class Obfi(models_dump.Obfi):
    class Meta(models_dump.Obfi.Meta):
        abstract = False


class Obgd(models_dump.Obgd):
    class Meta(models_dump.Obgd.Meta):
        abstract = False


class Obgs(models_dump.Obgs):
    class Meta(models_dump.Obgs.Meta):
        abstract = False


class Obgt(models_dump.Obgt):
    class Meta(models_dump.Obgt.Meta):
        abstract = False


class Obin(models_dump.Obin):
    class Meta(models_dump.Obin.Meta):
        abstract = False


class Obmi(models_dump.Obmi):
    class Meta(models_dump.Obmi.Meta):
        abstract = False


class Obnh(models_dump.Obnh):
    class Meta(models_dump.Obnh.Meta):
        abstract = False


class Obni(models_dump.Obni):
    class Meta(models_dump.Obni.Meta):
        abstract = False


class Obnk(models_dump.Obnk):
    class Meta(models_dump.Obnk.Meta):
        abstract = False


class Obob(models_dump.Obob):
    class Meta(models_dump.Obob.Meta):
        abstract = False


class Oboc(models_dump.Oboc):
    class Meta(models_dump.Oboc.Meta):
        abstract = False


class Obod(models_dump.Obod):
    class Meta(models_dump.Obod.Meta):
        abstract = False


class Oboe(models_dump.Oboe):
    class Meta(models_dump.Oboe.Meta):
        abstract = False


class Oboi(models_dump.Oboi):
    class Meta(models_dump.Oboi.Meta):
        abstract = False


class Obol(models_dump.Obol):
    class Meta(models_dump.Obol.Meta):
        abstract = False


class Obos(models_dump.Obos):
    class Meta(models_dump.Obos.Meta):
        abstract = False


class Obot(models_dump.Obot):
    class Meta(models_dump.Obot.Meta):
        abstract = False


class Obox(models_dump.Obox):
    class Meta(models_dump.Obox.Meta):
        abstract = False


class Obpd(models_dump.Obpd):
    class Meta(models_dump.Obpd.Meta):
        abstract = False


class Obpl(models_dump.Obpl):
    class Meta(models_dump.Obpl.Meta):
        abstract = False


class Obpp(models_dump.Obpp):
    class Meta(models_dump.Obpp.Meta):
        abstract = False


class Obrw(models_dump.Obrw):
    class Meta(models_dump.Obrw.Meta):
        abstract = False


class Obsi(models_dump.Obsi):
    class Meta(models_dump.Obsi.Meta):
        abstract = False


class Obsj(models_dump.Obsj):
    class Meta(models_dump.Obsj.Meta):
        abstract = False


class Obsl(models_dump.Obsl):
    class Meta(models_dump.Obsl.Meta):
        abstract = False


class Obst(models_dump.Obst):
    class Meta(models_dump.Obst.Meta):
        abstract = False


class Obta(models_dump.Obta):
    class Meta(models_dump.Obta.Meta):
        abstract = False


class Obtc(models_dump.Obtc):
    class Meta(models_dump.Obtc.Meta):
        abstract = False


class Obtd(models_dump.Obtd):
    class Meta(models_dump.Obtd.Meta):
        abstract = False


class Obtf(models_dump.Obtf):
    class Meta(models_dump.Obtf.Meta):
        abstract = False


class Obtl(models_dump.Obtl):
    class Meta(models_dump.Obtl.Meta):
        abstract = False


class Obtn(models_dump.Obtn):
    class Meta(models_dump.Obtn.Meta):
        abstract = False


class Obtq(models_dump.Obtq):
    class Meta(models_dump.Obtq.Meta):
        abstract = False


class Obtw(models_dump.Obtw):
    class Meta(models_dump.Obtw.Meta):
        abstract = False


class Obvl(models_dump.Obvl):
    class Meta(models_dump.Obvl.Meta):
        abstract = False


class Ocbi(models_dump.Ocbi):
    class Meta(models_dump.Ocbi.Meta):
        abstract = False


class Occd(models_dump.Occd):
    class Meta(models_dump.Occd.Meta):
        abstract = False


class Occs(models_dump.Occs):
    class Meta(models_dump.Occs.Meta):
        abstract = False


class Occt(models_dump.Occt):
    class Meta(models_dump.Occt.Meta):
        abstract = False


class Ocdc(models_dump.Ocdc):
    class Meta(models_dump.Ocdc.Meta):
        abstract = False


class Ocdp(models_dump.Ocdp):
    class Meta(models_dump.Ocdp.Meta):
        abstract = False


class Ocdt(models_dump.Ocdt):
    class Meta(models_dump.Ocdt.Meta):
        abstract = False


class Ocem(models_dump.Ocem):
    class Meta(models_dump.Ocem.Meta):
        abstract = False


class Ocest(models_dump.Ocest):
    class Meta(models_dump.Ocest.Meta):
        abstract = False


class Ocfh(models_dump.Ocfh):
    class Meta(models_dump.Ocfh.Meta):
        abstract = False


class Ocfl(models_dump.Ocfl):
    class Meta(models_dump.Ocfl.Meta):
        abstract = False


class Ocfn(models_dump.Ocfn):
    class Meta(models_dump.Ocfn.Meta):
        abstract = False


class Ocfp(models_dump.Ocfp):
    class Meta(models_dump.Ocfp.Meta):
        abstract = False


class Ocft(models_dump.Ocft):
    class Meta(models_dump.Ocft.Meta):
        abstract = False


class Ocfw(models_dump.Ocfw):
    class Meta(models_dump.Ocfw.Meta):
        abstract = False


class Ocfx(models_dump.Ocfx):
    class Meta(models_dump.Ocfx.Meta):
        abstract = False


class Ochd(models_dump.Ochd):
    class Meta(models_dump.Ochd.Meta):
        abstract = False


class Ochf(models_dump.Ochf):
    class Meta(models_dump.Ochf.Meta):
        abstract = False


class Ochh(models_dump.Ochh):
    class Meta(models_dump.Ochh.Meta):
        abstract = False


class Ocho(models_dump.Ocho):
    class Meta(models_dump.Ocho.Meta):
        abstract = False


class Ochp(models_dump.Ochp):
    class Meta(models_dump.Ochp.Meta):
        abstract = False


class Ocif(models_dump.Ocif):
    class Meta(models_dump.Ocif.Meta):
        abstract = False


class Ocig(models_dump.Ocig):
    class Meta(models_dump.Ocig.Meta):
        abstract = False


class Ocin(models_dump.Ocin):
    class Meta(models_dump.Ocin.Meta):
        abstract = False


class Ocip(models_dump.Ocip):
    class Meta(models_dump.Ocip.Meta):
        abstract = False


class Ocla(models_dump.Ocla):
    class Meta(models_dump.Ocla.Meta):
        abstract = False


class Oclc(models_dump.Oclc):
    class Meta(models_dump.Oclc.Meta):
        abstract = False


class Oclg(models_dump.Oclg):
    cardcode = models.ForeignKey(
        "Ocrd",
        models.DO_NOTHING,
        db_column="CardCode",
        max_length=15,
        blank=True,
        null=True,
    )
    attenduser = models.ForeignKey(
        "Ousr", models.DO_NOTHING, db_column="AttendUser", blank=True, null=True
    )
    cntctcode = models.ForeignKey(
        "Ocpr", models.DO_NOTHING, db_column="CntctCode", blank=True, null=True
    )
    cntcttype = models.ForeignKey(
        "Oclt", models.DO_NOTHING, db_column="CntctType", blank=True, null=True
    )

    class Meta(models_dump.Oclg.Meta):
        abstract = False


class Oclo(models_dump.Oclo):
    class Meta(models_dump.Oclo.Meta):
        abstract = False


class Ocls(models_dump.Ocls):
    class Meta(models_dump.Ocls.Meta):
        abstract = False


class Oclt(models_dump.Oclt):
    class Meta(models_dump.Oclt.Meta):
        abstract = False


class Ocmf(models_dump.Ocmf):
    class Meta(models_dump.Ocmf.Meta):
        abstract = False


class Ocmm(models_dump.Ocmm):
    class Meta(models_dump.Ocmm.Meta):
        abstract = False


class Ocmn(models_dump.Ocmn):
    class Meta(models_dump.Ocmn.Meta):
        abstract = False


class Ocmt(models_dump.Ocmt):
    class Meta(models_dump.Ocmt.Meta):
        abstract = False


class Ocna(models_dump.Ocna):
    class Meta(models_dump.Ocna.Meta):
        abstract = False


class Ocnt(models_dump.Ocnt):
    class Meta(models_dump.Ocnt.Meta):
        abstract = False


class Ocog(models_dump.Ocog):
    class Meta(models_dump.Ocog.Meta):
        abstract = False


class Ocpc(models_dump.Ocpc):
    class Meta(models_dump.Ocpc.Meta):
        abstract = False


class Ocpi(models_dump.Ocpi):
    class Meta(models_dump.Ocpi.Meta):
        abstract = False


class Ocpl(models_dump.Ocpl):
    class Meta(models_dump.Ocpl.Meta):
        abstract = False


class Ocpn(models_dump.Ocpn):
    class Meta(models_dump.Ocpn.Meta):
        abstract = False


class Ocpr(models_dump.Ocpr):
    cardcode = models.ForeignKey(
        "Ocrd", models.DO_NOTHING, db_column="CardCode", max_length=15
    )

    class Meta(models_dump.Ocpr.Meta):
        abstract = False


class Ocpt(models_dump.Ocpt):
    class Meta(models_dump.Ocpt.Meta):
        abstract = False


class Ocpv(models_dump.Ocpv):
    class Meta(models_dump.Ocpv.Meta):
        abstract = False


class Ocqg(models_dump.Ocqg):
    class Meta(models_dump.Ocqg.Meta):
        abstract = False


class Ocr1(models_dump.Ocr1):
    class Meta(models_dump.Ocr1.Meta):
        abstract = False


class Ocrb(models_dump.Ocrb):
    class Meta(models_dump.Ocrb.Meta):
        abstract = False


class Ocrc(models_dump.Ocrc):
    class Meta(models_dump.Ocrc.Meta):
        abstract = False


class Ocrd(models_dump.Ocrd):
    class Meta(models_dump.Ocrd.Meta):
        abstract = False


class Ocrg(models_dump.Ocrg):
    class Meta(models_dump.Ocrg.Meta):
        abstract = False


class Ocrh(models_dump.Ocrh):
    class Meta(models_dump.Ocrh.Meta):
        abstract = False


class Ocrn(models_dump.Ocrn):
    class Meta(models_dump.Ocrn.Meta):
        abstract = False


class Ocrp(models_dump.Ocrp):
    class Meta(models_dump.Ocrp.Meta):
        abstract = False


class Ocrt(models_dump.Ocrt):
    class Meta(models_dump.Ocrt.Meta):
        abstract = False


class Ocrv(models_dump.Ocrv):
    class Meta(models_dump.Ocrv.Meta):
        abstract = False


class Ocrw(models_dump.Ocrw):
    class Meta(models_dump.Ocrw.Meta):
        abstract = False


class Ocry(models_dump.Ocry):
    class Meta(models_dump.Ocry.Meta):
        abstract = False


class Ocsc(models_dump.Ocsc):
    class Meta(models_dump.Ocsc.Meta):
        abstract = False


class Ocsi(models_dump.Ocsi):
    class Meta(models_dump.Ocsi.Meta):
        abstract = False


class Ocsn(models_dump.Ocsn):
    class Meta(models_dump.Ocsn.Meta):
        abstract = False


class Ocsq(models_dump.Ocsq):
    class Meta(models_dump.Ocsq.Meta):
        abstract = False


class Ocst(models_dump.Ocst):
    class Meta(models_dump.Ocst.Meta):
        abstract = False


class Ocsv(models_dump.Ocsv):
    class Meta(models_dump.Ocsv.Meta):
        abstract = False


class Octg(models_dump.Octg):
    class Meta(models_dump.Octg.Meta):
        abstract = False


class Octr(models_dump.Octr):
    cstmrcode = models.ForeignKey(
        "Ocrd",
        models.DO_NOTHING,
        db_column="CstmrCode",
        max_length=15,
        blank=True,
        null=True,
    )
    u_product = models.ForeignKey(
        "DolMntProducts",
        models.DO_NOTHING,
        db_column="U_product",
        max_length=50,
        blank=True,
        null=True,
    )
    u_journey_cost_itmcde = models.ForeignKey(
        "Oitm",
        models.DO_NOTHING,
        db_column="U_journey_cost_itmcde",
        max_length=50,
        blank=True,
        null=True,
    )

    class Meta(models_dump.Octr.Meta):
        abstract = False


class Octt(models_dump.Octt):
    class Meta(models_dump.Octt.Meta):
        abstract = False


class Ocuc(models_dump.Ocuc):
    class Meta(models_dump.Ocuc.Meta):
        abstract = False


class Ocul(models_dump.Ocul):
    class Meta(models_dump.Ocul.Meta):
        abstract = False


class Ocup(models_dump.Ocup):
    class Meta(models_dump.Ocup.Meta):
        abstract = False


class Ocyc(models_dump.Ocyc):
    class Meta(models_dump.Ocyc.Meta):
        abstract = False


class Odab(models_dump.Odab):
    class Meta(models_dump.Odab.Meta):
        abstract = False


class Odal(models_dump.Odal):
    class Meta(models_dump.Odal.Meta):
        abstract = False


class Odar(models_dump.Odar):
    class Meta(models_dump.Odar.Meta):
        abstract = False


class Odbn(models_dump.Odbn):
    class Meta(models_dump.Odbn.Meta):
        abstract = False


class Odbw(models_dump.Odbw):
    class Meta(models_dump.Odbw.Meta):
        abstract = False


class Odcc(models_dump.Odcc):
    class Meta(models_dump.Odcc.Meta):
        abstract = False


class Odci(models_dump.Odci):
    class Meta(models_dump.Odci.Meta):
        abstract = False


class Odcr(models_dump.Odcr):
    class Meta(models_dump.Odcr.Meta):
        abstract = False


class Oddg(models_dump.Oddg):
    class Meta(models_dump.Oddg.Meta):
        abstract = False


class Oddt(models_dump.Oddt):
    class Meta(models_dump.Oddt.Meta):
        abstract = False


class Odgl(models_dump.Odgl):
    class Meta(models_dump.Odgl.Meta):
        abstract = False


class Odgp(models_dump.Odgp):
    class Meta(models_dump.Odgp.Meta):
        abstract = False


class Odim(models_dump.Odim):
    class Meta(models_dump.Odim.Meta):
        abstract = False


class Odll(models_dump.Odll):
    class Meta(models_dump.Odll.Meta):
        abstract = False


class Odln(models_dump.Odln):
    class Meta(models_dump.Odln.Meta):
        abstract = False


class Odmc(models_dump.Odmc):
    class Meta(models_dump.Odmc.Meta):
        abstract = False


class Odmw(models_dump.Odmw):
    class Meta(models_dump.Odmw.Meta):
        abstract = False


class Odnf(models_dump.Odnf):
    class Meta(models_dump.Odnf.Meta):
        abstract = False


class Odor(models_dump.Odor):
    class Meta(models_dump.Odor.Meta):
        abstract = False


class Odow(models_dump.Odow):
    class Meta(models_dump.Odow.Meta):
        abstract = False


class Odox(models_dump.Odox):
    class Meta(models_dump.Odox.Meta):
        abstract = False


class Odpa(models_dump.Odpa):
    class Meta(models_dump.Odpa.Meta):
        abstract = False


class Odph(models_dump.Odph):
    class Meta(models_dump.Odph.Meta):
        abstract = False


class Odpi(models_dump.Odpi):
    class Meta(models_dump.Odpi.Meta):
        abstract = False


class Odpo(models_dump.Odpo):
    class Meta(models_dump.Odpo.Meta):
        abstract = False


class Odpp(models_dump.Odpp):
    class Meta(models_dump.Odpp.Meta):
        abstract = False


class Odps(models_dump.Odps):
    class Meta(models_dump.Odps.Meta):
        abstract = False


class Odpt(models_dump.Odpt):
    class Meta(models_dump.Odpt.Meta):
        abstract = False


class Odpv(models_dump.Odpv):
    class Meta(models_dump.Odpv.Meta):
        abstract = False


class Odrc(models_dump.Odrc):
    class Meta(models_dump.Odrc.Meta):
        abstract = False


class Odrf(models_dump.Odrf):
    class Meta(models_dump.Odrf.Meta):
        abstract = False


class Odrn(models_dump.Odrn):
    class Meta(models_dump.Odrn.Meta):
        abstract = False


class Odsc(models_dump.Odsc):
    class Meta(models_dump.Odsc.Meta):
        abstract = False


class Odsl(models_dump.Odsl):
    class Meta(models_dump.Odsl.Meta):
        abstract = False


class Odsn(models_dump.Odsn):
    class Meta(models_dump.Odsn.Meta):
        abstract = False


class Odsw(models_dump.Odsw):
    class Meta(models_dump.Odsw.Meta):
        abstract = False


class Odtp(models_dump.Odtp):
    class Meta(models_dump.Odtp.Meta):
        abstract = False


class Odty(models_dump.Odty):
    class Meta(models_dump.Odty.Meta):
        abstract = False


class Odun(models_dump.Odun):
    class Meta(models_dump.Odun.Meta):
        abstract = False


class Odut(models_dump.Odut):
    class Meta(models_dump.Odut.Meta):
        abstract = False


class Odw1(models_dump.Odw1):
    class Meta(models_dump.Odw1.Meta):
        abstract = False


class Odwz(models_dump.Odwz):
    class Meta(models_dump.Odwz.Meta):
        abstract = False


class Oebk(models_dump.Oebk):
    class Meta(models_dump.Oebk.Meta):
        abstract = False


class Oebl(models_dump.Oebl):
    class Meta(models_dump.Oebl.Meta):
        abstract = False


class Oecc(models_dump.Oecc):
    class Meta(models_dump.Oecc.Meta):
        abstract = False


class Oecdw(models_dump.Oecdw):
    class Meta(models_dump.Oecdw.Meta):
        abstract = False


class Oecm(models_dump.Oecm):
    class Meta(models_dump.Oecm.Meta):
        abstract = False


class Oecp(models_dump.Oecp):
    class Meta(models_dump.Oecp.Meta):
        abstract = False


class Oedg(models_dump.Oedg):
    class Meta(models_dump.Oedg.Meta):
        abstract = False


class Oedt(models_dump.Oedt):
    class Meta(models_dump.Oedt.Meta):
        abstract = False


class Oefdw(models_dump.Oefdw):
    class Meta(models_dump.Oefdw.Meta):
        abstract = False


class Oegp(models_dump.Oegp):
    class Meta(models_dump.Oegp.Meta):
        abstract = False


class Oei1(models_dump.Oei1):
    class Meta(models_dump.Oei1.Meta):
        abstract = False


class Oei10(models_dump.Oei10):
    class Meta(models_dump.Oei10.Meta):
        abstract = False


class Oei11(models_dump.Oei11):
    class Meta(models_dump.Oei11.Meta):
        abstract = False


class Oei12(models_dump.Oei12):
    class Meta(models_dump.Oei12.Meta):
        abstract = False


class Oei13(models_dump.Oei13):
    class Meta(models_dump.Oei13.Meta):
        abstract = False


class Oei14(models_dump.Oei14):
    class Meta(models_dump.Oei14.Meta):
        abstract = False


class Oei15(models_dump.Oei15):
    class Meta(models_dump.Oei15.Meta):
        abstract = False


class Oei16(models_dump.Oei16):
    class Meta(models_dump.Oei16.Meta):
        abstract = False


class Oei17(models_dump.Oei17):
    class Meta(models_dump.Oei17.Meta):
        abstract = False


class Oei18(models_dump.Oei18):
    class Meta(models_dump.Oei18.Meta):
        abstract = False


class Oei19(models_dump.Oei19):
    class Meta(models_dump.Oei19.Meta):
        abstract = False


class Oei2(models_dump.Oei2):
    class Meta(models_dump.Oei2.Meta):
        abstract = False


class Oei20(models_dump.Oei20):
    class Meta(models_dump.Oei20.Meta):
        abstract = False


class Oei21(models_dump.Oei21):
    class Meta(models_dump.Oei21.Meta):
        abstract = False


class Oei22(models_dump.Oei22):
    class Meta(models_dump.Oei22.Meta):
        abstract = False


class Oei23(models_dump.Oei23):
    class Meta(models_dump.Oei23.Meta):
        abstract = False


class Oei24(models_dump.Oei24):
    class Meta(models_dump.Oei24.Meta):
        abstract = False


class Oei25(models_dump.Oei25):
    class Meta(models_dump.Oei25.Meta):
        abstract = False


class Oei26(models_dump.Oei26):
    class Meta(models_dump.Oei26.Meta):
        abstract = False


class Oei27(models_dump.Oei27):
    class Meta(models_dump.Oei27.Meta):
        abstract = False


class Oei28(models_dump.Oei28):
    class Meta(models_dump.Oei28.Meta):
        abstract = False


class Oei3(models_dump.Oei3):
    class Meta(models_dump.Oei3.Meta):
        abstract = False


class Oei4(models_dump.Oei4):
    class Meta(models_dump.Oei4.Meta):
        abstract = False


class Oei5(models_dump.Oei5):
    class Meta(models_dump.Oei5.Meta):
        abstract = False


class Oei6(models_dump.Oei6):
    class Meta(models_dump.Oei6.Meta):
        abstract = False


class Oei7(models_dump.Oei7):
    class Meta(models_dump.Oei7.Meta):
        abstract = False


class Oei8(models_dump.Oei8):
    class Meta(models_dump.Oei8.Meta):
        abstract = False


class Oei9(models_dump.Oei9):
    class Meta(models_dump.Oei9.Meta):
        abstract = False


class Oeit(models_dump.Oeit):
    class Meta(models_dump.Oeit.Meta):
        abstract = False


class Oejb(models_dump.Oejb):
    class Meta(models_dump.Oejb.Meta):
        abstract = False


class Oejd(models_dump.Oejd):
    class Meta(models_dump.Oejd.Meta):
        abstract = False


class Oeml(models_dump.Oeml):
    class Meta(models_dump.Oeml.Meta):
        abstract = False


class Oenc(models_dump.Oenc):
    class Meta(models_dump.Oenc.Meta):
        abstract = False


class Oent(models_dump.Oent):
    class Meta(models_dump.Oent.Meta):
        abstract = False


class Oeoy(models_dump.Oeoy):
    class Meta(models_dump.Oeoy.Meta):
        abstract = False


class Oepe(models_dump.Oepe):
    class Meta(models_dump.Oepe.Meta):
        abstract = False


class Oern(models_dump.Oern):
    class Meta(models_dump.Oern.Meta):
        abstract = False


class Oert(models_dump.Oert):
    class Meta(models_dump.Oert.Meta):
        abstract = False


class Oerx(models_dump.Oerx):
    class Meta(models_dump.Oerx.Meta):
        abstract = False


class Oest(models_dump.Oest):
    class Meta(models_dump.Oest.Meta):
        abstract = False


class Oetc(models_dump.Oetc):
    class Meta(models_dump.Oetc.Meta):
        abstract = False


class Oetm(models_dump.Oetm):
    class Meta(models_dump.Oetm.Meta):
        abstract = False


class Oeut(models_dump.Oeut):
    class Meta(models_dump.Oeut.Meta):
        abstract = False


class Oevt(models_dump.Oevt):
    class Meta(models_dump.Oevt.Meta):
        abstract = False


class Oexd(models_dump.Oexd):
    class Meta(models_dump.Oexd.Meta):
        abstract = False


class Oext(models_dump.Oext):
    class Meta(models_dump.Oext.Meta):
        abstract = False


class Ofaa(models_dump.Ofaa):
    class Meta(models_dump.Ofaa.Meta):
        abstract = False


class Ofac(models_dump.Ofac):
    class Meta(models_dump.Ofac.Meta):
        abstract = False


class Ofai(models_dump.Ofai):
    class Meta(models_dump.Ofai.Meta):
        abstract = False


class Ofam(models_dump.Ofam):
    class Meta(models_dump.Ofam.Meta):
        abstract = False


class Ofar(models_dump.Ofar):
    class Meta(models_dump.Ofar.Meta):
        abstract = False


class Ofat(models_dump.Ofat):
    class Meta(models_dump.Ofat.Meta):
        abstract = False


class Ofbt(models_dump.Ofbt):
    class Meta(models_dump.Ofbt.Meta):
        abstract = False


class Ofct(models_dump.Ofct):
    class Meta(models_dump.Ofct.Meta):
        abstract = False


class Ofix(models_dump.Ofix):
    class Meta(models_dump.Ofix.Meta):
        abstract = False


class Oflr(models_dump.Oflr):
    class Meta(models_dump.Oflr.Meta):
        abstract = False


class Oflt(models_dump.Oflt):
    class Meta(models_dump.Oflt.Meta):
        abstract = False


class Ofml(models_dump.Ofml):
    class Meta(models_dump.Ofml.Meta):
        abstract = False


class Ofnd(models_dump.Ofnd):
    class Meta(models_dump.Ofnd.Meta):
        abstract = False


class Ofns(models_dump.Ofns):
    class Meta(models_dump.Ofns.Meta):
        abstract = False


class Ofpc(models_dump.Ofpc):
    class Meta(models_dump.Ofpc.Meta):
        abstract = False


class Ofpr(models_dump.Ofpr):
    class Meta(models_dump.Ofpr.Meta):
        abstract = False


class Ofrc(models_dump.Ofrc):
    class Meta(models_dump.Ofrc.Meta):
        abstract = False


class Ofrm(models_dump.Ofrm):
    class Meta(models_dump.Ofrm.Meta):
        abstract = False


class Ofrt(models_dump.Ofrt):
    class Meta(models_dump.Ofrt.Meta):
        abstract = False


class Ofta(models_dump.Ofta):
    class Meta(models_dump.Ofta.Meta):
        abstract = False


class Oftp(models_dump.Oftp):
    class Meta(models_dump.Oftp.Meta):
        abstract = False


class Oftr(models_dump.Oftr):
    class Meta(models_dump.Oftr.Meta):
        abstract = False


class Oftt(models_dump.Oftt):
    class Meta(models_dump.Oftt.Meta):
        abstract = False


class Ofus(models_dump.Ofus):
    class Meta(models_dump.Ofus.Meta):
        abstract = False


class Ofym(models_dump.Ofym):
    class Meta(models_dump.Ofym.Meta):
        abstract = False


class Ogar(models_dump.Ogar):
    class Meta(models_dump.Ogar.Meta):
        abstract = False


class Ogbi(models_dump.Ogbi):
    class Meta(models_dump.Ogbi.Meta):
        abstract = False


class Ogdp(models_dump.Ogdp):
    class Meta(models_dump.Ogdp.Meta):
        abstract = False


class Ogfl(models_dump.Ogfl):
    class Meta(models_dump.Ogfl.Meta):
        abstract = False


class Ogpa(models_dump.Ogpa):
    class Meta(models_dump.Ogpa.Meta):
        abstract = False


class Ogpc(models_dump.Ogpc):
    class Meta(models_dump.Ogpc.Meta):
        abstract = False


class Ogrs(models_dump.Ogrs):
    class Meta(models_dump.Ogrs.Meta):
        abstract = False


class Ogsp(models_dump.Ogsp):
    class Meta(models_dump.Ogsp.Meta):
        abstract = False


class Ogti(models_dump.Ogti):
    class Meta(models_dump.Ogti.Meta):
        abstract = False


class Ogtm(models_dump.Ogtm):
    class Meta(models_dump.Ogtm.Meta):
        abstract = False


class Ogty(models_dump.Ogty):
    class Meta(models_dump.Ogty.Meta):
        abstract = False


class Ohed(models_dump.Ohed):
    class Meta(models_dump.Ohed.Meta):
        abstract = False


class Ohem(models_dump.Ohem):
    userid = models.ForeignKey(
        "Ousr", models.DO_NOTHING, db_column="userId", blank=True, null=True
    )

    class Meta(models_dump.Ohem.Meta):
        abstract = False


class Ohet(models_dump.Ohet):
    class Meta(models_dump.Ohet.Meta):
        abstract = False


class Ohfc(models_dump.Ohfc):
    class Meta(models_dump.Ohfc.Meta):
        abstract = False


class Ohld(models_dump.Ohld):
    class Meta(models_dump.Ohld.Meta):
        abstract = False


class Ohmm(models_dump.Ohmm):
    class Meta(models_dump.Ohmm.Meta):
        abstract = False


class Ohps(models_dump.Ohps):
    class Meta(models_dump.Ohps.Meta):
        abstract = False


class Ohst(models_dump.Ohst):
    class Meta(models_dump.Ohst.Meta):
        abstract = False


class Ohsv(models_dump.Ohsv):
    class Meta(models_dump.Ohsv.Meta):
        abstract = False


class Ohtm(models_dump.Ohtm):
    class Meta(models_dump.Ohtm.Meta):
        abstract = False


class Ohtr(models_dump.Ohtr):
    class Meta(models_dump.Ohtr.Meta):
        abstract = False


class Ohty(models_dump.Ohty):
    class Meta(models_dump.Ohty.Meta):
        abstract = False


class Oibq(models_dump.Oibq):
    class Meta(models_dump.Oibq.Meta):
        abstract = False


class Oicc(models_dump.Oicc):
    class Meta(models_dump.Oicc.Meta):
        abstract = False


class Oicd(models_dump.Oicd):
    class Meta(models_dump.Oicd.Meta):
        abstract = False


class Oicms(models_dump.Oicms):
    class Meta(models_dump.Oicms.Meta):
        abstract = False


class Oicp(models_dump.Oicp):
    class Meta(models_dump.Oicp.Meta):
        abstract = False


class Oidc(models_dump.Oidc):
    class Meta(models_dump.Oidc.Meta):
        abstract = False


class Oidt(models_dump.Oidt):
    class Meta(models_dump.Oidt.Meta):
        abstract = False


class Oidx(models_dump.Oidx):
    class Meta(models_dump.Oidx.Meta):
        abstract = False


class Oiei(models_dump.Oiei):
    class Meta(models_dump.Oiei.Meta):
        abstract = False


class Oier(models_dump.Oier):
    class Meta(models_dump.Oier.Meta):
        abstract = False


class Oige(models_dump.Oige):
    class Meta(models_dump.Oige.Meta):
        abstract = False


class Oign(models_dump.Oign):
    class Meta(models_dump.Oign.Meta):
        abstract = False


class Oigw(models_dump.Oigw):
    class Meta(models_dump.Oigw.Meta):
        abstract = False


class Oilm(models_dump.Oilm):
    class Meta(models_dump.Oilm.Meta):
        abstract = False


class Oimd(models_dump.Oimd):
    class Meta(models_dump.Oimd.Meta):
        abstract = False


class Oimt(models_dump.Oimt):
    class Meta(models_dump.Oimt.Meta):
        abstract = False


class Oinc(models_dump.Oinc):
    class Meta(models_dump.Oinc.Meta):
        abstract = False


class Oind(models_dump.Oind):
    class Meta(models_dump.Oind.Meta):
        abstract = False


class Oins(models_dump.Oins):
    class Meta(models_dump.Oins.Meta):
        abstract = False


class Oinv(models_dump.Oinv):
    cardcode = models.ForeignKey(
        "Ocrd",
        models.DO_NOTHING,
        db_column="CardCode",
        max_length=15,
        blank=True,
        null=True,
    )

    class Meta(models_dump.Oinv.Meta):
        abstract = False


class Oiod(models_dump.Oiod):
    class Meta(models_dump.Oiod.Meta):
        abstract = False


class Oipd(models_dump.Oipd):
    class Meta(models_dump.Oipd.Meta):
        abstract = False


class Oipf(models_dump.Oipf):
    class Meta(models_dump.Oipf.Meta):
        abstract = False


class Oipo(models_dump.Oipo):
    class Meta(models_dump.Oipo.Meta):
        abstract = False


class Oiqi(models_dump.Oiqi):
    class Meta(models_dump.Oiqi.Meta):
        abstract = False


class Oiqr(models_dump.Oiqr):
    class Meta(models_dump.Oiqr.Meta):
        abstract = False


class Oirc(models_dump.Oirc):
    class Meta(models_dump.Oirc.Meta):
        abstract = False


class Oird(models_dump.Oird):
    class Meta(models_dump.Oird.Meta):
        abstract = False


class Oiri(models_dump.Oiri):
    class Meta(models_dump.Oiri.Meta):
        abstract = False


class Oirr(models_dump.Oirr):
    class Meta(models_dump.Oirr.Meta):
        abstract = False


class Oirt(models_dump.Oirt):
    class Meta(models_dump.Oirt.Meta):
        abstract = False


class Oisc(models_dump.Oisc):
    class Meta(models_dump.Oisc.Meta):
        abstract = False


class Oisd(models_dump.Oisd):
    class Meta(models_dump.Oisd.Meta):
        abstract = False


class Oisi(models_dump.Oisi):
    class Meta(models_dump.Oisi.Meta):
        abstract = False


class Oisl(models_dump.Oisl):
    class Meta(models_dump.Oisl.Meta):
        abstract = False


class Oisr(models_dump.Oisr):
    class Meta(models_dump.Oisr.Meta):
        abstract = False


class Oist(models_dump.Oist):
    class Meta(models_dump.Oist.Meta):
        abstract = False


class Oisw(models_dump.Oisw):
    class Meta(models_dump.Oisw.Meta):
        abstract = False


class Oitb(models_dump.Oitb):
    class Meta(models_dump.Oitb.Meta):
        abstract = False


class Oitg(models_dump.Oitg):
    class Meta(models_dump.Oitg.Meta):
        abstract = False


class Oitl(models_dump.Oitl):
    class Meta(models_dump.Oitl.Meta):
        abstract = False


class Oitm(models_dump.Oitm):
    class Meta(models_dump.Oitm.Meta):
        abstract = False


class Oitr(models_dump.Oitr):
    class Meta(models_dump.Oitr.Meta):
        abstract = False


class Oitt(models_dump.Oitt):
    class Meta(models_dump.Oitt.Meta):
        abstract = False


class Oitw(models_dump.Oitw):
    class Meta(models_dump.Oitw.Meta):
        abstract = False


class Oive(models_dump.Oive):
    class Meta(models_dump.Oive.Meta):
        abstract = False


class Oivk(models_dump.Oivk):
    class Meta(models_dump.Oivk.Meta):
        abstract = False


class Oivl(models_dump.Oivl):
    class Meta(models_dump.Oivl.Meta):
        abstract = False


class Oivm(models_dump.Oivm):
    class Meta(models_dump.Oivm.Meta):
        abstract = False


class Oivq(models_dump.Oivq):
    class Meta(models_dump.Oivq.Meta):
        abstract = False


class Oiwb(models_dump.Oiwb):
    class Meta(models_dump.Oiwb.Meta):
        abstract = False


class Oiwz(models_dump.Oiwz):
    class Meta(models_dump.Oiwz.Meta):
        abstract = False


class Ojdt(models_dump.Ojdt):
    class Meta(models_dump.Ojdt.Meta):
        abstract = False


class Ojet(models_dump.Ojet):
    class Meta(models_dump.Ojet.Meta):
        abstract = False


class Ojpe(models_dump.Ojpe):
    class Meta(models_dump.Ojpe.Meta):
        abstract = False


class Ojson(models_dump.Ojson):
    class Meta(models_dump.Ojson.Meta):
        abstract = False


class Ojst(models_dump.Ojst):
    class Meta(models_dump.Ojst.Meta):
        abstract = False


class Okpf(models_dump.Okpf):
    class Meta(models_dump.Okpf.Meta):
        abstract = False


class Okpi(models_dump.Okpi):
    class Meta(models_dump.Okpi.Meta):
        abstract = False


class Okps(models_dump.Okps):
    class Meta(models_dump.Okps.Meta):
        abstract = False


class Okrt(models_dump.Okrt):
    class Meta(models_dump.Okrt.Meta):
        abstract = False


class Olcd(models_dump.Olcd):
    class Meta(models_dump.Olcd.Meta):
        abstract = False


class Olct(models_dump.Olct):
    class Meta(models_dump.Olct.Meta):
        abstract = False


class Olgl(models_dump.Olgl):
    class Meta(models_dump.Olgl.Meta):
        abstract = False


class Olgt(models_dump.Olgt):
    class Meta(models_dump.Olgt.Meta):
        abstract = False


class Ollf(models_dump.Ollf):
    class Meta(models_dump.Ollf.Meta):
        abstract = False


class Ollr(models_dump.Ollr):
    class Meta(models_dump.Ollr.Meta):
        abstract = False


class Olng(models_dump.Olng):
    class Meta(models_dump.Olng.Meta):
        abstract = False


class Olnk(models_dump.Olnk):
    class Meta(models_dump.Olnk.Meta):
        abstract = False


class Oltb(models_dump.Oltb):
    class Meta(models_dump.Oltb.Meta):
        abstract = False


class Oltf(models_dump.Oltf):
    class Meta(models_dump.Oltf.Meta):
        abstract = False


class Olti(models_dump.Olti):
    class Meta(models_dump.Olti.Meta):
        abstract = False


class Omao(models_dump.Omao):
    class Meta(models_dump.Omao.Meta):
        abstract = False


class Omap(models_dump.Omap):
    class Meta(models_dump.Omap.Meta):
        abstract = False


class Omdc(models_dump.Omdc):
    class Meta(models_dump.Omdc.Meta):
        abstract = False


class Omdp(models_dump.Omdp):
    class Meta(models_dump.Omdp.Meta):
        abstract = False


class Omdr(models_dump.Omdr):
    class Meta(models_dump.Omdr.Meta):
        abstract = False


class Omgp(models_dump.Omgp):
    class Meta(models_dump.Omgp.Meta):
        abstract = False


class Omin(models_dump.Omin):
    class Meta(models_dump.Omin.Meta):
        abstract = False


class Omiv(models_dump.Omiv):
    class Meta(models_dump.Omiv.Meta):
        abstract = False


class Omlp(models_dump.Omlp):
    class Meta(models_dump.Omlp.Meta):
        abstract = False


class Omls(models_dump.Omls):
    class Meta(models_dump.Omls.Meta):
        abstract = False


class Omlt(models_dump.Omlt):
    class Meta(models_dump.Omlt.Meta):
        abstract = False


class Ompf(models_dump.Ompf):
    class Meta(models_dump.Ompf.Meta):
        abstract = False


class Ompo(models_dump.Ompo):
    class Meta(models_dump.Ompo.Meta):
        abstract = False


class Omps(models_dump.Omps):
    class Meta(models_dump.Omps.Meta):
        abstract = False


class Omrc(models_dump.Omrc):
    class Meta(models_dump.Omrc.Meta):
        abstract = False


class Omrl(models_dump.Omrl):
    class Meta(models_dump.Omrl.Meta):
        abstract = False


class Omrv(models_dump.Omrv):
    class Meta(models_dump.Omrv.Meta):
        abstract = False


class Omsg(models_dump.Omsg):
    class Meta(models_dump.Omsg.Meta):
        abstract = False


class Omsn(models_dump.Omsn):
    class Meta(models_dump.Omsn.Meta):
        abstract = False


class Omtc(models_dump.Omtc):
    class Meta(models_dump.Omtc.Meta):
        abstract = False


class Omth(models_dump.Omth):
    class Meta(models_dump.Omth.Meta):
        abstract = False


class Omtp(models_dump.Omtp):
    class Meta(models_dump.Omtp.Meta):
        abstract = False


class Oncg(models_dump.Oncg):
    class Meta(models_dump.Oncg.Meta):
        abstract = False


class Oncm(models_dump.Oncm):
    class Meta(models_dump.Oncm.Meta):
        abstract = False


class Oncp(models_dump.Oncp):
    class Meta(models_dump.Oncp.Meta):
        abstract = False


class Onfm(models_dump.Onfm):
    class Meta(models_dump.Onfm.Meta):
        abstract = False


class Onfn(models_dump.Onfn):
    class Meta(models_dump.Onfn.Meta):
        abstract = False


class Onft(models_dump.Onft):
    class Meta(models_dump.Onft.Meta):
        abstract = False


class Onnm(models_dump.Onnm):
    class Meta(models_dump.Onnm.Meta):
        abstract = False


class Onoa(models_dump.Onoa):
    class Meta(models_dump.Onoa.Meta):
        abstract = False


class Ooat(models_dump.Ooat):
    class Meta(models_dump.Ooat.Meta):
        abstract = False


class Oocc(models_dump.Oocc):
    class Meta(models_dump.Oocc.Meta):
        abstract = False


class Oocr(models_dump.Oocr):
    class Meta(models_dump.Oocr.Meta):
        abstract = False


class Oodw(models_dump.Oodw):
    class Meta(models_dump.Oodw.Meta):
        abstract = False


class Ooei(models_dump.Ooei):
    class Meta(models_dump.Ooei.Meta):
        abstract = False


class Oofr(models_dump.Oofr):
    class Meta(models_dump.Oofr.Meta):
        abstract = False


class Ooin(models_dump.Ooin):
    class Meta(models_dump.Ooin.Meta):
        abstract = False


class Ooir(models_dump.Ooir):
    class Meta(models_dump.Ooir.Meta):
        abstract = False


class Oond(models_dump.Oond):
    class Meta(models_dump.Oond.Meta):
        abstract = False


class Oopr(models_dump.Oopr):
    class Meta(models_dump.Oopr.Meta):
        abstract = False


class Oorl(models_dump.Oorl):
    class Meta(models_dump.Oorl.Meta):
        abstract = False


class Oosr(models_dump.Oosr):
    class Meta(models_dump.Oosr.Meta):
        abstract = False


class Oost(models_dump.Oost):
    class Meta(models_dump.Oost.Meta):
        abstract = False


class Opac(models_dump.Opac):
    class Meta(models_dump.Opac.Meta):
        abstract = False


class Opbd(models_dump.Opbd):
    class Meta(models_dump.Opbd.Meta):
        abstract = False


class Opch(models_dump.Opch):
    class Meta(models_dump.Opch.Meta):
        abstract = False


class Opci(models_dump.Opci):
    class Meta(models_dump.Opci.Meta):
        abstract = False


class Opcm(models_dump.Opcm):
    class Meta(models_dump.Opcm.Meta):
        abstract = False


class Opct(models_dump.Opct):
    class Meta(models_dump.Opct.Meta):
        abstract = False


class Opdf(models_dump.Opdf):
    class Meta(models_dump.Opdf.Meta):
        abstract = False


class Opdn(models_dump.Opdn):
    class Meta(models_dump.Opdn.Meta):
        abstract = False


class Opdt(models_dump.Opdt):
    class Meta(models_dump.Opdt.Meta):
        abstract = False


class Opex(models_dump.Opex):
    class Meta(models_dump.Opex.Meta):
        abstract = False


class Opfs(models_dump.Opfs):
    class Meta(models_dump.Opfs.Meta):
        abstract = False


class Opft(models_dump.Opft):
    class Meta(models_dump.Opft.Meta):
        abstract = False


class Opha(models_dump.Opha):
    class Meta(models_dump.Opha.Meta):
        abstract = False


class Opid(models_dump.Opid):
    class Meta(models_dump.Opid.Meta):
        abstract = False


class Opjt(models_dump.Opjt):
    class Meta(models_dump.Opjt.Meta):
        abstract = False


class Opkg(models_dump.Opkg):
    class Meta(models_dump.Opkg.Meta):
        abstract = False


class Opkl(models_dump.Opkl):
    class Meta(models_dump.Opkl.Meta):
        abstract = False


class Opln(models_dump.Opln):
    class Meta(models_dump.Opln.Meta):
        abstract = False


class Opmc(models_dump.Opmc):
    class Meta(models_dump.Opmc.Meta):
        abstract = False


class Opmg(models_dump.Opmg):
    class Meta(models_dump.Opmg.Meta):
        abstract = False


class Opmx(models_dump.Opmx):
    class Meta(models_dump.Opmx.Meta):
        abstract = False


class Opoi(models_dump.Opoi):
    class Meta(models_dump.Opoi.Meta):
        abstract = False


class Opoo(models_dump.Opoo):
    class Meta(models_dump.Opoo.Meta):
        abstract = False


class Opor(models_dump.Opor):
    class Meta(models_dump.Opor.Meta):
        abstract = False


class Opos(models_dump.Opos):
    class Meta(models_dump.Opos.Meta):
        abstract = False


class Oppa(models_dump.Oppa):
    class Meta(models_dump.Oppa.Meta):
        abstract = False


class Oppt(models_dump.Oppt):
    class Meta(models_dump.Oppt.Meta):
        abstract = False


class Opqn(models_dump.Opqn):
    class Meta(models_dump.Opqn.Meta):
        abstract = False


class Opqt(models_dump.Opqt):
    class Meta(models_dump.Opqt.Meta):
        abstract = False


class Opqw(models_dump.Opqw):
    class Meta(models_dump.Opqw.Meta):
        abstract = False


class Opr1(models_dump.Opr1):
    class Meta(models_dump.Opr1.Meta):
        abstract = False


class Opr2(models_dump.Opr2):
    class Meta(models_dump.Opr2.Meta):
        abstract = False


class Opr3(models_dump.Opr3):
    class Meta(models_dump.Opr3.Meta):
        abstract = False


class Opr4(models_dump.Opr4):
    class Meta(models_dump.Opr4.Meta):
        abstract = False


class Opr5(models_dump.Opr5):
    class Meta(models_dump.Opr5.Meta):
        abstract = False


class Opra(models_dump.Opra):
    class Meta(models_dump.Opra.Meta):
        abstract = False


class Oprc(models_dump.Oprc):
    class Meta(models_dump.Oprc.Meta):
        abstract = False


class Oprf(models_dump.Oprf):
    class Meta(models_dump.Oprf.Meta):
        abstract = False


class Opri(models_dump.Opri):
    class Meta(models_dump.Opri.Meta):
        abstract = False


class Oprj(models_dump.Oprj):
    class Meta(models_dump.Oprj.Meta):
        abstract = False


class Opro(models_dump.Opro):
    class Meta(models_dump.Opro.Meta):
        abstract = False


class Oprq(models_dump.Oprq):
    class Meta(models_dump.Oprq.Meta):
        abstract = False


class Oprr(models_dump.Oprr):
    class Meta(models_dump.Oprr.Meta):
        abstract = False


class Oprs(models_dump.Oprs):
    class Meta(models_dump.Oprs.Meta):
        abstract = False


class Oprt(models_dump.Oprt):
    class Meta(models_dump.Oprt.Meta):
        abstract = False


class Opsc(models_dump.Opsc):
    class Meta(models_dump.Opsc.Meta):
        abstract = False


class Opsg(models_dump.Opsg):
    class Meta(models_dump.Opsg.Meta):
        abstract = False


class Opst(models_dump.Opst):
    class Meta(models_dump.Opst.Meta):
        abstract = False


class Optf(models_dump.Optf):
    class Meta(models_dump.Optf.Meta):
        abstract = False


class Opti(models_dump.Opti):
    class Meta(models_dump.Opti.Meta):
        abstract = False


class Optr(models_dump.Optr):
    class Meta(models_dump.Optr.Meta):
        abstract = False


class Opvl(models_dump.Opvl):
    class Meta(models_dump.Opvl.Meta):
        abstract = False


class Opwb(models_dump.Opwb):
    class Meta(models_dump.Opwb.Meta):
        abstract = False


class Opwz(models_dump.Opwz):
    class Meta(models_dump.Opwz.Meta):
        abstract = False


class Opyb(models_dump.Opyb):
    class Meta(models_dump.Opyb.Meta):
        abstract = False


class Opyd(models_dump.Opyd):
    class Meta(models_dump.Opyd.Meta):
        abstract = False


class Opym(models_dump.Opym):
    class Meta(models_dump.Opym.Meta):
        abstract = False


class Opyr(models_dump.Opyr):
    class Meta(models_dump.Opyr.Meta):
        abstract = False


class Oqag(models_dump.Oqag):
    class Meta(models_dump.Oqag.Meta):
        abstract = False


class Oqcn(models_dump.Oqcn):
    class Meta(models_dump.Oqcn.Meta):
        abstract = False


class Oqfd(models_dump.Oqfd):
    class Meta(models_dump.Oqfd.Meta):
        abstract = False


class Oqrc(models_dump.Oqrc):
    class Meta(models_dump.Oqrc.Meta):
        abstract = False


class Oque(models_dump.Oque):
    class Meta(models_dump.Oque.Meta):
        abstract = False


class Oqut(models_dump.Oqut):
    cardcode = models.ForeignKey(
        "Ocrd",
        models.DO_NOTHING,
        db_column="CardCode",
        max_length=15,
        blank=True,
        null=True,
    )

    class Meta(models_dump.Oqut.Meta):
        abstract = False


class Oqwz(models_dump.Oqwz):
    class Meta(models_dump.Oqwz.Meta):
        abstract = False


class Orci(models_dump.Orci):
    class Meta(models_dump.Orci.Meta):
        abstract = False


class Orcj(models_dump.Orcj):
    class Meta(models_dump.Orcj.Meta):
        abstract = False


class Orcl(models_dump.Orcl):
    class Meta(models_dump.Orcl.Meta):
        abstract = False


class Orcm(models_dump.Orcm):
    class Meta(models_dump.Orcm.Meta):
        abstract = False


class Orcn(models_dump.Orcn):
    class Meta(models_dump.Orcn.Meta):
        abstract = False


class Orcp(models_dump.Orcp):
    class Meta(models_dump.Orcp.Meta):
        abstract = False


class Orcr(models_dump.Orcr):
    class Meta(models_dump.Orcr.Meta):
        abstract = False


class Orct(models_dump.Orct):
    class Meta(models_dump.Orct.Meta):
        abstract = False


class Ordn(models_dump.Ordn):
    class Meta(models_dump.Ordn.Meta):
        abstract = False


class Ordr(models_dump.Ordr):
    class Meta(models_dump.Ordr.Meta):
        abstract = False


class Orea(models_dump.Orea):
    class Meta(models_dump.Orea.Meta):
        abstract = False


class Oreq(models_dump.Oreq):
    class Meta(models_dump.Oreq.Meta):
        abstract = False


class Orer(models_dump.Orer):
    class Meta(models_dump.Orer.Meta):
        abstract = False


class Orfl(models_dump.Orfl):
    class Meta(models_dump.Orfl.Meta):
        abstract = False


class Orin(models_dump.Orin):
    cardcode = models.ForeignKey(
        "Ocrd",
        models.DO_NOTHING,
        db_column="CardCode",
        max_length=15,
        blank=True,
        null=True,
    )

    class Meta(models_dump.Orin.Meta):
        abstract = False


class Orit(models_dump.Orit):
    class Meta(models_dump.Orit.Meta):
        abstract = False


class Orld(models_dump.Orld):
    class Meta(models_dump.Orld.Meta):
        abstract = False


class Orls(models_dump.Orls):
    class Meta(models_dump.Orls.Meta):
        abstract = False


class Ormk(models_dump.Ormk):
    class Meta(models_dump.Ormk.Meta):
        abstract = False


class Oroc(models_dump.Oroc):
    class Meta(models_dump.Oroc.Meta):
        abstract = False


class Orpc(models_dump.Orpc):
    class Meta(models_dump.Orpc.Meta):
        abstract = False


class Orpd(models_dump.Orpd):
    class Meta(models_dump.Orpd.Meta):
        abstract = False


class Orpt(models_dump.Orpt):
    class Meta(models_dump.Orpt.Meta):
        abstract = False


class Orrr(models_dump.Orrr):
    class Meta(models_dump.Orrr.Meta):
        abstract = False


class Orsb(models_dump.Orsb):
    class Meta(models_dump.Orsb.Meta):
        abstract = False


class Orsc(models_dump.Orsc):
    class Meta(models_dump.Orsc.Meta):
        abstract = False


class Orsg(models_dump.Orsg):
    class Meta(models_dump.Orsg.Meta):
        abstract = False


class Orst(models_dump.Orst):
    class Meta(models_dump.Orst.Meta):
        abstract = False


class Orti(models_dump.Orti):
    class Meta(models_dump.Orti.Meta):
        abstract = False


class Ortl(models_dump.Ortl):
    class Meta(models_dump.Ortl.Meta):
        abstract = False


class Ortm(models_dump.Ortm):
    class Meta(models_dump.Ortm.Meta):
        abstract = False


class Orts(models_dump.Orts):
    class Meta(models_dump.Orts.Meta):
        abstract = False


class Ortt(models_dump.Ortt):
    class Meta(models_dump.Ortt.Meta):
        abstract = False


class Ortw(models_dump.Ortw):
    class Meta(models_dump.Ortw.Meta):
        abstract = False


class Orvc(models_dump.Orvc):
    class Meta(models_dump.Orvc.Meta):
        abstract = False


class Osab(models_dump.Osab):
    class Meta(models_dump.Osab.Meta):
        abstract = False


class Osac(models_dump.Osac):
    class Meta(models_dump.Osac.Meta):
        abstract = False


class Osal(models_dump.Osal):
    class Meta(models_dump.Osal.Meta):
        abstract = False


class Osas(models_dump.Osas):
    class Meta(models_dump.Osas.Meta):
        abstract = False


class Osbq(models_dump.Osbq):
    class Meta(models_dump.Osbq.Meta):
        abstract = False


class Oscd(models_dump.Oscd):
    class Meta(models_dump.Oscd.Meta):
        abstract = False


class Oscg(models_dump.Oscg):
    class Meta(models_dump.Oscg.Meta):
        abstract = False


class Oscl(models_dump.Oscl):
    class Meta(models_dump.Oscl.Meta):
        abstract = False


class Oscm(models_dump.Oscm):
    class Meta(models_dump.Oscm.Meta):
        abstract = False


class Oscn(models_dump.Oscn):
    class Meta(models_dump.Oscn.Meta):
        abstract = False


class Osco(models_dump.Osco):
    class Meta(models_dump.Osco.Meta):
        abstract = False


class Oscp(models_dump.Oscp):
    class Meta(models_dump.Oscp.Meta):
        abstract = False


class Oscr(models_dump.Oscr):
    class Meta(models_dump.Oscr.Meta):
        abstract = False


class Oscs(models_dump.Oscs):
    class Meta(models_dump.Oscs.Meta):
        abstract = False


class Osct(models_dump.Osct):
    class Meta(models_dump.Osct.Meta):
        abstract = False


class Osdl(models_dump.Osdl):
    class Meta(models_dump.Osdl.Meta):
        abstract = False


class Osec(models_dump.Osec):
    class Meta(models_dump.Osec.Meta):
        abstract = False


class Osel(models_dump.Osel):
    class Meta(models_dump.Osel.Meta):
        abstract = False


class Oses(models_dump.Oses):
    class Meta(models_dump.Oses.Meta):
        abstract = False


class Osfc(models_dump.Osfc):
    class Meta(models_dump.Osfc.Meta):
        abstract = False


class Osfi(models_dump.Osfi):
    class Meta(models_dump.Osfi.Meta):
        abstract = False


class Osgp(models_dump.Osgp):
    class Meta(models_dump.Osgp.Meta):
        abstract = False


class Oshp(models_dump.Oshp):
    class Meta(models_dump.Oshp.Meta):
        abstract = False


class Oshr(models_dump.Oshr):
    class Meta(models_dump.Oshr.Meta):
        abstract = False


class Osld(models_dump.Osld):
    class Meta(models_dump.Osld.Meta):
        abstract = False


class Oslm(models_dump.Oslm):
    class Meta(models_dump.Oslm.Meta):
        abstract = False


class Oslp(models_dump.Oslp):
    class Meta(models_dump.Oslp.Meta):
        abstract = False


class Oslr(models_dump.Oslr):
    class Meta(models_dump.Oslr.Meta):
        abstract = False


class Oslt(models_dump.Oslt):
    class Meta(models_dump.Oslt.Meta):
        abstract = False


class Osoi(models_dump.Osoi):
    class Meta(models_dump.Osoi.Meta):
        abstract = False


class Osoil(models_dump.Osoil):
    class Meta(models_dump.Osoil.Meta):
        abstract = False


class Ospg(models_dump.Ospg):
    class Meta(models_dump.Ospg.Meta):
        abstract = False


class Ospp(models_dump.Ospp):
    class Meta(models_dump.Ospp.Meta):
        abstract = False


class Osql(models_dump.Osql):
    class Meta(models_dump.Osql.Meta):
        abstract = False


class Osqr(models_dump.Osqr):
    class Meta(models_dump.Osqr.Meta):
        abstract = False


class Osra(models_dump.Osra):
    class Meta(models_dump.Osra.Meta):
        abstract = False


class Osrc(models_dump.Osrc):
    class Meta(models_dump.Osrc.Meta):
        abstract = False


class Osrl(models_dump.Osrl):
    class Meta(models_dump.Osrl.Meta):
        abstract = False


class Osrn(models_dump.Osrn):
    class Meta(models_dump.Osrn.Meta):
        abstract = False


class Osrq(models_dump.Osrq):
    class Meta(models_dump.Osrq.Meta):
        abstract = False


class Osrt(models_dump.Osrt):
    class Meta(models_dump.Osrt.Meta):
        abstract = False


class Osrw(models_dump.Osrw):
    class Meta(models_dump.Osrw.Meta):
        abstract = False


class Ossg(models_dump.Ossg):
    class Meta(models_dump.Ossg.Meta):
        abstract = False


class Ossp(models_dump.Ossp):
    class Meta(models_dump.Ossp.Meta):
        abstract = False


class Osst(models_dump.Osst):
    class Meta(models_dump.Osst.Meta):
        abstract = False


class Osta(models_dump.Osta):
    class Meta(models_dump.Osta.Meta):
        abstract = False


class Ostb(models_dump.Ostb):
    class Meta(models_dump.Ostb.Meta):
        abstract = False


class Ostc(models_dump.Ostc):
    class Meta(models_dump.Ostc.Meta):
        abstract = False


class Ostg(models_dump.Ostg):
    class Meta(models_dump.Ostg.Meta):
        abstract = False


class Ostq(models_dump.Ostq):
    class Meta(models_dump.Ostq.Meta):
        abstract = False


class Osts(models_dump.Osts):
    class Meta(models_dump.Osts.Meta):
        abstract = False


class Ostt(models_dump.Ostt):
    class Meta(models_dump.Ostt.Meta):
        abstract = False


class Osuc(models_dump.Osuc):
    class Meta(models_dump.Osuc.Meta):
        abstract = False


class Osul(models_dump.Osul):
    class Meta(models_dump.Osul.Meta):
        abstract = False


class Osus(models_dump.Osus):
    class Meta(models_dump.Osus.Meta):
        abstract = False


class Osvm(models_dump.Osvm):
    class Meta(models_dump.Osvm.Meta):
        abstract = False


class Osvr(models_dump.Osvr):
    class Meta(models_dump.Osvr.Meta):
        abstract = False


class Osvt(models_dump.Osvt):
    class Meta(models_dump.Osvt.Meta):
        abstract = False


class Oswa(models_dump.Oswa):
    class Meta(models_dump.Oswa.Meta):
        abstract = False


class Otax(models_dump.Otax):
    class Meta(models_dump.Otax.Meta):
        abstract = False


class Otbp(models_dump.Otbp):
    class Meta(models_dump.Otbp.Meta):
        abstract = False


class Otcc(models_dump.Otcc):
    class Meta(models_dump.Otcc.Meta):
        abstract = False


class Otcd(models_dump.Otcd):
    class Meta(models_dump.Otcd.Meta):
        abstract = False


class Otcn(models_dump.Otcn):
    class Meta(models_dump.Otcn.Meta):
        abstract = False


class Otcx(models_dump.Otcx):
    class Meta(models_dump.Otcx.Meta):
        abstract = False


class Oter(models_dump.Oter):
    class Meta(models_dump.Oter.Meta):
        abstract = False


class Otfc(models_dump.Otfc):
    class Meta(models_dump.Otfc.Meta):
        abstract = False


class Otgg(models_dump.Otgg):
    class Meta(models_dump.Otgg.Meta):
        abstract = False


class Othl(models_dump.Othl):
    class Meta(models_dump.Othl.Meta):
        abstract = False


class Otiz(models_dump.Otiz):
    class Meta(models_dump.Otiz.Meta):
        abstract = False


class Otnc(models_dump.Otnc):
    class Meta(models_dump.Otnc.Meta):
        abstract = False


class Otnl(models_dump.Otnl):
    class Meta(models_dump.Otnl.Meta):
        abstract = False


class Otnn(models_dump.Otnn):
    class Meta(models_dump.Otnn.Meta):
        abstract = False


class Otob(models_dump.Otob):
    class Meta(models_dump.Otob.Meta):
        abstract = False


class Otof(models_dump.Otof):
    class Meta(models_dump.Otof.Meta):
        abstract = False


class Otpa(models_dump.Otpa):
    class Meta(models_dump.Otpa.Meta):
        abstract = False


class Otpi(models_dump.Otpi):
    class Meta(models_dump.Otpi.Meta):
        abstract = False


class Otpl(models_dump.Otpl):
    class Meta(models_dump.Otpl.Meta):
        abstract = False


class Otpr(models_dump.Otpr):
    class Meta(models_dump.Otpr.Meta):
        abstract = False


class Otps(models_dump.Otps):
    class Meta(models_dump.Otps.Meta):
        abstract = False


class Otpw(models_dump.Otpw):
    class Meta(models_dump.Otpw.Meta):
        abstract = False


class Otra(models_dump.Otra):
    class Meta(models_dump.Otra.Meta):
        abstract = False


class Otrb(models_dump.Otrb):
    class Meta(models_dump.Otrb.Meta):
        abstract = False


class Otrc(models_dump.Otrc):
    class Meta(models_dump.Otrc.Meta):
        abstract = False


class Otrn(models_dump.Otrn):
    class Meta(models_dump.Otrn.Meta):
        abstract = False


class Otro(models_dump.Otro):
    class Meta(models_dump.Otro.Meta):
        abstract = False


class Otrs(models_dump.Otrs):
    class Meta(models_dump.Otrs.Meta):
        abstract = False


class Otrss(models_dump.Otrss):
    class Meta(models_dump.Otrss.Meta):
        abstract = False


class Otrt(models_dump.Otrt):
    class Meta(models_dump.Otrt.Meta):
        abstract = False


class Otrx(models_dump.Otrx):
    class Meta(models_dump.Otrx.Meta):
        abstract = False


class Otsc(models_dump.Otsc):
    class Meta(models_dump.Otsc.Meta):
        abstract = False


class Otsh(models_dump.Otsh):
    class Meta(models_dump.Otsh.Meta):
        abstract = False


class Otsi(models_dump.Otsi):
    class Meta(models_dump.Otsi.Meta):
        abstract = False


class Otsm(models_dump.Otsm):
    class Meta(models_dump.Otsm.Meta):
        abstract = False


class Otsp(models_dump.Otsp):
    class Meta(models_dump.Otsp.Meta):
        abstract = False


class Ottp(models_dump.Ottp):
    class Meta(models_dump.Ottp.Meta):
        abstract = False


class Ottr(models_dump.Ottr):
    class Meta(models_dump.Ottr.Meta):
        abstract = False


class Otws(models_dump.Otws):
    class Meta(models_dump.Otws.Meta):
        abstract = False


class Otxd(models_dump.Otxd):
    class Meta(models_dump.Otxd.Meta):
        abstract = False


class Oual(models_dump.Oual):
    class Meta(models_dump.Oual.Meta):
        abstract = False


class Oubr(models_dump.Oubr):
    class Meta(models_dump.Oubr.Meta):
        abstract = False


class Oudg(models_dump.Oudg):
    class Meta(models_dump.Oudg.Meta):
        abstract = False


class Oudo(models_dump.Oudo):
    class Meta(models_dump.Oudo.Meta):
        abstract = False


class Oudp(models_dump.Oudp):
    class Meta(models_dump.Oudp.Meta):
        abstract = False


class Oudv(models_dump.Oudv):
    class Meta(models_dump.Oudv.Meta):
        abstract = False


class Ougp(models_dump.Ougp):
    class Meta(models_dump.Ougp.Meta):
        abstract = False


class Ougr(models_dump.Ougr):
    class Meta(models_dump.Ougr.Meta):
        abstract = False


class Oukd(models_dump.Oukd):
    class Meta(models_dump.Oukd.Meta):
        abstract = False


class Oula(models_dump.Oula):
    class Meta(models_dump.Oula.Meta):
        abstract = False


class Ouncl(models_dump.Ouncl):
    class Meta(models_dump.Ouncl.Meta):
        abstract = False


class Ouom(models_dump.Ouom):
    class Meta(models_dump.Ouom.Meta):
        abstract = False


class Oupt(models_dump.Oupt):
    class Meta(models_dump.Oupt.Meta):
        abstract = False


class Ouqr(models_dump.Ouqr):
    class Meta(models_dump.Ouqr.Meta):
        abstract = False


class Ousg(models_dump.Ousg):
    class Meta(models_dump.Ousg.Meta):
        abstract = False


class Ousr(models_dump.Ousr):
    class Meta(models_dump.Ousr.Meta):
        abstract = False


class Outb(models_dump.Outb):
    class Meta(models_dump.Outb.Meta):
        abstract = False


class Outx(models_dump.Outx):
    class Meta(models_dump.Outx.Meta):
        abstract = False


class Ouwtx(models_dump.Ouwtx):
    class Meta(models_dump.Ouwtx.Meta):
        abstract = False


class Oveb(models_dump.Oveb):
    class Meta(models_dump.Oveb.Meta):
        abstract = False


class Ovec(models_dump.Ovec):
    class Meta(models_dump.Ovec.Meta):
        abstract = False


class Ovet(models_dump.Ovet):
    class Meta(models_dump.Ovet.Meta):
        abstract = False


class Ovmc(models_dump.Ovmc):
    class Meta(models_dump.Ovmc.Meta):
        abstract = False


class Ovnm(models_dump.Ovnm):
    class Meta(models_dump.Ovnm.Meta):
        abstract = False


class Ovpm(models_dump.Ovpm):
    class Meta(models_dump.Ovpm.Meta):
        abstract = False


class Ovrt(models_dump.Ovrt):
    class Meta(models_dump.Ovrt.Meta):
        abstract = False


class Ovrw(models_dump.Ovrw):
    class Meta(models_dump.Ovrw.Meta):
        abstract = False


class Ovtg(models_dump.Ovtg):
    class Meta(models_dump.Ovtg.Meta):
        abstract = False


class Ovtp(models_dump.Ovtp):
    class Meta(models_dump.Ovtp.Meta):
        abstract = False


class Ovtr(models_dump.Ovtr):
    class Meta(models_dump.Ovtr.Meta):
        abstract = False


class Owdbd(models_dump.Owdbd):
    class Meta(models_dump.Owdbd.Meta):
        abstract = False


class Owdd(models_dump.Owdd):
    class Meta(models_dump.Owdd.Meta):
        abstract = False


class Owdt(models_dump.Owdt):
    class Meta(models_dump.Owdt.Meta):
        abstract = False


class Owex(models_dump.Owex):
    class Meta(models_dump.Owex.Meta):
        abstract = False


class Owfer(models_dump.Owfer):
    class Meta(models_dump.Owfer.Meta):
        abstract = False


class Owfi(models_dump.Owfi):
    class Meta(models_dump.Owfi.Meta):
        abstract = False


class Owflt(models_dump.Owflt):
    class Meta(models_dump.Owflt.Meta):
        abstract = False


class Owfst(models_dump.Owfst):
    class Meta(models_dump.Owfst.Meta):
        abstract = False


class Owgt(models_dump.Owgt):
    class Meta(models_dump.Owgt.Meta):
        abstract = False


class Owhc(models_dump.Owhc):
    class Meta(models_dump.Owhc.Meta):
        abstract = False


class Owhl(models_dump.Owhl):
    class Meta(models_dump.Owhl.Meta):
        abstract = False


class Owhs(models_dump.Owhs):
    class Meta(models_dump.Owhs.Meta):
        abstract = False


class Owht(models_dump.Owht):
    class Meta(models_dump.Owht.Meta):
        abstract = False


class Owin(models_dump.Owin):
    class Meta(models_dump.Owin.Meta):
        abstract = False


class Owjb(models_dump.Owjb):
    class Meta(models_dump.Owjb.Meta):
        abstract = False


class Owko(models_dump.Owko):
    class Meta(models_dump.Owko.Meta):
        abstract = False


class Owlbt(models_dump.Owlbt):
    class Meta(models_dump.Owlbt.Meta):
        abstract = False


class Owlpd(models_dump.Owlpd):
    class Meta(models_dump.Owlpd.Meta):
        abstract = False


class Owls(models_dump.Owls):
    class Meta(models_dump.Owls.Meta):
        abstract = False


class Owmg(models_dump.Owmg):
    class Meta(models_dump.Owmg.Meta):
        abstract = False


class Ownot(models_dump.Ownot):
    class Meta(models_dump.Ownot.Meta):
        abstract = False


class Owor(models_dump.Owor):
    class Meta(models_dump.Owor.Meta):
        abstract = False


class Owpk(models_dump.Owpk):
    class Meta(models_dump.Owpk.Meta):
        abstract = False


class Owst(models_dump.Owst):
    class Meta(models_dump.Owst.Meta):
        abstract = False


class Owta(models_dump.Owta):
    class Meta(models_dump.Owta.Meta):
        abstract = False


class Owtc(models_dump.Owtc):
    class Meta(models_dump.Owtc.Meta):
        abstract = False


class Owtd(models_dump.Owtd):
    class Meta(models_dump.Owtd.Meta):
        abstract = False


class Owti(models_dump.Owti):
    class Meta(models_dump.Owti.Meta):
        abstract = False


class Owtj(models_dump.Owtj):
    class Meta(models_dump.Owtj.Meta):
        abstract = False


class Owtm(models_dump.Owtm):
    class Meta(models_dump.Owtm.Meta):
        abstract = False


class Owtq(models_dump.Owtq):
    class Meta(models_dump.Owtq.Meta):
        abstract = False


class Owtr(models_dump.Owtr):
    class Meta(models_dump.Owtr.Meta):
        abstract = False


class Owts(models_dump.Owts):
    class Meta(models_dump.Owts.Meta):
        abstract = False


class Owtt(models_dump.Owtt):
    class Meta(models_dump.Owtt.Meta):
        abstract = False


class Owtx(models_dump.Owtx):
    class Meta(models_dump.Owtx.Meta):
        abstract = False


class Owuac(models_dump.Owuac):
    class Meta(models_dump.Owuac.Meta):
        abstract = False


class Owupr(models_dump.Owupr):
    class Meta(models_dump.Owupr.Meta):
        abstract = False


class Owvg(models_dump.Owvg):
    class Meta(models_dump.Owvg.Meta):
        abstract = False


class Owvt(models_dump.Owvt):
    class Meta(models_dump.Owvt.Meta):
        abstract = False


class Owwt(models_dump.Owwt):
    class Meta(models_dump.Owwt.Meta):
        abstract = False


class Owxt(models_dump.Owxt):
    class Meta(models_dump.Owxt.Meta):
        abstract = False


class Oxap(models_dump.Oxap):
    class Meta(models_dump.Oxap.Meta):
        abstract = False


class Ozrd(models_dump.Ozrd):
    class Meta(models_dump.Ozrd.Meta):
        abstract = False


class Pact(models_dump.Pact):
    class Meta(models_dump.Pact.Meta):
        abstract = False


class Pact1(models_dump.Pact1):
    class Meta(models_dump.Pact1.Meta):
        abstract = False


class Pact3(models_dump.Pact3):
    class Meta(models_dump.Pact3.Meta):
        abstract = False


class Pch1(models_dump.Pch1):
    class Meta(models_dump.Pch1.Meta):
        abstract = False


class Pch10(models_dump.Pch10):
    class Meta(models_dump.Pch10.Meta):
        abstract = False


class Pch11(models_dump.Pch11):
    class Meta(models_dump.Pch11.Meta):
        abstract = False


class Pch12(models_dump.Pch12):
    class Meta(models_dump.Pch12.Meta):
        abstract = False


class Pch13(models_dump.Pch13):
    class Meta(models_dump.Pch13.Meta):
        abstract = False


class Pch14(models_dump.Pch14):
    class Meta(models_dump.Pch14.Meta):
        abstract = False


class Pch15(models_dump.Pch15):
    class Meta(models_dump.Pch15.Meta):
        abstract = False


class Pch16(models_dump.Pch16):
    class Meta(models_dump.Pch16.Meta):
        abstract = False


class Pch17(models_dump.Pch17):
    class Meta(models_dump.Pch17.Meta):
        abstract = False


class Pch18(models_dump.Pch18):
    class Meta(models_dump.Pch18.Meta):
        abstract = False


class Pch19(models_dump.Pch19):
    class Meta(models_dump.Pch19.Meta):
        abstract = False


class Pch2(models_dump.Pch2):
    class Meta(models_dump.Pch2.Meta):
        abstract = False


class Pch20(models_dump.Pch20):
    class Meta(models_dump.Pch20.Meta):
        abstract = False


class Pch21(models_dump.Pch21):
    class Meta(models_dump.Pch21.Meta):
        abstract = False


class Pch22(models_dump.Pch22):
    class Meta(models_dump.Pch22.Meta):
        abstract = False


class Pch23(models_dump.Pch23):
    class Meta(models_dump.Pch23.Meta):
        abstract = False


class Pch24(models_dump.Pch24):
    class Meta(models_dump.Pch24.Meta):
        abstract = False


class Pch25(models_dump.Pch25):
    class Meta(models_dump.Pch25.Meta):
        abstract = False


class Pch26(models_dump.Pch26):
    class Meta(models_dump.Pch26.Meta):
        abstract = False


class Pch27(models_dump.Pch27):
    class Meta(models_dump.Pch27.Meta):
        abstract = False


class Pch28(models_dump.Pch28):
    class Meta(models_dump.Pch28.Meta):
        abstract = False


class Pch3(models_dump.Pch3):
    class Meta(models_dump.Pch3.Meta):
        abstract = False


class Pch4(models_dump.Pch4):
    class Meta(models_dump.Pch4.Meta):
        abstract = False


class Pch5(models_dump.Pch5):
    class Meta(models_dump.Pch5.Meta):
        abstract = False


class Pch6(models_dump.Pch6):
    class Meta(models_dump.Pch6.Meta):
        abstract = False


class Pch7(models_dump.Pch7):
    class Meta(models_dump.Pch7.Meta):
        abstract = False


class Pch8(models_dump.Pch8):
    class Meta(models_dump.Pch8.Meta):
        abstract = False


class Pch9(models_dump.Pch9):
    class Meta(models_dump.Pch9.Meta):
        abstract = False


class Pci1(models_dump.Pci1):
    class Meta(models_dump.Pci1.Meta):
        abstract = False


class Pdf1(models_dump.Pdf1):
    class Meta(models_dump.Pdf1.Meta):
        abstract = False


class Pdf2(models_dump.Pdf2):
    class Meta(models_dump.Pdf2.Meta):
        abstract = False


class Pdf3(models_dump.Pdf3):
    class Meta(models_dump.Pdf3.Meta):
        abstract = False


class Pdf4(models_dump.Pdf4):
    class Meta(models_dump.Pdf4.Meta):
        abstract = False


class Pdf5(models_dump.Pdf5):
    class Meta(models_dump.Pdf5.Meta):
        abstract = False


class Pdf6(models_dump.Pdf6):
    class Meta(models_dump.Pdf6.Meta):
        abstract = False


class Pdf7(models_dump.Pdf7):
    class Meta(models_dump.Pdf7.Meta):
        abstract = False


class Pdf8(models_dump.Pdf8):
    class Meta(models_dump.Pdf8.Meta):
        abstract = False


class Pdf9(models_dump.Pdf9):
    class Meta(models_dump.Pdf9.Meta):
        abstract = False


class Pdn1(models_dump.Pdn1):
    class Meta(models_dump.Pdn1.Meta):
        abstract = False


class Pdn10(models_dump.Pdn10):
    class Meta(models_dump.Pdn10.Meta):
        abstract = False


class Pdn11(models_dump.Pdn11):
    class Meta(models_dump.Pdn11.Meta):
        abstract = False


class Pdn12(models_dump.Pdn12):
    class Meta(models_dump.Pdn12.Meta):
        abstract = False


class Pdn13(models_dump.Pdn13):
    class Meta(models_dump.Pdn13.Meta):
        abstract = False


class Pdn14(models_dump.Pdn14):
    class Meta(models_dump.Pdn14.Meta):
        abstract = False


class Pdn15(models_dump.Pdn15):
    class Meta(models_dump.Pdn15.Meta):
        abstract = False


class Pdn16(models_dump.Pdn16):
    class Meta(models_dump.Pdn16.Meta):
        abstract = False


class Pdn17(models_dump.Pdn17):
    class Meta(models_dump.Pdn17.Meta):
        abstract = False


class Pdn18(models_dump.Pdn18):
    class Meta(models_dump.Pdn18.Meta):
        abstract = False


class Pdn19(models_dump.Pdn19):
    class Meta(models_dump.Pdn19.Meta):
        abstract = False


class Pdn2(models_dump.Pdn2):
    class Meta(models_dump.Pdn2.Meta):
        abstract = False


class Pdn20(models_dump.Pdn20):
    class Meta(models_dump.Pdn20.Meta):
        abstract = False


class Pdn21(models_dump.Pdn21):
    class Meta(models_dump.Pdn21.Meta):
        abstract = False


class Pdn22(models_dump.Pdn22):
    class Meta(models_dump.Pdn22.Meta):
        abstract = False


class Pdn23(models_dump.Pdn23):
    class Meta(models_dump.Pdn23.Meta):
        abstract = False


class Pdn24(models_dump.Pdn24):
    class Meta(models_dump.Pdn24.Meta):
        abstract = False


class Pdn25(models_dump.Pdn25):
    class Meta(models_dump.Pdn25.Meta):
        abstract = False


class Pdn26(models_dump.Pdn26):
    class Meta(models_dump.Pdn26.Meta):
        abstract = False


class Pdn27(models_dump.Pdn27):
    class Meta(models_dump.Pdn27.Meta):
        abstract = False


class Pdn28(models_dump.Pdn28):
    class Meta(models_dump.Pdn28.Meta):
        abstract = False


class Pdn3(models_dump.Pdn3):
    class Meta(models_dump.Pdn3.Meta):
        abstract = False


class Pdn4(models_dump.Pdn4):
    class Meta(models_dump.Pdn4.Meta):
        abstract = False


class Pdn5(models_dump.Pdn5):
    class Meta(models_dump.Pdn5.Meta):
        abstract = False


class Pdn6(models_dump.Pdn6):
    class Meta(models_dump.Pdn6.Meta):
        abstract = False


class Pdn7(models_dump.Pdn7):
    class Meta(models_dump.Pdn7.Meta):
        abstract = False


class Pdn8(models_dump.Pdn8):
    class Meta(models_dump.Pdn8.Meta):
        abstract = False


class Pdn9(models_dump.Pdn9):
    class Meta(models_dump.Pdn9.Meta):
        abstract = False


class Pex1(models_dump.Pex1):
    class Meta(models_dump.Pex1.Meta):
        abstract = False


class Pha1(models_dump.Pha1):
    class Meta(models_dump.Pha1.Meta):
        abstract = False


class Pha2(models_dump.Pha2):
    class Meta(models_dump.Pha2.Meta):
        abstract = False


class Pha3(models_dump.Pha3):
    class Meta(models_dump.Pha3.Meta):
        abstract = False


class Pha4(models_dump.Pha4):
    class Meta(models_dump.Pha4.Meta):
        abstract = False


class Pha5(models_dump.Pha5):
    class Meta(models_dump.Pha5.Meta):
        abstract = False


class Pha6(models_dump.Pha6):
    class Meta(models_dump.Pha6.Meta):
        abstract = False


class Pha7(models_dump.Pha7):
    class Meta(models_dump.Pha7.Meta):
        abstract = False


class Pha8(models_dump.Pha8):
    class Meta(models_dump.Pha8.Meta):
        abstract = False


class Pjt1(models_dump.Pjt1):
    class Meta(models_dump.Pjt1.Meta):
        abstract = False


class Pjt2(models_dump.Pjt2):
    class Meta(models_dump.Pjt2.Meta):
        abstract = False


class Pkl1(models_dump.Pkl1):
    class Meta(models_dump.Pkl1.Meta):
        abstract = False


class Pkl2(models_dump.Pkl2):
    class Meta(models_dump.Pkl2.Meta):
        abstract = False


class Pmc1(models_dump.Pmc1):
    class Meta(models_dump.Pmc1.Meta):
        abstract = False


class Pmc2(models_dump.Pmc2):
    class Meta(models_dump.Pmc2.Meta):
        abstract = False


class Pmc3(models_dump.Pmc3):
    class Meta(models_dump.Pmc3.Meta):
        abstract = False


class Pmc4(models_dump.Pmc4):
    class Meta(models_dump.Pmc4.Meta):
        abstract = False


class Pmc5(models_dump.Pmc5):
    class Meta(models_dump.Pmc5.Meta):
        abstract = False


class Pmc6(models_dump.Pmc6):
    class Meta(models_dump.Pmc6.Meta):
        abstract = False


class Pmg1(models_dump.Pmg1):
    class Meta(models_dump.Pmg1.Meta):
        abstract = False


class Pmg2(models_dump.Pmg2):
    class Meta(models_dump.Pmg2.Meta):
        abstract = False


class Pmg3(models_dump.Pmg3):
    class Meta(models_dump.Pmg3.Meta):
        abstract = False


class Pmg4(models_dump.Pmg4):
    class Meta(models_dump.Pmg4.Meta):
        abstract = False


class Pmg5(models_dump.Pmg5):
    class Meta(models_dump.Pmg5.Meta):
        abstract = False


class Pmg6(models_dump.Pmg6):
    class Meta(models_dump.Pmg6.Meta):
        abstract = False


class Pmg7(models_dump.Pmg7):
    class Meta(models_dump.Pmg7.Meta):
        abstract = False


class Pmg8(models_dump.Pmg8):
    class Meta(models_dump.Pmg8.Meta):
        abstract = False


class Pmg9(models_dump.Pmg9):
    class Meta(models_dump.Pmg9.Meta):
        abstract = False


class Pmn5(models_dump.Pmn5):
    class Meta(models_dump.Pmn5.Meta):
        abstract = False


class Pmv8(models_dump.Pmv8):
    class Meta(models_dump.Pmv8.Meta):
        abstract = False


class Pmx1(models_dump.Pmx1):
    class Meta(models_dump.Pmx1.Meta):
        abstract = False


class Por1(models_dump.Por1):
    class Meta(models_dump.Por1.Meta):
        abstract = False


class Por10(models_dump.Por10):
    class Meta(models_dump.Por10.Meta):
        abstract = False


class Por11(models_dump.Por11):
    class Meta(models_dump.Por11.Meta):
        abstract = False


class Por12(models_dump.Por12):
    class Meta(models_dump.Por12.Meta):
        abstract = False


class Por13(models_dump.Por13):
    class Meta(models_dump.Por13.Meta):
        abstract = False


class Por14(models_dump.Por14):
    class Meta(models_dump.Por14.Meta):
        abstract = False


class Por15(models_dump.Por15):
    class Meta(models_dump.Por15.Meta):
        abstract = False


class Por16(models_dump.Por16):
    class Meta(models_dump.Por16.Meta):
        abstract = False


class Por17(models_dump.Por17):
    class Meta(models_dump.Por17.Meta):
        abstract = False


class Por18(models_dump.Por18):
    class Meta(models_dump.Por18.Meta):
        abstract = False


class Por19(models_dump.Por19):
    class Meta(models_dump.Por19.Meta):
        abstract = False


class Por2(models_dump.Por2):
    class Meta(models_dump.Por2.Meta):
        abstract = False


class Por20(models_dump.Por20):
    class Meta(models_dump.Por20.Meta):
        abstract = False


class Por21(models_dump.Por21):
    class Meta(models_dump.Por21.Meta):
        abstract = False


class Por22(models_dump.Por22):
    class Meta(models_dump.Por22.Meta):
        abstract = False


class Por23(models_dump.Por23):
    class Meta(models_dump.Por23.Meta):
        abstract = False


class Por24(models_dump.Por24):
    class Meta(models_dump.Por24.Meta):
        abstract = False


class Por25(models_dump.Por25):
    class Meta(models_dump.Por25.Meta):
        abstract = False


class Por26(models_dump.Por26):
    class Meta(models_dump.Por26.Meta):
        abstract = False


class Por27(models_dump.Por27):
    class Meta(models_dump.Por27.Meta):
        abstract = False


class Por28(models_dump.Por28):
    class Meta(models_dump.Por28.Meta):
        abstract = False


class Por3(models_dump.Por3):
    class Meta(models_dump.Por3.Meta):
        abstract = False


class Por4(models_dump.Por4):
    class Meta(models_dump.Por4.Meta):
        abstract = False


class Por5(models_dump.Por5):
    class Meta(models_dump.Por5.Meta):
        abstract = False


class Por6(models_dump.Por6):
    class Meta(models_dump.Por6.Meta):
        abstract = False


class Por7(models_dump.Por7):
    class Meta(models_dump.Por7.Meta):
        abstract = False


class Por8(models_dump.Por8):
    class Meta(models_dump.Por8.Meta):
        abstract = False


class Por9(models_dump.Por9):
    class Meta(models_dump.Por9.Meta):
        abstract = False


class Pqt1(models_dump.Pqt1):
    class Meta(models_dump.Pqt1.Meta):
        abstract = False


class Pqt10(models_dump.Pqt10):
    class Meta(models_dump.Pqt10.Meta):
        abstract = False


class Pqt11(models_dump.Pqt11):
    class Meta(models_dump.Pqt11.Meta):
        abstract = False


class Pqt12(models_dump.Pqt12):
    class Meta(models_dump.Pqt12.Meta):
        abstract = False


class Pqt13(models_dump.Pqt13):
    class Meta(models_dump.Pqt13.Meta):
        abstract = False


class Pqt14(models_dump.Pqt14):
    class Meta(models_dump.Pqt14.Meta):
        abstract = False


class Pqt15(models_dump.Pqt15):
    class Meta(models_dump.Pqt15.Meta):
        abstract = False


class Pqt16(models_dump.Pqt16):
    class Meta(models_dump.Pqt16.Meta):
        abstract = False


class Pqt17(models_dump.Pqt17):
    class Meta(models_dump.Pqt17.Meta):
        abstract = False


class Pqt18(models_dump.Pqt18):
    class Meta(models_dump.Pqt18.Meta):
        abstract = False


class Pqt19(models_dump.Pqt19):
    class Meta(models_dump.Pqt19.Meta):
        abstract = False


class Pqt2(models_dump.Pqt2):
    class Meta(models_dump.Pqt2.Meta):
        abstract = False


class Pqt20(models_dump.Pqt20):
    class Meta(models_dump.Pqt20.Meta):
        abstract = False


class Pqt21(models_dump.Pqt21):
    class Meta(models_dump.Pqt21.Meta):
        abstract = False


class Pqt22(models_dump.Pqt22):
    class Meta(models_dump.Pqt22.Meta):
        abstract = False


class Pqt23(models_dump.Pqt23):
    class Meta(models_dump.Pqt23.Meta):
        abstract = False


class Pqt24(models_dump.Pqt24):
    class Meta(models_dump.Pqt24.Meta):
        abstract = False


class Pqt25(models_dump.Pqt25):
    class Meta(models_dump.Pqt25.Meta):
        abstract = False


class Pqt26(models_dump.Pqt26):
    class Meta(models_dump.Pqt26.Meta):
        abstract = False


class Pqt27(models_dump.Pqt27):
    class Meta(models_dump.Pqt27.Meta):
        abstract = False


class Pqt28(models_dump.Pqt28):
    class Meta(models_dump.Pqt28.Meta):
        abstract = False


class Pqt3(models_dump.Pqt3):
    class Meta(models_dump.Pqt3.Meta):
        abstract = False


class Pqt4(models_dump.Pqt4):
    class Meta(models_dump.Pqt4.Meta):
        abstract = False


class Pqt5(models_dump.Pqt5):
    class Meta(models_dump.Pqt5.Meta):
        abstract = False


class Pqt6(models_dump.Pqt6):
    class Meta(models_dump.Pqt6.Meta):
        abstract = False


class Pqt7(models_dump.Pqt7):
    class Meta(models_dump.Pqt7.Meta):
        abstract = False


class Pqt8(models_dump.Pqt8):
    class Meta(models_dump.Pqt8.Meta):
        abstract = False


class Pqt9(models_dump.Pqt9):
    class Meta(models_dump.Pqt9.Meta):
        abstract = False


class Pqw1(models_dump.Pqw1):
    class Meta(models_dump.Pqw1.Meta):
        abstract = False


class Prq1(models_dump.Prq1):
    class Meta(models_dump.Prq1.Meta):
        abstract = False


class Prq10(models_dump.Prq10):
    class Meta(models_dump.Prq10.Meta):
        abstract = False


class Prq11(models_dump.Prq11):
    class Meta(models_dump.Prq11.Meta):
        abstract = False


class Prq12(models_dump.Prq12):
    class Meta(models_dump.Prq12.Meta):
        abstract = False


class Prq13(models_dump.Prq13):
    class Meta(models_dump.Prq13.Meta):
        abstract = False


class Prq14(models_dump.Prq14):
    class Meta(models_dump.Prq14.Meta):
        abstract = False


class Prq15(models_dump.Prq15):
    class Meta(models_dump.Prq15.Meta):
        abstract = False


class Prq16(models_dump.Prq16):
    class Meta(models_dump.Prq16.Meta):
        abstract = False


class Prq17(models_dump.Prq17):
    class Meta(models_dump.Prq17.Meta):
        abstract = False


class Prq18(models_dump.Prq18):
    class Meta(models_dump.Prq18.Meta):
        abstract = False


class Prq19(models_dump.Prq19):
    class Meta(models_dump.Prq19.Meta):
        abstract = False


class Prq2(models_dump.Prq2):
    class Meta(models_dump.Prq2.Meta):
        abstract = False


class Prq20(models_dump.Prq20):
    class Meta(models_dump.Prq20.Meta):
        abstract = False


class Prq21(models_dump.Prq21):
    class Meta(models_dump.Prq21.Meta):
        abstract = False


class Prq22(models_dump.Prq22):
    class Meta(models_dump.Prq22.Meta):
        abstract = False


class Prq23(models_dump.Prq23):
    class Meta(models_dump.Prq23.Meta):
        abstract = False


class Prq24(models_dump.Prq24):
    class Meta(models_dump.Prq24.Meta):
        abstract = False


class Prq25(models_dump.Prq25):
    class Meta(models_dump.Prq25.Meta):
        abstract = False


class Prq26(models_dump.Prq26):
    class Meta(models_dump.Prq26.Meta):
        abstract = False


class Prq27(models_dump.Prq27):
    class Meta(models_dump.Prq27.Meta):
        abstract = False


class Prq28(models_dump.Prq28):
    class Meta(models_dump.Prq28.Meta):
        abstract = False


class Prq3(models_dump.Prq3):
    class Meta(models_dump.Prq3.Meta):
        abstract = False


class Prq4(models_dump.Prq4):
    class Meta(models_dump.Prq4.Meta):
        abstract = False


class Prq5(models_dump.Prq5):
    class Meta(models_dump.Prq5.Meta):
        abstract = False


class Prq6(models_dump.Prq6):
    class Meta(models_dump.Prq6.Meta):
        abstract = False


class Prq7(models_dump.Prq7):
    class Meta(models_dump.Prq7.Meta):
        abstract = False


class Prq8(models_dump.Prq8):
    class Meta(models_dump.Prq8.Meta):
        abstract = False


class Prq9(models_dump.Prq9):
    class Meta(models_dump.Prq9.Meta):
        abstract = False


class Prr1(models_dump.Prr1):
    class Meta(models_dump.Prr1.Meta):
        abstract = False


class Prr10(models_dump.Prr10):
    class Meta(models_dump.Prr10.Meta):
        abstract = False


class Prr11(models_dump.Prr11):
    class Meta(models_dump.Prr11.Meta):
        abstract = False


class Prr12(models_dump.Prr12):
    class Meta(models_dump.Prr12.Meta):
        abstract = False


class Prr13(models_dump.Prr13):
    class Meta(models_dump.Prr13.Meta):
        abstract = False


class Prr14(models_dump.Prr14):
    class Meta(models_dump.Prr14.Meta):
        abstract = False


class Prr15(models_dump.Prr15):
    class Meta(models_dump.Prr15.Meta):
        abstract = False


class Prr16(models_dump.Prr16):
    class Meta(models_dump.Prr16.Meta):
        abstract = False


class Prr17(models_dump.Prr17):
    class Meta(models_dump.Prr17.Meta):
        abstract = False


class Prr18(models_dump.Prr18):
    class Meta(models_dump.Prr18.Meta):
        abstract = False


class Prr19(models_dump.Prr19):
    class Meta(models_dump.Prr19.Meta):
        abstract = False


class Prr2(models_dump.Prr2):
    class Meta(models_dump.Prr2.Meta):
        abstract = False


class Prr20(models_dump.Prr20):
    class Meta(models_dump.Prr20.Meta):
        abstract = False


class Prr21(models_dump.Prr21):
    class Meta(models_dump.Prr21.Meta):
        abstract = False


class Prr22(models_dump.Prr22):
    class Meta(models_dump.Prr22.Meta):
        abstract = False


class Prr23(models_dump.Prr23):
    class Meta(models_dump.Prr23.Meta):
        abstract = False


class Prr24(models_dump.Prr24):
    class Meta(models_dump.Prr24.Meta):
        abstract = False


class Prr25(models_dump.Prr25):
    class Meta(models_dump.Prr25.Meta):
        abstract = False


class Prr26(models_dump.Prr26):
    class Meta(models_dump.Prr26.Meta):
        abstract = False


class Prr27(models_dump.Prr27):
    class Meta(models_dump.Prr27.Meta):
        abstract = False


class Prr28(models_dump.Prr28):
    class Meta(models_dump.Prr28.Meta):
        abstract = False


class Prr3(models_dump.Prr3):
    class Meta(models_dump.Prr3.Meta):
        abstract = False


class Prr4(models_dump.Prr4):
    class Meta(models_dump.Prr4.Meta):
        abstract = False


class Prr5(models_dump.Prr5):
    class Meta(models_dump.Prr5.Meta):
        abstract = False


class Prr6(models_dump.Prr6):
    class Meta(models_dump.Prr6.Meta):
        abstract = False


class Prr7(models_dump.Prr7):
    class Meta(models_dump.Prr7.Meta):
        abstract = False


class Prr8(models_dump.Prr8):
    class Meta(models_dump.Prr8.Meta):
        abstract = False


class Prr9(models_dump.Prr9):
    class Meta(models_dump.Prr9.Meta):
        abstract = False


class Prs1(models_dump.Prs1):
    class Meta(models_dump.Prs1.Meta):
        abstract = False


class Putr(models_dump.Putr):
    class Meta(models_dump.Putr.Meta):
        abstract = False


class Putr1(models_dump.Putr1):
    class Meta(models_dump.Putr1.Meta):
        abstract = False


class Pwz1(models_dump.Pwz1):
    class Meta(models_dump.Pwz1.Meta):
        abstract = False


class Pwz2(models_dump.Pwz2):
    class Meta(models_dump.Pwz2.Meta):
        abstract = False


class Pwz3(models_dump.Pwz3):
    class Meta(models_dump.Pwz3.Meta):
        abstract = False


class Pwz4(models_dump.Pwz4):
    class Meta(models_dump.Pwz4.Meta):
        abstract = False


class Pwz5(models_dump.Pwz5):
    class Meta(models_dump.Pwz5.Meta):
        abstract = False


class Pwz6(models_dump.Pwz6):
    class Meta(models_dump.Pwz6.Meta):
        abstract = False


class Pwz7(models_dump.Pwz7):
    class Meta(models_dump.Pwz7.Meta):
        abstract = False


class Pyd1(models_dump.Pyd1):
    class Meta(models_dump.Pyd1.Meta):
        abstract = False


class Pym1(models_dump.Pym1):
    class Meta(models_dump.Pym1.Meta):
        abstract = False


class Qag1(models_dump.Qag1):
    class Meta(models_dump.Qag1.Meta):
        abstract = False


class Qfd1(models_dump.Qfd1):
    class Meta(models_dump.Qfd1.Meta):
        abstract = False


class Que1(models_dump.Que1):
    class Meta(models_dump.Que1.Meta):
        abstract = False


class Que2(models_dump.Que2):
    class Meta(models_dump.Que2.Meta):
        abstract = False


class Qut1(models_dump.Qut1):
    itemcode = models.ForeignKey(
        "Oitm", models.DO_NOTHING, db_column="ItemCode", max_length=50
    )

    class Meta(models_dump.Qut1.Meta):
        abstract = False


class Qut10(models_dump.Qut10):
    class Meta(models_dump.Qut10.Meta):
        abstract = False


class Qut11(models_dump.Qut11):
    class Meta(models_dump.Qut11.Meta):
        abstract = False


class Qut12(models_dump.Qut12):
    class Meta(models_dump.Qut12.Meta):
        abstract = False


class Qut13(models_dump.Qut13):
    class Meta(models_dump.Qut13.Meta):
        abstract = False


class Qut14(models_dump.Qut14):
    class Meta(models_dump.Qut14.Meta):
        abstract = False


class Qut15(models_dump.Qut15):
    class Meta(models_dump.Qut15.Meta):
        abstract = False


class Qut16(models_dump.Qut16):
    class Meta(models_dump.Qut16.Meta):
        abstract = False


class Qut17(models_dump.Qut17):
    class Meta(models_dump.Qut17.Meta):
        abstract = False


class Qut18(models_dump.Qut18):
    class Meta(models_dump.Qut18.Meta):
        abstract = False


class Qut19(models_dump.Qut19):
    class Meta(models_dump.Qut19.Meta):
        abstract = False


class Qut2(models_dump.Qut2):
    class Meta(models_dump.Qut2.Meta):
        abstract = False


class Qut20(models_dump.Qut20):
    class Meta(models_dump.Qut20.Meta):
        abstract = False


class Qut21(models_dump.Qut21):
    class Meta(models_dump.Qut21.Meta):
        abstract = False


class Qut22(models_dump.Qut22):
    class Meta(models_dump.Qut22.Meta):
        abstract = False


class Qut23(models_dump.Qut23):
    class Meta(models_dump.Qut23.Meta):
        abstract = False


class Qut24(models_dump.Qut24):
    class Meta(models_dump.Qut24.Meta):
        abstract = False


class Qut25(models_dump.Qut25):
    class Meta(models_dump.Qut25.Meta):
        abstract = False


class Qut26(models_dump.Qut26):
    class Meta(models_dump.Qut26.Meta):
        abstract = False


class Qut27(models_dump.Qut27):
    class Meta(models_dump.Qut27.Meta):
        abstract = False


class Qut28(models_dump.Qut28):
    class Meta(models_dump.Qut28.Meta):
        abstract = False


class Qut3(models_dump.Qut3):
    class Meta(models_dump.Qut3.Meta):
        abstract = False


class Qut4(models_dump.Qut4):
    class Meta(models_dump.Qut4.Meta):
        abstract = False


class Qut5(models_dump.Qut5):
    class Meta(models_dump.Qut5.Meta):
        abstract = False


class Qut6(models_dump.Qut6):
    class Meta(models_dump.Qut6.Meta):
        abstract = False


class Qut7(models_dump.Qut7):
    class Meta(models_dump.Qut7.Meta):
        abstract = False


class Qut8(models_dump.Qut8):
    class Meta(models_dump.Qut8.Meta):
        abstract = False


class Qut9(models_dump.Qut9):
    class Meta(models_dump.Qut9.Meta):
        abstract = False


class Qwz1(models_dump.Qwz1):
    class Meta(models_dump.Qwz1.Meta):
        abstract = False


class Qwz2(models_dump.Qwz2):
    class Meta(models_dump.Qwz2.Meta):
        abstract = False


class Qwz3(models_dump.Qwz3):
    class Meta(models_dump.Qwz3.Meta):
        abstract = False


class Rcc4(models_dump.Rcc4):
    class Meta(models_dump.Rcc4.Meta):
        abstract = False


class Rci1(models_dump.Rci1):
    class Meta(models_dump.Rci1.Meta):
        abstract = False


class Rcon(models_dump.Rcon):
    class Meta(models_dump.Rcon.Meta):
        abstract = False


class Rcr1(models_dump.Rcr1):
    class Meta(models_dump.Rcr1.Meta):
        abstract = False


class Rcr2(models_dump.Rcr2):
    class Meta(models_dump.Rcr2.Meta):
        abstract = False


class Rct1(models_dump.Rct1):
    class Meta(models_dump.Rct1.Meta):
        abstract = False


class Rct2(models_dump.Rct2):
    class Meta(models_dump.Rct2.Meta):
        abstract = False


class Rct3(models_dump.Rct3):
    class Meta(models_dump.Rct3.Meta):
        abstract = False


class Rct4(models_dump.Rct4):
    class Meta(models_dump.Rct4.Meta):
        abstract = False


class Rct5(models_dump.Rct5):
    class Meta(models_dump.Rct5.Meta):
        abstract = False


class Rct6(models_dump.Rct6):
    class Meta(models_dump.Rct6.Meta):
        abstract = False


class Rct7(models_dump.Rct7):
    class Meta(models_dump.Rct7.Meta):
        abstract = False


class Rct8(models_dump.Rct8):
    class Meta(models_dump.Rct8.Meta):
        abstract = False


class Rct9(models_dump.Rct9):
    class Meta(models_dump.Rct9.Meta):
        abstract = False


class Rdc1(models_dump.Rdc1):
    class Meta(models_dump.Rdc1.Meta):
        abstract = False


class Rdfl(models_dump.Rdfl):
    class Meta(models_dump.Rdfl.Meta):
        abstract = False


class Rdn1(models_dump.Rdn1):
    class Meta(models_dump.Rdn1.Meta):
        abstract = False


class Rdn10(models_dump.Rdn10):
    class Meta(models_dump.Rdn10.Meta):
        abstract = False


class Rdn11(models_dump.Rdn11):
    class Meta(models_dump.Rdn11.Meta):
        abstract = False


class Rdn12(models_dump.Rdn12):
    class Meta(models_dump.Rdn12.Meta):
        abstract = False


class Rdn13(models_dump.Rdn13):
    class Meta(models_dump.Rdn13.Meta):
        abstract = False


class Rdn14(models_dump.Rdn14):
    class Meta(models_dump.Rdn14.Meta):
        abstract = False


class Rdn15(models_dump.Rdn15):
    class Meta(models_dump.Rdn15.Meta):
        abstract = False


class Rdn16(models_dump.Rdn16):
    class Meta(models_dump.Rdn16.Meta):
        abstract = False


class Rdn17(models_dump.Rdn17):
    class Meta(models_dump.Rdn17.Meta):
        abstract = False


class Rdn18(models_dump.Rdn18):
    class Meta(models_dump.Rdn18.Meta):
        abstract = False


class Rdn19(models_dump.Rdn19):
    class Meta(models_dump.Rdn19.Meta):
        abstract = False


class Rdn2(models_dump.Rdn2):
    class Meta(models_dump.Rdn2.Meta):
        abstract = False


class Rdn20(models_dump.Rdn20):
    class Meta(models_dump.Rdn20.Meta):
        abstract = False


class Rdn21(models_dump.Rdn21):
    class Meta(models_dump.Rdn21.Meta):
        abstract = False


class Rdn22(models_dump.Rdn22):
    class Meta(models_dump.Rdn22.Meta):
        abstract = False


class Rdn23(models_dump.Rdn23):
    class Meta(models_dump.Rdn23.Meta):
        abstract = False


class Rdn24(models_dump.Rdn24):
    class Meta(models_dump.Rdn24.Meta):
        abstract = False


class Rdn25(models_dump.Rdn25):
    class Meta(models_dump.Rdn25.Meta):
        abstract = False


class Rdn26(models_dump.Rdn26):
    class Meta(models_dump.Rdn26.Meta):
        abstract = False


class Rdn27(models_dump.Rdn27):
    class Meta(models_dump.Rdn27.Meta):
        abstract = False


class Rdn28(models_dump.Rdn28):
    class Meta(models_dump.Rdn28.Meta):
        abstract = False


class Rdn3(models_dump.Rdn3):
    class Meta(models_dump.Rdn3.Meta):
        abstract = False


class Rdn4(models_dump.Rdn4):
    class Meta(models_dump.Rdn4.Meta):
        abstract = False


class Rdn5(models_dump.Rdn5):
    class Meta(models_dump.Rdn5.Meta):
        abstract = False


class Rdn6(models_dump.Rdn6):
    class Meta(models_dump.Rdn6.Meta):
        abstract = False


class Rdn7(models_dump.Rdn7):
    class Meta(models_dump.Rdn7.Meta):
        abstract = False


class Rdn8(models_dump.Rdn8):
    class Meta(models_dump.Rdn8.Meta):
        abstract = False


class Rdn9(models_dump.Rdn9):
    class Meta(models_dump.Rdn9.Meta):
        abstract = False


class Rdoc(models_dump.Rdoc):
    class Meta(models_dump.Rdoc.Meta):
        abstract = False


class Rdr1(models_dump.Rdr1):
    class Meta(models_dump.Rdr1.Meta):
        abstract = False


class Rdr10(models_dump.Rdr10):
    class Meta(models_dump.Rdr10.Meta):
        abstract = False


class Rdr11(models_dump.Rdr11):
    class Meta(models_dump.Rdr11.Meta):
        abstract = False


class Rdr12(models_dump.Rdr12):
    class Meta(models_dump.Rdr12.Meta):
        abstract = False


class Rdr13(models_dump.Rdr13):
    class Meta(models_dump.Rdr13.Meta):
        abstract = False


class Rdr14(models_dump.Rdr14):
    class Meta(models_dump.Rdr14.Meta):
        abstract = False


class Rdr15(models_dump.Rdr15):
    class Meta(models_dump.Rdr15.Meta):
        abstract = False


class Rdr16(models_dump.Rdr16):
    class Meta(models_dump.Rdr16.Meta):
        abstract = False


class Rdr17(models_dump.Rdr17):
    class Meta(models_dump.Rdr17.Meta):
        abstract = False


class Rdr18(models_dump.Rdr18):
    class Meta(models_dump.Rdr18.Meta):
        abstract = False


class Rdr19(models_dump.Rdr19):
    class Meta(models_dump.Rdr19.Meta):
        abstract = False


class Rdr2(models_dump.Rdr2):
    class Meta(models_dump.Rdr2.Meta):
        abstract = False


class Rdr20(models_dump.Rdr20):
    class Meta(models_dump.Rdr20.Meta):
        abstract = False


class Rdr21(models_dump.Rdr21):
    class Meta(models_dump.Rdr21.Meta):
        abstract = False


class Rdr22(models_dump.Rdr22):
    class Meta(models_dump.Rdr22.Meta):
        abstract = False


class Rdr23(models_dump.Rdr23):
    class Meta(models_dump.Rdr23.Meta):
        abstract = False


class Rdr24(models_dump.Rdr24):
    class Meta(models_dump.Rdr24.Meta):
        abstract = False


class Rdr25(models_dump.Rdr25):
    class Meta(models_dump.Rdr25.Meta):
        abstract = False


class Rdr26(models_dump.Rdr26):
    class Meta(models_dump.Rdr26.Meta):
        abstract = False


class Rdr27(models_dump.Rdr27):
    class Meta(models_dump.Rdr27.Meta):
        abstract = False


class Rdr28(models_dump.Rdr28):
    class Meta(models_dump.Rdr28.Meta):
        abstract = False


class Rdr3(models_dump.Rdr3):
    class Meta(models_dump.Rdr3.Meta):
        abstract = False


class Rdr4(models_dump.Rdr4):
    class Meta(models_dump.Rdr4.Meta):
        abstract = False


class Rdr5(models_dump.Rdr5):
    class Meta(models_dump.Rdr5.Meta):
        abstract = False


class Rdr6(models_dump.Rdr6):
    class Meta(models_dump.Rdr6.Meta):
        abstract = False


class Rdr7(models_dump.Rdr7):
    class Meta(models_dump.Rdr7.Meta):
        abstract = False


class Rdr8(models_dump.Rdr8):
    class Meta(models_dump.Rdr8.Meta):
        abstract = False


class Rdr9(models_dump.Rdr9):
    class Meta(models_dump.Rdr9.Meta):
        abstract = False


class Req1(models_dump.Req1):
    class Meta(models_dump.Req1.Meta):
        abstract = False


class Req2(models_dump.Req2):
    class Meta(models_dump.Req2.Meta):
        abstract = False


class Req3(models_dump.Req3):
    class Meta(models_dump.Req3.Meta):
        abstract = False


class Rin1(models_dump.Rin1):
    class Meta(models_dump.Rin1.Meta):
        abstract = False


class Rin10(models_dump.Rin10):
    class Meta(models_dump.Rin10.Meta):
        abstract = False


class Rin11(models_dump.Rin11):
    class Meta(models_dump.Rin11.Meta):
        abstract = False


class Rin12(models_dump.Rin12):
    class Meta(models_dump.Rin12.Meta):
        abstract = False


class Rin13(models_dump.Rin13):
    class Meta(models_dump.Rin13.Meta):
        abstract = False


class Rin14(models_dump.Rin14):
    class Meta(models_dump.Rin14.Meta):
        abstract = False


class Rin15(models_dump.Rin15):
    class Meta(models_dump.Rin15.Meta):
        abstract = False


class Rin16(models_dump.Rin16):
    class Meta(models_dump.Rin16.Meta):
        abstract = False


class Rin17(models_dump.Rin17):
    class Meta(models_dump.Rin17.Meta):
        abstract = False


class Rin18(models_dump.Rin18):
    class Meta(models_dump.Rin18.Meta):
        abstract = False


class Rin19(models_dump.Rin19):
    class Meta(models_dump.Rin19.Meta):
        abstract = False


class Rin2(models_dump.Rin2):
    class Meta(models_dump.Rin2.Meta):
        abstract = False


class Rin20(models_dump.Rin20):
    class Meta(models_dump.Rin20.Meta):
        abstract = False


class Rin21(models_dump.Rin21):
    class Meta(models_dump.Rin21.Meta):
        abstract = False


class Rin22(models_dump.Rin22):
    class Meta(models_dump.Rin22.Meta):
        abstract = False


class Rin23(models_dump.Rin23):
    class Meta(models_dump.Rin23.Meta):
        abstract = False


class Rin24(models_dump.Rin24):
    class Meta(models_dump.Rin24.Meta):
        abstract = False


class Rin25(models_dump.Rin25):
    class Meta(models_dump.Rin25.Meta):
        abstract = False


class Rin26(models_dump.Rin26):
    class Meta(models_dump.Rin26.Meta):
        abstract = False


class Rin27(models_dump.Rin27):
    class Meta(models_dump.Rin27.Meta):
        abstract = False


class Rin28(models_dump.Rin28):
    class Meta(models_dump.Rin28.Meta):
        abstract = False


class Rin3(models_dump.Rin3):
    class Meta(models_dump.Rin3.Meta):
        abstract = False


class Rin4(models_dump.Rin4):
    class Meta(models_dump.Rin4.Meta):
        abstract = False


class Rin5(models_dump.Rin5):
    class Meta(models_dump.Rin5.Meta):
        abstract = False


class Rin6(models_dump.Rin6):
    class Meta(models_dump.Rin6.Meta):
        abstract = False


class Rin7(models_dump.Rin7):
    class Meta(models_dump.Rin7.Meta):
        abstract = False


class Rin8(models_dump.Rin8):
    class Meta(models_dump.Rin8.Meta):
        abstract = False


class Rin9(models_dump.Rin9):
    class Meta(models_dump.Rin9.Meta):
        abstract = False


class Rit1(models_dump.Rit1):
    class Meta(models_dump.Rit1.Meta):
        abstract = False


class Ritm(models_dump.Ritm):
    class Meta(models_dump.Ritm.Meta):
        abstract = False


class Rld1(models_dump.Rld1):
    class Meta(models_dump.Rld1.Meta):
        abstract = False


class Rls1(models_dump.Rls1):
    class Meta(models_dump.Rls1.Meta):
        abstract = False


class Rpc1(models_dump.Rpc1):
    class Meta(models_dump.Rpc1.Meta):
        abstract = False


class Rpc10(models_dump.Rpc10):
    class Meta(models_dump.Rpc10.Meta):
        abstract = False


class Rpc11(models_dump.Rpc11):
    class Meta(models_dump.Rpc11.Meta):
        abstract = False


class Rpc12(models_dump.Rpc12):
    class Meta(models_dump.Rpc12.Meta):
        abstract = False


class Rpc13(models_dump.Rpc13):
    class Meta(models_dump.Rpc13.Meta):
        abstract = False


class Rpc14(models_dump.Rpc14):
    class Meta(models_dump.Rpc14.Meta):
        abstract = False


class Rpc15(models_dump.Rpc15):
    class Meta(models_dump.Rpc15.Meta):
        abstract = False


class Rpc16(models_dump.Rpc16):
    class Meta(models_dump.Rpc16.Meta):
        abstract = False


class Rpc17(models_dump.Rpc17):
    class Meta(models_dump.Rpc17.Meta):
        abstract = False


class Rpc18(models_dump.Rpc18):
    class Meta(models_dump.Rpc18.Meta):
        abstract = False


class Rpc19(models_dump.Rpc19):
    class Meta(models_dump.Rpc19.Meta):
        abstract = False


class Rpc2(models_dump.Rpc2):
    class Meta(models_dump.Rpc2.Meta):
        abstract = False


class Rpc20(models_dump.Rpc20):
    class Meta(models_dump.Rpc20.Meta):
        abstract = False


class Rpc21(models_dump.Rpc21):
    class Meta(models_dump.Rpc21.Meta):
        abstract = False


class Rpc22(models_dump.Rpc22):
    class Meta(models_dump.Rpc22.Meta):
        abstract = False


class Rpc23(models_dump.Rpc23):
    class Meta(models_dump.Rpc23.Meta):
        abstract = False


class Rpc24(models_dump.Rpc24):
    class Meta(models_dump.Rpc24.Meta):
        abstract = False


class Rpc25(models_dump.Rpc25):
    class Meta(models_dump.Rpc25.Meta):
        abstract = False


class Rpc26(models_dump.Rpc26):
    class Meta(models_dump.Rpc26.Meta):
        abstract = False


class Rpc27(models_dump.Rpc27):
    class Meta(models_dump.Rpc27.Meta):
        abstract = False


class Rpc28(models_dump.Rpc28):
    class Meta(models_dump.Rpc28.Meta):
        abstract = False


class Rpc3(models_dump.Rpc3):
    class Meta(models_dump.Rpc3.Meta):
        abstract = False


class Rpc4(models_dump.Rpc4):
    class Meta(models_dump.Rpc4.Meta):
        abstract = False


class Rpc5(models_dump.Rpc5):
    class Meta(models_dump.Rpc5.Meta):
        abstract = False


class Rpc6(models_dump.Rpc6):
    class Meta(models_dump.Rpc6.Meta):
        abstract = False


class Rpc7(models_dump.Rpc7):
    class Meta(models_dump.Rpc7.Meta):
        abstract = False


class Rpc8(models_dump.Rpc8):
    class Meta(models_dump.Rpc8.Meta):
        abstract = False


class Rpc9(models_dump.Rpc9):
    class Meta(models_dump.Rpc9.Meta):
        abstract = False


class Rpd1(models_dump.Rpd1):
    class Meta(models_dump.Rpd1.Meta):
        abstract = False


class Rpd10(models_dump.Rpd10):
    class Meta(models_dump.Rpd10.Meta):
        abstract = False


class Rpd11(models_dump.Rpd11):
    class Meta(models_dump.Rpd11.Meta):
        abstract = False


class Rpd12(models_dump.Rpd12):
    class Meta(models_dump.Rpd12.Meta):
        abstract = False


class Rpd13(models_dump.Rpd13):
    class Meta(models_dump.Rpd13.Meta):
        abstract = False


class Rpd14(models_dump.Rpd14):
    class Meta(models_dump.Rpd14.Meta):
        abstract = False


class Rpd15(models_dump.Rpd15):
    class Meta(models_dump.Rpd15.Meta):
        abstract = False


class Rpd16(models_dump.Rpd16):
    class Meta(models_dump.Rpd16.Meta):
        abstract = False


class Rpd17(models_dump.Rpd17):
    class Meta(models_dump.Rpd17.Meta):
        abstract = False


class Rpd18(models_dump.Rpd18):
    class Meta(models_dump.Rpd18.Meta):
        abstract = False


class Rpd19(models_dump.Rpd19):
    class Meta(models_dump.Rpd19.Meta):
        abstract = False


class Rpd2(models_dump.Rpd2):
    class Meta(models_dump.Rpd2.Meta):
        abstract = False


class Rpd20(models_dump.Rpd20):
    class Meta(models_dump.Rpd20.Meta):
        abstract = False


class Rpd21(models_dump.Rpd21):
    class Meta(models_dump.Rpd21.Meta):
        abstract = False


class Rpd22(models_dump.Rpd22):
    class Meta(models_dump.Rpd22.Meta):
        abstract = False


class Rpd23(models_dump.Rpd23):
    class Meta(models_dump.Rpd23.Meta):
        abstract = False


class Rpd24(models_dump.Rpd24):
    class Meta(models_dump.Rpd24.Meta):
        abstract = False


class Rpd25(models_dump.Rpd25):
    class Meta(models_dump.Rpd25.Meta):
        abstract = False


class Rpd26(models_dump.Rpd26):
    class Meta(models_dump.Rpd26.Meta):
        abstract = False


class Rpd27(models_dump.Rpd27):
    class Meta(models_dump.Rpd27.Meta):
        abstract = False


class Rpd28(models_dump.Rpd28):
    class Meta(models_dump.Rpd28.Meta):
        abstract = False


class Rpd3(models_dump.Rpd3):
    class Meta(models_dump.Rpd3.Meta):
        abstract = False


class Rpd4(models_dump.Rpd4):
    class Meta(models_dump.Rpd4.Meta):
        abstract = False


class Rpd5(models_dump.Rpd5):
    class Meta(models_dump.Rpd5.Meta):
        abstract = False


class Rpd6(models_dump.Rpd6):
    class Meta(models_dump.Rpd6.Meta):
        abstract = False


class Rpd7(models_dump.Rpd7):
    class Meta(models_dump.Rpd7.Meta):
        abstract = False


class Rpd8(models_dump.Rpd8):
    class Meta(models_dump.Rpd8.Meta):
        abstract = False


class Rpd9(models_dump.Rpd9):
    class Meta(models_dump.Rpd9.Meta):
        abstract = False


class Rprs(models_dump.Rprs):
    class Meta(models_dump.Rprs.Meta):
        abstract = False


class Rrr1(models_dump.Rrr1):
    class Meta(models_dump.Rrr1.Meta):
        abstract = False


class Rrr10(models_dump.Rrr10):
    class Meta(models_dump.Rrr10.Meta):
        abstract = False


class Rrr11(models_dump.Rrr11):
    class Meta(models_dump.Rrr11.Meta):
        abstract = False


class Rrr12(models_dump.Rrr12):
    class Meta(models_dump.Rrr12.Meta):
        abstract = False


class Rrr13(models_dump.Rrr13):
    class Meta(models_dump.Rrr13.Meta):
        abstract = False


class Rrr14(models_dump.Rrr14):
    class Meta(models_dump.Rrr14.Meta):
        abstract = False


class Rrr15(models_dump.Rrr15):
    class Meta(models_dump.Rrr15.Meta):
        abstract = False


class Rrr16(models_dump.Rrr16):
    class Meta(models_dump.Rrr16.Meta):
        abstract = False


class Rrr17(models_dump.Rrr17):
    class Meta(models_dump.Rrr17.Meta):
        abstract = False


class Rrr18(models_dump.Rrr18):
    class Meta(models_dump.Rrr18.Meta):
        abstract = False


class Rrr19(models_dump.Rrr19):
    class Meta(models_dump.Rrr19.Meta):
        abstract = False


class Rrr2(models_dump.Rrr2):
    class Meta(models_dump.Rrr2.Meta):
        abstract = False


class Rrr20(models_dump.Rrr20):
    class Meta(models_dump.Rrr20.Meta):
        abstract = False


class Rrr21(models_dump.Rrr21):
    class Meta(models_dump.Rrr21.Meta):
        abstract = False


class Rrr22(models_dump.Rrr22):
    class Meta(models_dump.Rrr22.Meta):
        abstract = False


class Rrr23(models_dump.Rrr23):
    class Meta(models_dump.Rrr23.Meta):
        abstract = False


class Rrr24(models_dump.Rrr24):
    class Meta(models_dump.Rrr24.Meta):
        abstract = False


class Rrr25(models_dump.Rrr25):
    class Meta(models_dump.Rrr25.Meta):
        abstract = False


class Rrr26(models_dump.Rrr26):
    class Meta(models_dump.Rrr26.Meta):
        abstract = False


class Rrr27(models_dump.Rrr27):
    class Meta(models_dump.Rrr27.Meta):
        abstract = False


class Rrr28(models_dump.Rrr28):
    class Meta(models_dump.Rrr28.Meta):
        abstract = False


class Rrr3(models_dump.Rrr3):
    class Meta(models_dump.Rrr3.Meta):
        abstract = False


class Rrr4(models_dump.Rrr4):
    class Meta(models_dump.Rrr4.Meta):
        abstract = False


class Rrr5(models_dump.Rrr5):
    class Meta(models_dump.Rrr5.Meta):
        abstract = False


class Rrr6(models_dump.Rrr6):
    class Meta(models_dump.Rrr6.Meta):
        abstract = False


class Rrr7(models_dump.Rrr7):
    class Meta(models_dump.Rrr7.Meta):
        abstract = False


class Rrr8(models_dump.Rrr8):
    class Meta(models_dump.Rrr8.Meta):
        abstract = False


class Rrr9(models_dump.Rrr9):
    class Meta(models_dump.Rrr9.Meta):
        abstract = False


class Rsat(models_dump.Rsat):
    class Meta(models_dump.Rsat.Meta):
        abstract = False


class Rsc1(models_dump.Rsc1):
    class Meta(models_dump.Rsc1.Meta):
        abstract = False


class Rsc2(models_dump.Rsc2):
    class Meta(models_dump.Rsc2.Meta):
        abstract = False


class Rsc3(models_dump.Rsc3):
    class Meta(models_dump.Rsc3.Meta):
        abstract = False


class Rsc4(models_dump.Rsc4):
    class Meta(models_dump.Rsc4.Meta):
        abstract = False


class Rsc5(models_dump.Rsc5):
    class Meta(models_dump.Rsc5.Meta):
        abstract = False


class Rsc6(models_dump.Rsc6):
    class Meta(models_dump.Rsc6.Meta):
        abstract = False


class Rti1(models_dump.Rti1):
    class Meta(models_dump.Rti1.Meta):
        abstract = False


class Rti2(models_dump.Rti2):
    class Meta(models_dump.Rti2.Meta):
        abstract = False


class Rti3(models_dump.Rti3):
    class Meta(models_dump.Rti3.Meta):
        abstract = False


class Rtl1(models_dump.Rtl1):
    class Meta(models_dump.Rtl1.Meta):
        abstract = False


class Rtm1(models_dump.Rtm1):
    class Meta(models_dump.Rtm1.Meta):
        abstract = False


class Rtm2(models_dump.Rtm2):
    class Meta(models_dump.Rtm2.Meta):
        abstract = False


class Rtw1(models_dump.Rtw1):
    class Meta(models_dump.Rtw1.Meta):
        abstract = False


class Rtw2(models_dump.Rtw2):
    class Meta(models_dump.Rtw2.Meta):
        abstract = False


class Rtyp(models_dump.Rtyp):
    class Meta(models_dump.Rtyp.Meta):
        abstract = False


class Sal1(models_dump.Sal1):
    class Meta(models_dump.Sal1.Meta):
        abstract = False


class Scl1(models_dump.Scl1):
    class Meta(models_dump.Scl1.Meta):
        abstract = False


class Scl2(models_dump.Scl2):
    class Meta(models_dump.Scl2.Meta):
        abstract = False


class Scl3(models_dump.Scl3):
    class Meta(models_dump.Scl3.Meta):
        abstract = False


class Scl4(models_dump.Scl4):
    class Meta(models_dump.Scl4.Meta):
        abstract = False


class Scl5(models_dump.Scl5):
    class Meta(models_dump.Scl5.Meta):
        abstract = False


class Scl6(models_dump.Scl6):
    class Meta(models_dump.Scl6.Meta):
        abstract = False


class Scl7(models_dump.Scl7):
    class Meta(models_dump.Scl7.Meta):
        abstract = False


class Scm1(models_dump.Scm1):
    class Meta(models_dump.Scm1.Meta):
        abstract = False


class Scm2(models_dump.Scm2):
    class Meta(models_dump.Scm2.Meta):
        abstract = False


class Scm3(models_dump.Scm3):
    class Meta(models_dump.Scm3.Meta):
        abstract = False


class Scr1(models_dump.Scr1):
    class Meta(models_dump.Scr1.Meta):
        abstract = False


class Scr2(models_dump.Scr2):
    class Meta(models_dump.Scr2.Meta):
        abstract = False


class Scr3(models_dump.Scr3):
    class Meta(models_dump.Scr3.Meta):
        abstract = False


class Scrt(models_dump.Scrt):
    class Meta(models_dump.Scrt.Meta):
        abstract = False


class Sdex(models_dump.Sdex):
    class Meta(models_dump.Sdex.Meta):
        abstract = False


class Sdis(models_dump.Sdis):
    class Meta(models_dump.Sdis.Meta):
        abstract = False


class Sel1(models_dump.Sel1):
    class Meta(models_dump.Sel1.Meta):
        abstract = False


class Sfc1(models_dump.Sfc1):
    class Meta(models_dump.Sfc1.Meta):
        abstract = False


class Sfc10(models_dump.Sfc10):
    class Meta(models_dump.Sfc10.Meta):
        abstract = False


class Sfc11(models_dump.Sfc11):
    class Meta(models_dump.Sfc11.Meta):
        abstract = False


class Sfc12(models_dump.Sfc12):
    class Meta(models_dump.Sfc12.Meta):
        abstract = False


class Sfc13(models_dump.Sfc13):
    class Meta(models_dump.Sfc13.Meta):
        abstract = False


class Sfc14(models_dump.Sfc14):
    class Meta(models_dump.Sfc14.Meta):
        abstract = False


class Sfc15(models_dump.Sfc15):
    class Meta(models_dump.Sfc15.Meta):
        abstract = False


class Sfc16(models_dump.Sfc16):
    class Meta(models_dump.Sfc16.Meta):
        abstract = False


class Sfc17(models_dump.Sfc17):
    class Meta(models_dump.Sfc17.Meta):
        abstract = False


class Sfc18(models_dump.Sfc18):
    class Meta(models_dump.Sfc18.Meta):
        abstract = False


class Sfc19(models_dump.Sfc19):
    class Meta(models_dump.Sfc19.Meta):
        abstract = False


class Sfc2(models_dump.Sfc2):
    class Meta(models_dump.Sfc2.Meta):
        abstract = False


class Sfc20(models_dump.Sfc20):
    class Meta(models_dump.Sfc20.Meta):
        abstract = False


class Sfc21(models_dump.Sfc21):
    class Meta(models_dump.Sfc21.Meta):
        abstract = False


class Sfc22(models_dump.Sfc22):
    class Meta(models_dump.Sfc22.Meta):
        abstract = False


class Sfc23(models_dump.Sfc23):
    class Meta(models_dump.Sfc23.Meta):
        abstract = False


class Sfc24(models_dump.Sfc24):
    class Meta(models_dump.Sfc24.Meta):
        abstract = False


class Sfc25(models_dump.Sfc25):
    class Meta(models_dump.Sfc25.Meta):
        abstract = False


class Sfc26(models_dump.Sfc26):
    class Meta(models_dump.Sfc26.Meta):
        abstract = False


class Sfc27(models_dump.Sfc27):
    class Meta(models_dump.Sfc27.Meta):
        abstract = False


class Sfc28(models_dump.Sfc28):
    class Meta(models_dump.Sfc28.Meta):
        abstract = False


class Sfc3(models_dump.Sfc3):
    class Meta(models_dump.Sfc3.Meta):
        abstract = False


class Sfc4(models_dump.Sfc4):
    class Meta(models_dump.Sfc4.Meta):
        abstract = False


class Sfc5(models_dump.Sfc5):
    class Meta(models_dump.Sfc5.Meta):
        abstract = False


class Sfc6(models_dump.Sfc6):
    class Meta(models_dump.Sfc6.Meta):
        abstract = False


class Sfc7(models_dump.Sfc7):
    class Meta(models_dump.Sfc7.Meta):
        abstract = False


class Sfc8(models_dump.Sfc8):
    class Meta(models_dump.Sfc8.Meta):
        abstract = False


class Sfc9(models_dump.Sfc9):
    class Meta(models_dump.Sfc9.Meta):
        abstract = False


class Sfi1(models_dump.Sfi1):
    class Meta(models_dump.Sfi1.Meta):
        abstract = False


class Sfi10(models_dump.Sfi10):
    class Meta(models_dump.Sfi10.Meta):
        abstract = False


class Sfi11(models_dump.Sfi11):
    class Meta(models_dump.Sfi11.Meta):
        abstract = False


class Sfi12(models_dump.Sfi12):
    class Meta(models_dump.Sfi12.Meta):
        abstract = False


class Sfi13(models_dump.Sfi13):
    class Meta(models_dump.Sfi13.Meta):
        abstract = False


class Sfi14(models_dump.Sfi14):
    class Meta(models_dump.Sfi14.Meta):
        abstract = False


class Sfi15(models_dump.Sfi15):
    class Meta(models_dump.Sfi15.Meta):
        abstract = False


class Sfi16(models_dump.Sfi16):
    class Meta(models_dump.Sfi16.Meta):
        abstract = False


class Sfi17(models_dump.Sfi17):
    class Meta(models_dump.Sfi17.Meta):
        abstract = False


class Sfi18(models_dump.Sfi18):
    class Meta(models_dump.Sfi18.Meta):
        abstract = False


class Sfi19(models_dump.Sfi19):
    class Meta(models_dump.Sfi19.Meta):
        abstract = False


class Sfi2(models_dump.Sfi2):
    class Meta(models_dump.Sfi2.Meta):
        abstract = False


class Sfi20(models_dump.Sfi20):
    class Meta(models_dump.Sfi20.Meta):
        abstract = False


class Sfi21(models_dump.Sfi21):
    class Meta(models_dump.Sfi21.Meta):
        abstract = False


class Sfi22(models_dump.Sfi22):
    class Meta(models_dump.Sfi22.Meta):
        abstract = False


class Sfi23(models_dump.Sfi23):
    class Meta(models_dump.Sfi23.Meta):
        abstract = False


class Sfi24(models_dump.Sfi24):
    class Meta(models_dump.Sfi24.Meta):
        abstract = False


class Sfi25(models_dump.Sfi25):
    class Meta(models_dump.Sfi25.Meta):
        abstract = False


class Sfi26(models_dump.Sfi26):
    class Meta(models_dump.Sfi26.Meta):
        abstract = False


class Sfi27(models_dump.Sfi27):
    class Meta(models_dump.Sfi27.Meta):
        abstract = False


class Sfi28(models_dump.Sfi28):
    class Meta(models_dump.Sfi28.Meta):
        abstract = False


class Sfi3(models_dump.Sfi3):
    class Meta(models_dump.Sfi3.Meta):
        abstract = False


class Sfi4(models_dump.Sfi4):
    class Meta(models_dump.Sfi4.Meta):
        abstract = False


class Sfi5(models_dump.Sfi5):
    class Meta(models_dump.Sfi5.Meta):
        abstract = False


class Sfi6(models_dump.Sfi6):
    class Meta(models_dump.Sfi6.Meta):
        abstract = False


class Sfi7(models_dump.Sfi7):
    class Meta(models_dump.Sfi7.Meta):
        abstract = False


class Sfi8(models_dump.Sfi8):
    class Meta(models_dump.Sfi8.Meta):
        abstract = False


class Sfi9(models_dump.Sfi9):
    class Meta(models_dump.Sfi9.Meta):
        abstract = False


class Shr1(models_dump.Shr1):
    class Meta(models_dump.Shr1.Meta):
        abstract = False


class Shs1(models_dump.Shs1):
    class Meta(models_dump.Shs1.Meta):
        abstract = False


class Sitm(models_dump.Sitm):
    class Meta(models_dump.Sitm.Meta):
        abstract = False


class Sitw(models_dump.Sitw):
    class Meta(models_dump.Sitw.Meta):
        abstract = False


class Sive(models_dump.Sive):
    class Meta(models_dump.Sive.Meta):
        abstract = False


class Sivk(models_dump.Sivk):
    class Meta(models_dump.Sivk.Meta):
        abstract = False


class Sivl(models_dump.Sivl):
    class Meta(models_dump.Sivl.Meta):
        abstract = False


class Sivl1(models_dump.Sivl1):
    class Meta(models_dump.Sivl1.Meta):
        abstract = False


class Sivl2(models_dump.Sivl2):
    class Meta(models_dump.Sivl2.Meta):
        abstract = False


class Sivq(models_dump.Sivq):
    class Meta(models_dump.Sivq.Meta):
        abstract = False


class Slm1(models_dump.Slm1):
    class Meta(models_dump.Slm1.Meta):
        abstract = False


class Slr1(models_dump.Slr1):
    class Meta(models_dump.Slr1.Meta):
        abstract = False


class Soi1(models_dump.Soi1):
    class Meta(models_dump.Soi1.Meta):
        abstract = False


class Soi2(models_dump.Soi2):
    class Meta(models_dump.Soi2.Meta):
        abstract = False


class Soi3(models_dump.Soi3):
    class Meta(models_dump.Soi3.Meta):
        abstract = False


class Soi4(models_dump.Soi4):
    class Meta(models_dump.Soi4.Meta):
        abstract = False


class Soi5(models_dump.Soi5):
    class Meta(models_dump.Soi5.Meta):
        abstract = False


class Spp1(models_dump.Spp1):
    class Meta(models_dump.Spp1.Meta):
        abstract = False


class Spp2(models_dump.Spp2):
    class Meta(models_dump.Spp2.Meta):
        abstract = False


class Sprg(models_dump.Sprg):
    class Meta(models_dump.Sprg.Meta):
        abstract = False


class Sqr1(models_dump.Sqr1):
    class Meta(models_dump.Sqr1.Meta):
        abstract = False


class Sra1(models_dump.Sra1):
    class Meta(models_dump.Sra1.Meta):
        abstract = False


class Sra2(models_dump.Sra2):
    class Meta(models_dump.Sra2.Meta):
        abstract = False


class Sra3(models_dump.Sra3):
    class Meta(models_dump.Sra3.Meta):
        abstract = False


class Srt1(models_dump.Srt1):
    class Meta(models_dump.Srt1.Meta):
        abstract = False


class Srt2(models_dump.Srt2):
    class Meta(models_dump.Srt2.Meta):
        abstract = False


class Sta1(models_dump.Sta1):
    class Meta(models_dump.Sta1.Meta):
        abstract = False


class Stc1(models_dump.Stc1):
    class Meta(models_dump.Stc1.Meta):
        abstract = False


class Svm1(models_dump.Svm1):
    class Meta(models_dump.Svm1.Meta):
        abstract = False


class Svm2(models_dump.Svm2):
    class Meta(models_dump.Svm2.Meta):
        abstract = False


class Svr1(models_dump.Svr1):
    class Meta(models_dump.Svr1.Meta):
        abstract = False


class Taas(models_dump.Taas):
    class Meta(models_dump.Taas.Meta):
        abstract = False


class Taasf(models_dump.Taasf):
    class Meta(models_dump.Taasf.Meta):
        abstract = False


class Tax1(models_dump.Tax1):
    class Meta(models_dump.Tax1.Meta):
        abstract = False


class Tcd1(models_dump.Tcd1):
    class Meta(models_dump.Tcd1.Meta):
        abstract = False


class Tcd2(models_dump.Tcd2):
    class Meta(models_dump.Tcd2.Meta):
        abstract = False


class Tcd3(models_dump.Tcd3):
    class Meta(models_dump.Tcd3.Meta):
        abstract = False


class Tcd4(models_dump.Tcd4):
    class Meta(models_dump.Tcd4.Meta):
        abstract = False


class Tcd5(models_dump.Tcd5):
    class Meta(models_dump.Tcd5.Meta):
        abstract = False


class Tcn1(models_dump.Tcn1):
    class Meta(models_dump.Tcn1.Meta):
        abstract = False


class Tcn2(models_dump.Tcn2):
    class Meta(models_dump.Tcn2.Meta):
        abstract = False


class Tcn3(models_dump.Tcn3):
    class Meta(models_dump.Tcn3.Meta):
        abstract = False


class Tdr1(models_dump.Tdr1):
    class Meta(models_dump.Tdr1.Meta):
        abstract = False


class Tfc1(models_dump.Tfc1):
    class Meta(models_dump.Tfc1.Meta):
        abstract = False


class Tgg1(models_dump.Tgg1):
    class Meta(models_dump.Tgg1.Meta):
        abstract = False


class Tgpa(models_dump.Tgpa):
    class Meta(models_dump.Tgpa.Meta):
        abstract = False


class Tmp8(models_dump.Tmp8):
    class Meta(models_dump.Tmp8.Meta):
        abstract = False


class Tnn1(models_dump.Tnn1):
    class Meta(models_dump.Tnn1.Meta):
        abstract = False


class Tpi1(models_dump.Tpi1):
    class Meta(models_dump.Tpi1.Meta):
        abstract = False


class Tpi2(models_dump.Tpi2):
    class Meta(models_dump.Tpi2.Meta):
        abstract = False


class Tpi3(models_dump.Tpi3):
    class Meta(models_dump.Tpi3.Meta):
        abstract = False


class Tpi4(models_dump.Tpi4):
    class Meta(models_dump.Tpi4.Meta):
        abstract = False


class Tpl1(models_dump.Tpl1):
    class Meta(models_dump.Tpl1.Meta):
        abstract = False


class Tps1(models_dump.Tps1):
    class Meta(models_dump.Tps1.Meta):
        abstract = False


class Tps2(models_dump.Tps2):
    class Meta(models_dump.Tps2.Meta):
        abstract = False


class Tpw1(models_dump.Tpw1):
    class Meta(models_dump.Tpw1.Meta):
        abstract = False


class Tpw2(models_dump.Tpw2):
    class Meta(models_dump.Tpw2.Meta):
        abstract = False


class Tpw3(models_dump.Tpw3):
    class Meta(models_dump.Tpw3.Meta):
        abstract = False


class Tpw4(models_dump.Tpw4):
    class Meta(models_dump.Tpw4.Meta):
        abstract = False


class Tpw5(models_dump.Tpw5):
    class Meta(models_dump.Tpw5.Meta):
        abstract = False


class Tpw6(models_dump.Tpw6):
    class Meta(models_dump.Tpw6.Meta):
        abstract = False


class Tra1(models_dump.Tra1):
    class Meta(models_dump.Tra1.Meta):
        abstract = False


class Trb1(models_dump.Trb1):
    class Meta(models_dump.Trb1.Meta):
        abstract = False


class Trb2(models_dump.Trb2):
    class Meta(models_dump.Trb2.Meta):
        abstract = False


class Trb3(models_dump.Trb3):
    class Meta(models_dump.Trb3.Meta):
        abstract = False


class Trb4(models_dump.Trb4):
    class Meta(models_dump.Trb4.Meta):
        abstract = False


class Trb5(models_dump.Trb5):
    class Meta(models_dump.Trb5.Meta):
        abstract = False


class Trn1(models_dump.Trn1):
    class Meta(models_dump.Trn1.Meta):
        abstract = False


class Trn2(models_dump.Trn2):
    class Meta(models_dump.Trn2.Meta):
        abstract = False


class Tro1(models_dump.Tro1):
    class Meta(models_dump.Tro1.Meta):
        abstract = False


class Trs1(models_dump.Trs1):
    class Meta(models_dump.Trs1.Meta):
        abstract = False


class Trs2(models_dump.Trs2):
    class Meta(models_dump.Trs2.Meta):
        abstract = False


class Trt1(models_dump.Trt1):
    class Meta(models_dump.Trt1.Meta):
        abstract = False


class Tsh1(models_dump.Tsh1):
    class Meta(models_dump.Tsh1.Meta):
        abstract = False


class Tsi1(models_dump.Tsi1):
    class Meta(models_dump.Tsi1.Meta):
        abstract = False


class Tsi2(models_dump.Tsi2):
    class Meta(models_dump.Tsi2.Meta):
        abstract = False


class Tsi3(models_dump.Tsi3):
    class Meta(models_dump.Tsi3.Meta):
        abstract = False


class Tsi4(models_dump.Tsi4):
    class Meta(models_dump.Tsi4.Meta):
        abstract = False


class Tsp1(models_dump.Tsp1):
    class Meta(models_dump.Tsp1.Meta):
        abstract = False


class Ttp1(models_dump.Ttp1):
    class Meta(models_dump.Ttp1.Meta):
        abstract = False


class Txd1(models_dump.Txd1):
    class Meta(models_dump.Txd1.Meta):
        abstract = False


class Txd2(models_dump.Txd2):
    class Meta(models_dump.Txd2.Meta):
        abstract = False


class Txd3(models_dump.Txd3):
    class Meta(models_dump.Txd3.Meta):
        abstract = False


class Txd4(models_dump.Txd4):
    class Meta(models_dump.Txd4.Meta):
        abstract = False


class Ubtn(models_dump.Ubtn):
    class Meta(models_dump.Ubtn.Meta):
        abstract = False


class Ubvl(models_dump.Ubvl):
    class Meta(models_dump.Ubvl.Meta):
        abstract = False


class Udg1(models_dump.Udg1):
    class Meta(models_dump.Udg1.Meta):
        abstract = False


class Udg2(models_dump.Udg2):
    class Meta(models_dump.Udg2.Meta):
        abstract = False


class Udg3(models_dump.Udg3):
    class Meta(models_dump.Udg3.Meta):
        abstract = False


class Udg4(models_dump.Udg4):
    class Meta(models_dump.Udg4.Meta):
        abstract = False


class Udo1(models_dump.Udo1):
    class Meta(models_dump.Udo1.Meta):
        abstract = False


class Udo2(models_dump.Udo2):
    class Meta(models_dump.Udo2.Meta):
        abstract = False


class Udo3(models_dump.Udo3):
    class Meta(models_dump.Udo3.Meta):
        abstract = False


class Udo4(models_dump.Udo4):
    class Meta(models_dump.Udo4.Meta):
        abstract = False


class Ufd1(models_dump.Ufd1):
    class Meta(models_dump.Ufd1.Meta):
        abstract = False


class Ugp1(models_dump.Ugp1):
    class Meta(models_dump.Ugp1.Meta):
        abstract = False


class Ugr1(models_dump.Ugr1):
    class Meta(models_dump.Ugr1.Meta):
        abstract = False


class Uic1(models_dump.Uic1):
    class Meta(models_dump.Uic1.Meta):
        abstract = False


class Uic2(models_dump.Uic2):
    class Meta(models_dump.Uic2.Meta):
        abstract = False


class Uic3(models_dump.Uic3):
    class Meta(models_dump.Uic3.Meta):
        abstract = False


class Uic4(models_dump.Uic4):
    class Meta(models_dump.Uic4.Meta):
        abstract = False


class Uic5(models_dump.Uic5):
    class Meta(models_dump.Uic5.Meta):
        abstract = False


class Uic6(models_dump.Uic6):
    class Meta(models_dump.Uic6.Meta):
        abstract = False


class Uicu(models_dump.Uicu):
    class Meta(models_dump.Uicu.Meta):
        abstract = False


class Uilm(models_dump.Uilm):
    class Meta(models_dump.Uilm.Meta):
        abstract = False


class Uilm1(models_dump.Uilm1):
    class Meta(models_dump.Uilm1.Meta):
        abstract = False


class Uilm2(models_dump.Uilm2):
    class Meta(models_dump.Uilm2.Meta):
        abstract = False


class Uilm3(models_dump.Uilm3):
    class Meta(models_dump.Uilm3.Meta):
        abstract = False


class Uitm(models_dump.Uitm):
    class Meta(models_dump.Uitm.Meta):
        abstract = False


class Uitw(models_dump.Uitw):
    class Meta(models_dump.Uitw.Meta):
        abstract = False


class Uive(models_dump.Uive):
    class Meta(models_dump.Uive.Meta):
        abstract = False


class Uivk(models_dump.Uivk):
    class Meta(models_dump.Uivk.Meta):
        abstract = False


class Uivl(models_dump.Uivl):
    class Meta(models_dump.Uivl.Meta):
        abstract = False


class Uivl1(models_dump.Uivl1):
    class Meta(models_dump.Uivl1.Meta):
        abstract = False


class Uivl2(models_dump.Uivl2):
    class Meta(models_dump.Uivl2.Meta):
        abstract = False


class Uivq(models_dump.Uivq):
    class Meta(models_dump.Uivq.Meta):
        abstract = False


class Ukd1(models_dump.Ukd1):
    class Meta(models_dump.Ukd1.Meta):
        abstract = False


class Upt1(models_dump.Upt1):
    class Meta(models_dump.Upt1.Meta):
        abstract = False


class Uqr1(models_dump.Uqr1):
    class Meta(models_dump.Uqr1.Meta):
        abstract = False


class Usr1(models_dump.Usr1):
    class Meta(models_dump.Usr1.Meta):
        abstract = False


class Usr2(models_dump.Usr2):
    class Meta(models_dump.Usr2.Meta):
        abstract = False


class Usr3(models_dump.Usr3):
    class Meta(models_dump.Usr3.Meta):
        abstract = False


class Usr5(models_dump.Usr5):
    class Meta(models_dump.Usr5.Meta):
        abstract = False


class Usr6(models_dump.Usr6):
    class Meta(models_dump.Usr6.Meta):
        abstract = False


class Usr7(models_dump.Usr7):
    class Meta(models_dump.Usr7.Meta):
        abstract = False


class Usr8(models_dump.Usr8):
    class Meta(models_dump.Usr8.Meta):
        abstract = False


class Usrn(models_dump.Usrn):
    class Meta(models_dump.Usrn.Meta):
        abstract = False


class Utx1(models_dump.Utx1):
    class Meta(models_dump.Utx1.Meta):
        abstract = False


class Uwko(models_dump.Uwko):
    class Meta(models_dump.Uwko.Meta):
        abstract = False


class Uwko1(models_dump.Uwko1):
    class Meta(models_dump.Uwko1.Meta):
        abstract = False


class Uwo2(models_dump.Uwo2):
    class Meta(models_dump.Uwo2.Meta):
        abstract = False


class Uwor(models_dump.Uwor):
    class Meta(models_dump.Uwor.Meta):
        abstract = False


class Uwor1(models_dump.Uwor1):
    class Meta(models_dump.Uwor1.Meta):
        abstract = False


class Uwor3(models_dump.Uwor3):
    class Meta(models_dump.Uwor3.Meta):
        abstract = False


class Uwor4(models_dump.Uwor4):
    class Meta(models_dump.Uwor4.Meta):
        abstract = False


class Uwor5(models_dump.Uwor5):
    class Meta(models_dump.Uwor5.Meta):
        abstract = False


class Uwtx1(models_dump.Uwtx1):
    class Meta(models_dump.Uwtx1.Meta):
        abstract = False


class Uxilm(models_dump.Uxilm):
    class Meta(models_dump.Uxilm.Meta):
        abstract = False


class Veb1(models_dump.Veb1):
    class Meta(models_dump.Veb1.Meta):
        abstract = False


class Views(models_dump.Views):
    class Meta(models_dump.Views.Meta):
        abstract = False


class Vlg1(models_dump.Vlg1):
    class Meta(models_dump.Vlg1.Meta):
        abstract = False


class Vpm1(models_dump.Vpm1):
    class Meta(models_dump.Vpm1.Meta):
        abstract = False


class Vpm2(models_dump.Vpm2):
    class Meta(models_dump.Vpm2.Meta):
        abstract = False


class Vpm3(models_dump.Vpm3):
    class Meta(models_dump.Vpm3.Meta):
        abstract = False


class Vpm4(models_dump.Vpm4):
    class Meta(models_dump.Vpm4.Meta):
        abstract = False


class Vpm5(models_dump.Vpm5):
    class Meta(models_dump.Vpm5.Meta):
        abstract = False


class Vpm6(models_dump.Vpm6):
    class Meta(models_dump.Vpm6.Meta):
        abstract = False


class Vpm7(models_dump.Vpm7):
    class Meta(models_dump.Vpm7.Meta):
        abstract = False


class Vpm8(models_dump.Vpm8):
    class Meta(models_dump.Vpm8.Meta):
        abstract = False


class Vpm9(models_dump.Vpm9):
    class Meta(models_dump.Vpm9.Meta):
        abstract = False


class Vrt1(models_dump.Vrt1):
    class Meta(models_dump.Vrt1.Meta):
        abstract = False


class Vrt2(models_dump.Vrt2):
    class Meta(models_dump.Vrt2.Meta):
        abstract = False


class Vrw1(models_dump.Vrw1):
    class Meta(models_dump.Vrw1.Meta):
        abstract = False


class Vrw2(models_dump.Vrw2):
    class Meta(models_dump.Vrw2.Meta):
        abstract = False


class Vrw3(models_dump.Vrw3):
    class Meta(models_dump.Vrw3.Meta):
        abstract = False


class Vrw4(models_dump.Vrw4):
    class Meta(models_dump.Vrw4.Meta):
        abstract = False


class Vtg1(models_dump.Vtg1):
    class Meta(models_dump.Vtg1.Meta):
        abstract = False


class Vtr1(models_dump.Vtr1):
    class Meta(models_dump.Vtr1.Meta):
        abstract = False


class Vtr2(models_dump.Vtr2):
    class Meta(models_dump.Vtr2.Meta):
        abstract = False


class Vtr3(models_dump.Vtr3):
    class Meta(models_dump.Vtr3.Meta):
        abstract = False


class Wdbd1(models_dump.Wdbd1):
    class Meta(models_dump.Wdbd1.Meta):
        abstract = False


class Wdd1(models_dump.Wdd1):
    class Meta(models_dump.Wdd1.Meta):
        abstract = False


class Wdd2(models_dump.Wdd2):
    class Meta(models_dump.Wdd2.Meta):
        abstract = False


class Wflt1(models_dump.Wflt1):
    class Meta(models_dump.Wflt1.Meta):
        abstract = False


class Wfst1(models_dump.Wfst1):
    class Meta(models_dump.Wfst1.Meta):
        abstract = False


class Whl1(models_dump.Whl1):
    class Meta(models_dump.Whl1.Meta):
        abstract = False


class Whl2(models_dump.Whl2):
    class Meta(models_dump.Whl2.Meta):
        abstract = False


class Whl3(models_dump.Whl3):
    class Meta(models_dump.Whl3.Meta):
        abstract = False


class Wht1(models_dump.Wht1):
    class Meta(models_dump.Wht1.Meta):
        abstract = False


class Wht2(models_dump.Wht2):
    class Meta(models_dump.Wht2.Meta):
        abstract = False


class Wht3(models_dump.Wht3):
    class Meta(models_dump.Wht3.Meta):
        abstract = False


class Wko1(models_dump.Wko1):
    class Meta(models_dump.Wko1.Meta):
        abstract = False


class Wlpd1(models_dump.Wlpd1):
    class Meta(models_dump.Wlpd1.Meta):
        abstract = False


class Wlpd2(models_dump.Wlpd2):
    class Meta(models_dump.Wlpd2.Meta):
        abstract = False


class Wls1(models_dump.Wls1):
    class Meta(models_dump.Wls1.Meta):
        abstract = False


class Wls2(models_dump.Wls2):
    class Meta(models_dump.Wls2.Meta):
        abstract = False


class Wls3(models_dump.Wls3):
    class Meta(models_dump.Wls3.Meta):
        abstract = False


class Wls4(models_dump.Wls4):
    class Meta(models_dump.Wls4.Meta):
        abstract = False


class Wls5(models_dump.Wls5):
    class Meta(models_dump.Wls5.Meta):
        abstract = False


class Wor1(models_dump.Wor1):
    class Meta(models_dump.Wor1.Meta):
        abstract = False


class Wor2(models_dump.Wor2):
    class Meta(models_dump.Wor2.Meta):
        abstract = False


class Wor2V(models_dump.Wor2V):
    class Meta(models_dump.Wor2V.Meta):
        abstract = False


class Wor3(models_dump.Wor3):
    class Meta(models_dump.Wor3.Meta):
        abstract = False


class Wor4(models_dump.Wor4):
    class Meta(models_dump.Wor4.Meta):
        abstract = False


class Wor5(models_dump.Wor5):
    class Meta(models_dump.Wor5.Meta):
        abstract = False


class Wpk1(models_dump.Wpk1):
    class Meta(models_dump.Wpk1.Meta):
        abstract = False


class Wpk2(models_dump.Wpk2):
    class Meta(models_dump.Wpk2.Meta):
        abstract = False


class Wpk3(models_dump.Wpk3):
    class Meta(models_dump.Wpk3.Meta):
        abstract = False


class Wpk4(models_dump.Wpk4):
    class Meta(models_dump.Wpk4.Meta):
        abstract = False


class Wst1(models_dump.Wst1):
    class Meta(models_dump.Wst1.Meta):
        abstract = False


class Wtc1(models_dump.Wtc1):
    class Meta(models_dump.Wtc1.Meta):
        abstract = False


class Wtc2(models_dump.Wtc2):
    class Meta(models_dump.Wtc2.Meta):
        abstract = False


class Wtd1(models_dump.Wtd1):
    class Meta(models_dump.Wtd1.Meta):
        abstract = False


class Wtd2(models_dump.Wtd2):
    class Meta(models_dump.Wtd2.Meta):
        abstract = False


class Wtd3(models_dump.Wtd3):
    class Meta(models_dump.Wtd3.Meta):
        abstract = False


class Wtd4(models_dump.Wtd4):
    class Meta(models_dump.Wtd4.Meta):
        abstract = False


class Wtd5(models_dump.Wtd5):
    class Meta(models_dump.Wtd5.Meta):
        abstract = False


class Wtm1(models_dump.Wtm1):
    class Meta(models_dump.Wtm1.Meta):
        abstract = False


class Wtm2(models_dump.Wtm2):
    class Meta(models_dump.Wtm2.Meta):
        abstract = False


class Wtm3(models_dump.Wtm3):
    class Meta(models_dump.Wtm3.Meta):
        abstract = False


class Wtm4(models_dump.Wtm4):
    class Meta(models_dump.Wtm4.Meta):
        abstract = False


class Wtm5(models_dump.Wtm5):
    class Meta(models_dump.Wtm5.Meta):
        abstract = False


class Wtq1(models_dump.Wtq1):
    class Meta(models_dump.Wtq1.Meta):
        abstract = False


class Wtq10(models_dump.Wtq10):
    class Meta(models_dump.Wtq10.Meta):
        abstract = False


class Wtq11(models_dump.Wtq11):
    class Meta(models_dump.Wtq11.Meta):
        abstract = False


class Wtq12(models_dump.Wtq12):
    class Meta(models_dump.Wtq12.Meta):
        abstract = False


class Wtq13(models_dump.Wtq13):
    class Meta(models_dump.Wtq13.Meta):
        abstract = False


class Wtq14(models_dump.Wtq14):
    class Meta(models_dump.Wtq14.Meta):
        abstract = False


class Wtq15(models_dump.Wtq15):
    class Meta(models_dump.Wtq15.Meta):
        abstract = False


class Wtq16(models_dump.Wtq16):
    class Meta(models_dump.Wtq16.Meta):
        abstract = False


class Wtq17(models_dump.Wtq17):
    class Meta(models_dump.Wtq17.Meta):
        abstract = False


class Wtq18(models_dump.Wtq18):
    class Meta(models_dump.Wtq18.Meta):
        abstract = False


class Wtq19(models_dump.Wtq19):
    class Meta(models_dump.Wtq19.Meta):
        abstract = False


class Wtq2(models_dump.Wtq2):
    class Meta(models_dump.Wtq2.Meta):
        abstract = False


class Wtq20(models_dump.Wtq20):
    class Meta(models_dump.Wtq20.Meta):
        abstract = False


class Wtq21(models_dump.Wtq21):
    class Meta(models_dump.Wtq21.Meta):
        abstract = False


class Wtq22(models_dump.Wtq22):
    class Meta(models_dump.Wtq22.Meta):
        abstract = False


class Wtq23(models_dump.Wtq23):
    class Meta(models_dump.Wtq23.Meta):
        abstract = False


class Wtq24(models_dump.Wtq24):
    class Meta(models_dump.Wtq24.Meta):
        abstract = False


class Wtq25(models_dump.Wtq25):
    class Meta(models_dump.Wtq25.Meta):
        abstract = False


class Wtq26(models_dump.Wtq26):
    class Meta(models_dump.Wtq26.Meta):
        abstract = False


class Wtq27(models_dump.Wtq27):
    class Meta(models_dump.Wtq27.Meta):
        abstract = False


class Wtq28(models_dump.Wtq28):
    class Meta(models_dump.Wtq28.Meta):
        abstract = False


class Wtq3(models_dump.Wtq3):
    class Meta(models_dump.Wtq3.Meta):
        abstract = False


class Wtq4(models_dump.Wtq4):
    class Meta(models_dump.Wtq4.Meta):
        abstract = False


class Wtq5(models_dump.Wtq5):
    class Meta(models_dump.Wtq5.Meta):
        abstract = False


class Wtq6(models_dump.Wtq6):
    class Meta(models_dump.Wtq6.Meta):
        abstract = False


class Wtq7(models_dump.Wtq7):
    class Meta(models_dump.Wtq7.Meta):
        abstract = False


class Wtq8(models_dump.Wtq8):
    class Meta(models_dump.Wtq8.Meta):
        abstract = False


class Wtq9(models_dump.Wtq9):
    class Meta(models_dump.Wtq9.Meta):
        abstract = False


class Wtr1(models_dump.Wtr1):
    class Meta(models_dump.Wtr1.Meta):
        abstract = False


class Wtr10(models_dump.Wtr10):
    class Meta(models_dump.Wtr10.Meta):
        abstract = False


class Wtr11(models_dump.Wtr11):
    class Meta(models_dump.Wtr11.Meta):
        abstract = False


class Wtr12(models_dump.Wtr12):
    class Meta(models_dump.Wtr12.Meta):
        abstract = False


class Wtr13(models_dump.Wtr13):
    class Meta(models_dump.Wtr13.Meta):
        abstract = False


class Wtr14(models_dump.Wtr14):
    class Meta(models_dump.Wtr14.Meta):
        abstract = False


class Wtr15(models_dump.Wtr15):
    class Meta(models_dump.Wtr15.Meta):
        abstract = False


class Wtr16(models_dump.Wtr16):
    class Meta(models_dump.Wtr16.Meta):
        abstract = False


class Wtr17(models_dump.Wtr17):
    class Meta(models_dump.Wtr17.Meta):
        abstract = False


class Wtr18(models_dump.Wtr18):
    class Meta(models_dump.Wtr18.Meta):
        abstract = False


class Wtr19(models_dump.Wtr19):
    class Meta(models_dump.Wtr19.Meta):
        abstract = False


class Wtr2(models_dump.Wtr2):
    class Meta(models_dump.Wtr2.Meta):
        abstract = False


class Wtr20(models_dump.Wtr20):
    class Meta(models_dump.Wtr20.Meta):
        abstract = False


class Wtr21(models_dump.Wtr21):
    class Meta(models_dump.Wtr21.Meta):
        abstract = False


class Wtr22(models_dump.Wtr22):
    class Meta(models_dump.Wtr22.Meta):
        abstract = False


class Wtr23(models_dump.Wtr23):
    class Meta(models_dump.Wtr23.Meta):
        abstract = False


class Wtr24(models_dump.Wtr24):
    class Meta(models_dump.Wtr24.Meta):
        abstract = False


class Wtr25(models_dump.Wtr25):
    class Meta(models_dump.Wtr25.Meta):
        abstract = False


class Wtr26(models_dump.Wtr26):
    class Meta(models_dump.Wtr26.Meta):
        abstract = False


class Wtr27(models_dump.Wtr27):
    class Meta(models_dump.Wtr27.Meta):
        abstract = False


class Wtr28(models_dump.Wtr28):
    class Meta(models_dump.Wtr28.Meta):
        abstract = False


class Wtr3(models_dump.Wtr3):
    class Meta(models_dump.Wtr3.Meta):
        abstract = False


class Wtr4(models_dump.Wtr4):
    class Meta(models_dump.Wtr4.Meta):
        abstract = False


class Wtr5(models_dump.Wtr5):
    class Meta(models_dump.Wtr5.Meta):
        abstract = False


class Wtr6(models_dump.Wtr6):
    class Meta(models_dump.Wtr6.Meta):
        abstract = False


class Wtr7(models_dump.Wtr7):
    class Meta(models_dump.Wtr7.Meta):
        abstract = False


class Wtr8(models_dump.Wtr8):
    class Meta(models_dump.Wtr8.Meta):
        abstract = False


class Wtr9(models_dump.Wtr9):
    class Meta(models_dump.Wtr9.Meta):
        abstract = False


class Wtx1(models_dump.Wtx1):
    class Meta(models_dump.Wtx1.Meta):
        abstract = False


class Wvt1(models_dump.Wvt1):
    class Meta(models_dump.Wvt1.Meta):
        abstract = False


class Wvt10(models_dump.Wvt10):
    class Meta(models_dump.Wvt10.Meta):
        abstract = False


class Wvt11(models_dump.Wvt11):
    class Meta(models_dump.Wvt11.Meta):
        abstract = False


class Wvt2(models_dump.Wvt2):
    class Meta(models_dump.Wvt2.Meta):
        abstract = False


class Wvt3(models_dump.Wvt3):
    class Meta(models_dump.Wvt3.Meta):
        abstract = False


class Wvt4(models_dump.Wvt4):
    class Meta(models_dump.Wvt4.Meta):
        abstract = False


class Wvt5(models_dump.Wvt5):
    class Meta(models_dump.Wvt5.Meta):
        abstract = False


class Wvt6(models_dump.Wvt6):
    class Meta(models_dump.Wvt6.Meta):
        abstract = False


class Wvt7(models_dump.Wvt7):
    class Meta(models_dump.Wvt7.Meta):
        abstract = False


class Wvt8(models_dump.Wvt8):
    class Meta(models_dump.Wvt8.Meta):
        abstract = False


class Wvt9(models_dump.Wvt9):
    class Meta(models_dump.Wvt9.Meta):
        abstract = False


class Xap1(models_dump.Xap1):
    class Meta(models_dump.Xap1.Meta):
        abstract = False


class Xap2(models_dump.Xap2):
    class Meta(models_dump.Xap2.Meta):
        abstract = False


class Xap3(models_dump.Xap3):
    class Meta(models_dump.Xap3.Meta):
        abstract = False


class Xap4(models_dump.Xap4):
    class Meta(models_dump.Xap4.Meta):
        abstract = False


class Xrdbv(models_dump.Xrdbv):
    class Meta(models_dump.Xrdbv.Meta):
        abstract = False


class Xrobj(models_dump.Xrobj):
    class Meta(models_dump.Xrobj.Meta):
        abstract = False


class Xrrel(models_dump.Xrrel):
    class Meta(models_dump.Xrrel.Meta):
        abstract = False


class Xrudf(models_dump.Xrudf):
    class Meta(models_dump.Xrudf.Meta):
        abstract = False


class Xrxls(models_dump.Xrxls):
    class Meta(models_dump.Xrxls.Meta):
        abstract = False


class Xrxml(models_dump.Xrxml):
    class Meta(models_dump.Xrxml.Meta):
        abstract = False


class Zrd1(models_dump.Zrd1):
    class Meta(models_dump.Zrd1.Meta):
        abstract = False


class Sysdiagrams(models_dump.Sysdiagrams):
    class Meta(models_dump.Sysdiagrams.Meta):
        abstract = False
