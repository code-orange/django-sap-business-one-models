from datetime import datetime

from django_sap_business_one_models.django_sap_business_one_models.models import *
from django_svcstack_models.django_svcstack_models.models import *


def convert_appointment(appointment: Oclg):
    dtstartraw = str(appointment.begintime)
    dtstartraw = dtstartraw.zfill(4)
    dtstartraw_h = dtstartraw[0] + dtstartraw[1]
    dtstartraw_m = dtstartraw[2] + dtstartraw[3]
    dtendraw = str(appointment.endtime)
    dtendraw = dtendraw.zfill(4)
    dtendraw_h = dtendraw[0] + dtendraw[1]
    dtendraw_m = dtendraw[2] + dtendraw[3]

    dtstart = datetime.strptime(
        appointment.recontact.strftime("%Y-%m-%d")
        + " "
        + str(dtstartraw_h)
        + ":"
        + str(dtstartraw_m),
        "%Y-%m-%d %H:%M",
    )

    dtend = datetime.strptime(
        appointment.enddate.strftime("%Y-%m-%d")
        + " "
        + str(dtendraw_h)
        + ":"
        + str(dtendraw_m),
        "%Y-%m-%d %H:%M",
    )

    svcstack_appointment = SvcstackActivities(
        subject=appointment.details, date_start=dtstart, date_end=dtend
    )

    return svcstack_appointment


def convert_appointment_list(appointment_list):
    new_appointment_list = list()

    for appointment in appointment_list:
        new_appointment_list.append(convert_appointment(appointment))

    return new_appointment_list
